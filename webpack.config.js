var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var autoprefixer = require('autoprefixer');
var LiveReloadPlugin = require('webpack-livereload-plugin');

var staticPath = './app/frontend/';
var jsPath = staticPath + 'javascripts/';
var cssPath = staticPath + 'stylesheets/';
var imgPath = staticPath + 'images/';

var NODE_ENV = process.env.NODE_ENV;

var config = {
  cache: true,
  colors: true,
  context: __dirname,

  devtool: 'eval',

  watch: NODE_ENV === 'development',

  watchOptions: {
    aggregateTimeout: 100
  },

  entry: {
    app: jsPath + 'app/bootstrap-app.coffee',
    login: jsPath + 'app/bootstrap-login.coffee',
    stylesheets: cssPath + 'application.sass',
    vendors: jsPath + 'app/vendors.coffee',
    vendorscss: cssPath + 'vendors.sass'
  },

  output: {
    path: path.join('app', 'assets', 'bundle'),
    pathinfo: true,
    filename: '[name].js',
    publicPath: '/assets/'
  },

  resolve: {
    extensions: ['', '.js', '.coffee', '.png', '.jpg', '.gif', '.html', '.sass', '.css'],
    modulesDirectories: [ './app/frontend/javascript/vendors', path.join(__dirname, 'node_modules') ],
    root: path.resolve(__dirname)
  },

  sassLoader: {
    includePaths: [path.resolve(__dirname, jsPath), path.resolve(__dirname, 'node_modules')]
  },

  module: {
    noParse: ['ws'],
    loaders: [
      { test: /\.(svg|png|jpg|gif|woff|woff2|eot|ttf)$/, loader: "file-loader?name=[name].[ext]" },
      { test: /\.sass$/, loader: ExtractTextPlugin.extract("style", "css!postcss!sass!import-glob-loader") },
      { test: /\.coffee$/, loader: 'coffee-loader' },
      { test: /\.html\.(slm|slim)$/, loader: 'ngtemplate?relativeTo=' + path.resolve(__dirname, jsPath) + '!html!slm' },
      { test: /\.html$/, loader: 'ngtemplate?relativeTo=' + path.resolve(__dirname, jsPath) + '!html' }
    ]
  },

  postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],

  externals: ['ws'],

  plugins: [
    new webpack.NoErrorsPlugin(),

    new webpack.ResolverPlugin(
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(".package.json")
    ),

    new webpack.ContextReplacementPlugin(/node_modules\/moment\/locale/, /ru|en/),

    new webpack.IgnorePlugin(/angular-websocket.min.js/),

    new ExtractTextPlugin("[name].css"),

    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      moment: "moment",
      Handsontable: 'handsontable/dist/handsontable.full.min'
    }),

    new webpack.optimize.OccurrenceOrderPlugin(),

    new webpack.optimize.DedupePlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      minChunks: 10000
    }),

    new webpack.EnvironmentPlugin([
      "NODE_ENV"
    ]),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      compress: {
        warnings: false,
        drop_console: true,
        drop_debugger: true
      },
      output: {
        comments: false
      },
      mangle: {
        except: ['$super', '$', 'exports', 'require', '$q', '$ocLazyLoad']
      }
    }),

  ]
};

if (NODE_ENV === 'development') {
  config.plugins.push(new LiveReloadPlugin());
}

module.exports = config;
