require 'bundler'
require 'settings'
require 'faye'
require 'faye/redis'
require 'redis'
require 'logger'
require 'thin'
require 'faye/websocket'

Faye::WebSocket.load_adapter('thin')
Settings::Utils.environments << :staging

class EditorsForDocuments
  TTL = 86400

  def initialize
    log "EditorsForDocuments extension started"
  end

  def incoming(message, request, callback)

    # Клиент подключился и анонсировал себя
    if message['channel'] =~ /^\/document\-subscribe/
      _, document_id = message['channel'].split('_')

      if message['data']['id']
        # Регистрирует вошедшего пользователя, сохраняя данные и о документе куда он залез
        redis.setex key('editor', message['clientId']), TTL, [message['data']['id'], document_id].join('-')

        if clients = redis.get(key('editors_for_document', document_id))
          clients = clients.split(',')
        else
          clients = []
        end

        clients << message['clientId']
        redis.setex key('editors_for_document', document_id), TTL, clients.join(',')

        log "+ #{message['clientId']} -> #{document_id}"
      else
        announce_for_document(document_id)
      end
    end

    # Клиент совсем отвалился или вышел из документа
    if '/meta/unsubscribe' == message['channel']
      editor = redis.get key('editor', message['clientId'])

      if editor 
        redis.del key('editor', message['clientId'])

        _, document_id = editor.split('-')
        announce_for_document(document_id)
      end

      log "- #{message['clientId']}"
    end

    callback.call(message)
  end

  # Собирает ключ с namespace класса
  def key(*args)
    "faye:#{Settings::Utils.env}:state:#{args.join(':')}"
  end

  def log(message)
    logger.info message
  end

  # Подключение к Редису
  def redis
    @redis ||= Redis.new host: Settings.redis.host, db: Settings.redis.db
  end

  # Выбирает из редиса пользователей для документа и анонсирует их
  def announce_for_document(document_id)
    clients = redis.get(key('editors_for_document', document_id))

    clean_clients    = []
    editors          = []

    if clients
      clients.split(',').each do |client_id|
        if editor_id = redis.get(key('editor', client_id))
          editors       << editor_id.split('-')[0]
          clean_clients << client_id
        end
      end 

      if clean_clients.size > 0
        redis.setex key('editors_for_document', document_id), TTL, clean_clients.join(',')
      else
        redis.del key('editors_for_document', document_id)
      end

      editors.uniq!
    end

    client.publish("/document-editors_#{document_id}", { editors: editors })
    log "* [#{editors.join(', ')}] -> #{document_id}"
  end
private
  def logger
    @logger ||= Logger.new(STDOUT)
  end

  # Faye client
  def client
    @client ||= Faye::Client.new(Settings.faye)
  end
end

# Faye.logger = Logger.new(STDOUT)

server = Faye::RackAdapter.new(
  mount: '/faye',
  timeout: 30,
  ping: 10,
  engine: {
    type: Faye::Redis,
    host: Settings.redis.host,
    database: Settings.redis.db,
    namespace: "faye:#{Settings::Utils.env}:"
  }
)

editors_handler = EditorsForDocuments.new

server.add_extension(editors_handler)

server.on :disconnect do |client_id|
  editor = editors_handler.redis.get editors_handler.key('editor', client_id)

  if editor 
    editors_handler.redis.del editors_handler.key('editor', client_id)

    _, document_id = editor.split('-')
    editors_handler.announce_for_document(document_id)
  end

  editors_handler.log "- #{client_id}"
end

run server
