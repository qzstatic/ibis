class AngularTemplates
  include ActionView::Helpers::JavaScriptHelper
  
  def self.compile(*args)
    self.new.compile(*args)
  end
  
  def compile(include: [])
    templates = include.map { |path| Dir.glob(path) }
    templates.flatten!.uniq!

    templates.map! do |file_name|
      # Компилируем шаблон и квотим
      template = escape_javascript(Slim::Template.new(file_name).render())
    
      # Убираем ненужный мусор из пути
      file_name = file_name.split('app/').join('')
      file_name = file_name.split('modules/').join('')
      file_name = file_name.split('.slim').join('')
      file_name = "/#{file_name}"

      "$templateCache.put('#{file_name}', '#{template}');"
    end
    
    templates = templates.join("\n")
    
    "angular.module('app').run(function($templateCache){ #{templates} });"
  end
end
