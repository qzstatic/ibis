class ReportsView
  attr_reader :result

  def initialize(result)
    @result = result
  end

  def call
    if result['aggregations']['authors']['buckets'].present?
      result['aggregations']['authors']['buckets'].each do |author|
        begin
          if document = Roompen.document_by_id(author['key'].split(':')[1])
            author[:document] = document
          end
        rescue Roompen::NotFound => e
          Rails.logger.warn("Can't find author: #{e}")
        end
      end
    end
    result
  end
end