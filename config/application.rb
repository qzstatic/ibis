require File.expand_path('../boot', __FILE__)

require "active_model/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"

Bundler.require(*Rails.groups)

module Ibis
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/lib)
    
    config.time_zone = 'Moscow'
    config.i18n.default_locale = :en
  end
end
