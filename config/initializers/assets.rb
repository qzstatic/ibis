Rails.application.configure do
  config.assets.paths << Rails.root.join("app", "frontend", "bundle")
  config.assets.precompile += %w( *.js )
  config.assets.precompile += %w( *.css )
  config.assets.precompile += %w( *.html )
end
