Rails.application.assets.register_engine('.slim', Slim::Template)
Slim::Engine.set_options(pretty: true) if Rails.env.development?
