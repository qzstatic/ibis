Rails.application.routes.draw do
  # Load balancer
  get '/check_alive' => 'main#check_alive'
  
  get 'login', to: 'main#login'
  get 'revision', to: 'main#revision'
  
  root 'main#index'

  get 'reports/authors', to: 'reports#authors'

  get  '*path', to: 'main#index'
end
