module ApplicationHelper
  def production?
    Rails.env.production? || Rails.env.staging?
  end

  def p(total, current)
    "#{(current.to_f * 100 / total).round(1)}%"
  end

  def document(url)
    document = Roompen.document("vedomosti.ru#{url}")
    document.title.present? ? document : false 
  rescue
    false
  end
end
