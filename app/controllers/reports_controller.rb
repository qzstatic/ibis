class ReportsController < ApplicationController
  layout false

  def authors
    start_date = @start_date = if params[:start_date].present?
      Date.parse(params[:start_date])
    else
      (Time.now - 1.month).strftime('%Y-%m-%d')
    end

    end_date = @end_date = if params[:end_date].present?
      Date.parse(params[:end_date])
    else
      Time.now.strftime('%Y-%m-%d')
    end

    c = Faraday.new do |f|
      f.adapter Faraday.default_adapter
      f.headers['X-Access-Token'] = params[:token]
    end

    resp = c.get("#{Settings.hosts.snipe}/v1/documents/by_period?start_date=#{start_date}&end_date=#{end_date}")

    if 200 == resp.status
      raw_documents = Oj.load(resp.body)

      ids = []

      raw_documents.each do |item|
        ids << item[:id]
      end

      query = {
          "query" => {
              "bool" => {
                  "must" => [
                      {
                          "terms" => {
                              "tdoc_id" => ids
                          }
                      },
                      {
                          "range" => {
                              "time" => {
                                  "lt" => "#{end_date}T00:00",
                                  "gt" => "#{start_date}T00:00"
                              }
                          }
                      },
                      {
                          "term" => {
                              "event" => "hit"
                          }
                      },
                  ]
              }
          },
          "aggs" => {
              "authors" => {
                  "terms" => {
                      "field" => "tags",
                      "include" => "author:[0-9]+",
                      "size" => 0
                  },
                  "aggs" => {
                      "total" => {
                          "filter" => {
                              "term" => {
                                  "event" => "hit"
                              }
                          }
                      },
                      "total_from_yandex" => {
                          "filter" => {
                              "regexp" => {
                                  "referer" => "(.*?)yandex.ru"
                              }
                          }
                      },
                      "total_from_google" => {
                          "filter" => {
                              "regexp" => {
                                  "referer" => "(.*?)google.com"
                              }
                          }
                      },
                      "total_from_vedomosti" => {
                          "filter" => {
                              "regexp" => {
                                  "referer" => "(.*?)vedomosti.ru"
                              }
                          }
                      },
                      "articles" => {
                          "terms" => {
                              "field" => "path",
                              "size" => 30
                          },
                          "aggs" => {
                              "from_yandex" => {
                                  "filter" => {
                                      "regexp" => {
                                          "referer" => "(.*?)yandex.ru"
                                      }
                                  }
                              },
                              "from_google" => {
                                  "filter" => {
                                      "regexp" => {
                                          "referer" => "(.*?)google.com"
                                      }
                                  }
                              },
                              "from_vedomosti" => {
                                  "filter" => {
                                      "regexp" => {
                                          "referer" => "(.*?)vedomosti.ru"
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          },
          "size" => 0
      }

      client = Elasticsearch::Client.new host: 'es.vedomosti.ru'
      @result = ReportsView.new(client.search index: 'visits', body: query).call
    else
      render inline: resp.status.to_s
    end
  end
end
