class MainController < ApplicationController
  def index
    respond_to do |format|
      format.html { render nothing: true, layout: true }
    end
  end
  
  def login
    respond_to do |format|
      format.html { render nothing: true, layout: 'login' }
    end
  end
  
  def check_alive
    head :ok
  end

  def revision
    branch_file = Rails.root.join('.git/HEAD')
    branch      = branch_file.read.strip.sub(/^ref: refs\/heads\//,'')
    revision    = Rails.root.join(".git/refs/heads/#{branch}").read.strip

    render json: {
      deploy_at: branch_file.mtime,
      branch: branch,
      revision: Rails.root.join(".git/refs/heads/#{branch}").read.strip
    }
  end
end
