indexTemplate = require('./index.html')
dialogTemplate = require('../dialogs/dialog.html')

angular.module('import', [])
  .config ($stateProvider) ->
    $stateProvider

      .state 'logged.import',
        abstract: true,
        url: '/import'
        data:
          permissions:
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.import.list',
        auth: true,
        url: '/list'
        views:
          'content@logged':
            templateUrl: indexTemplate
            controller:  'ImportListController'

  .controller 'ImportListController', ($scope, $rootScope, Aquarium, Snipe, $localStorage, $state, $uibModal, Notify) ->

    current_item = null

    reload = (filter) ->
      prepared_filter = angular.copy(filter)
      prepared_filter.date_from = moment(prepared_filter.date_from).format('YYYY-MM-DD 00:00:00 +3')
      prepared_filter.date_to   = moment(prepared_filter.date_to).format('YYYY-MM-DD 23:59:59 +3')

      Aquarium.all('import_news').getList(prepared_filter).then (news) -> $scope.news = news

    $scope.$storage = $localStorage.$default
      imported_news_filter:
        limit: 20
        order: 'news_date'
        date_from: moment().add(-1, 'day').format('YYYY-MM-DD')

    $scope.limits = [20, 50, 100, 200, 500]
    $scope.order = [
      { value: 'news_date', title: 'времени новости' },
      { value: 'updated_at', title: 'времени импорта' }
    ]

    $scope.filters = $scope.$storage.imported_news_filter
    $scope.filters.date_to =  moment().format('YYYY-MM-DD')

    $scope.$watchCollection 'filters', (filter) -> reload(filter)

    $scope.format = (body) ->
      formatted = _.trim(body).split(/(\r\n|\n)+/g).join('</p><p>')
      "<p>#{formatted}</p>"

    $scope.toggle = (item) ->
      if current_item != item
        current_item.more = false if current_item?
        current_item = item

      item.more = !item.more

    $scope.create_news = (item) ->
      modal_instance = $uibModal.open
        templateUrl: dialogTemplate
        controller: ($scope, $uibModalInstance) ->
          $scope.title = 'Создание новости'
          $scope.text = 'На базе выбранной новости будет создан новый документ, отменить эту операцию будет невозможно. Хотите продолжить?'

          $scope.ok = -> $uibModalInstance.close('ok');
          $scope.close = -> $uibModalInstance.dismiss('close');

      modal_instance.result.then (state) ->
        if 'ok' == state
          new_document = {
            category_ids: [140737488355530, 140737488355531, 140737488355328, 140737488355329, 140737488355335, 140737488355336, 140737488355337, 140737488355338]
            title: item.title
          }

          Snipe.all('documents').post(attributes: new_document).then (document) ->
            document.one('root_box').get().then (root_box) ->
              index = 0
              paras = []

              for para in item.body.split("\n")
                para = _.trim(para)
                if '' != para
                  index += 1000
                  paras.push {
                    parent_id: root_box.id,
                    position: index,
                    body: para,
                    type: 'paragraph',
                    kind: 'plain',
                    text_align: 'left'
                  }

              Snipe.all('boxes/multi').post(attributes: paras).then ->
                Notify.add type: 'success', msg: 'Документ успешно создан и вы перемещены к нему.'
                $rootScope.newDocument = true
                $state.go('logged.documents.edit', id: document.id)


