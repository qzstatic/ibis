messagesTemplate = require('./messages.html')

angular.module('notify', [])
  
  .service 'Notify', ($timeout, ProxyStorage) ->
    
    # Класс {Notify}, реализующий сервис оповещений. Позволяет добавлять в стек сообщения и 
    # удалять их через указанное время. Сообщения сохраняются между сессиями пользователя в {ProxyStorage}.
    class Notify
      messages_store = []
      
      # Инициализирует объект и загружает из {ProxyStorage} объект с сообщениями
      constructor: -> @add((ProxyStorage.get('notify') || []).reverse(), true)
        
      # Удаляет сообщение из стека
      #
      # @param [Object] message сообщение для удаления
      # @return [Object] self
      remove: (message) ->
        index = _.indexOf(messages_store, message)
        messages_store.splice(index, 1)
        
        ProxyStorage.put('notify', messages_store)
        
        @
      
      # Добавляет новое сообщение в стек
      #
      # @example Посылка уведомления
      #   Notify.add(timeout: 5, msg: 'Сообщение')
      #
      # @example Посылка нескольких уведомлений
      #   Notify.add([
      #    { timeout: 5, msg: 'Сообщение' },
      #    { type: 'info', timeout: 10, msg: 'Еще одно сообщение' }
      #   ])
      #
      # @param [Object, Array] message единичное сообщение или массив сообщений
      # @param [Boolean] fill флаг, отвечающий за сохранение сообщения в {ProxyStorage}
      # @option messages [Integer] id уникальный идентификатор сообщения 
      # @option messages [String]  type тип сообщения: info, success или error 
      # @option messages [Integer] timeout время жизни сообщения в секундах 
      # @option messages [String]  msg сообщение для отображения 
      #
      # @return [Object] self
      add: (messages, fill = false) ->
        messages = [messages] unless angular.isArray(messages) 
  
        for message in messages
          message.id      ?= new Date().getTime()
          message.type    ?= 'info'
          message.timeout ?= 5

          messages_store.unshift(message)
          $timeout (=> @remove(message)), message.timeout * 1000

        ProxyStorage.put('notify', messages_store) unless fill
        
        @
        
      # Возвращает массив из сообщений
      #
      # @return [Object] массив из сообщений
      messages: -> messages_store
      
      count: -> messages_store.length
      
      empty: -> messages_store = []
      
    new Notify
   
  
  # Директива, управляющая выводом списка оповещений
  .directive 'rmNotifyMessages', ->
    scope: {}
    restrict: 'E'
    templateUrl: messagesTemplate
    controller: ($scope, Notify, $state) ->
      $scope.messages = Notify.messages()
      $scope.close = (message) -> Notify.remove(message)
      $scope.class_name = (message) -> "b-notify-message_#{message.type}"
      $scope.goto_state = (state,params) -> $state.go(state,params) if state?
      $scope.has_state = (state) -> true if state?
