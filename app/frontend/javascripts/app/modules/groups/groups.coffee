listTemplate = require('./index.html')
editTemplate = require('./edit.html')
newTemplate = require('./new.html')

angular.module('app')

  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.groups',
        abstract: true,
        url: '/groups'
        data: 
          permissions: 
            only: ['ADMIN']
            redirectTo: 'errors.403'
            
      .state 'logged.groups.list',
        url: '/list'
        auth: true
        resolve:
          groups: (Snipe) -> Snipe.all('groups').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'GroupsListController'
      
      .state 'logged.groups.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          group: (Snipe, $stateParams) -> Snipe.one('groups', $stateParams.id).get()
          editors: (Snipe) -> Snipe.all('editors').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'GroupsEditController'
            
      .state 'logged.groups.new',
        url: '/new'
        auth: true
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'GroupsNewController'
  
controllers =
  GroupsListController: ($scope, groups, $filter) -> 
    $scope.groups =  $filter('orderBy')(groups, 'position', true)
      
  GroupsEditController: ($scope, editors, group, Notify) -> 
    $scope.group = group
    $scope.editors = editors

    for editor in editors
      editor.checked = editor.id in group.editor_ids

    $scope.update = (group) -> 
      $scope.feed = group.patch().then ->
        Notify.add(msg: 'Группа изменена')

    $scope.update_editors = (editor) ->
      if editor.checked
        # добавили редактора
        $scope.group.editor_ids.push(editor.id)
        editor.group_ids.push($scope.group.id)
        Notify.add(msg: 'В группу добавлен редактор')
      else
        # убрали группу
        $scope.group.editor_ids = _.without($scope.group.editor_ids, editor.id)
        editor.group_ids = _.without(editor.group_ids, $scope.group.id)
        Notify.add(msg: 'Из группы убран редактор')
      
      editor.patch(group_ids: editor.group_ids)
      
  GroupsNewController: ($scope, $state, Snipe) ->    
    $scope.create = (group) -> 
      $scope.feed = Snipe.all('groups').post(attributes: group).then -> $state.go('logged.groups.list')
        
angular.module('groups', []).controller(controllers)
