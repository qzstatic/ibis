documentTemplate = require('./document_linked.html.slim')

angular.module('ui')
  .directive 'rmDocumentLinked', ->
    restrict: 'E'
    scope:
      box: '='
      document: '='
    templateUrl: documentTemplate
    controller: ($scope, Snipe, $http, Settings, Search, Utils) ->
      $scope.Utils = Utils

      getBoundDocument = () ->
        Snipe.one('documents', $scope.box.bound_document_id).get().then (document) ->
          $scope.document = document

      if !$scope.document?.id && $scope.box?.bound_document_id?
        getBoundDocument()

      $scope.$watch 'box', (curr, prev) ->
        if curr? && prev? && $scope.box.bound_document_id? && curr.bound_document_id != prev.bound_document_id
          getBoundDocument()
      , true
