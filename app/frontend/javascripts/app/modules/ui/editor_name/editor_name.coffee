angular.module('ui')
  .directive 'rmEditorName', ->
    restrict: 'A'
    template: '{{ ::full_name }}'
    controller: ($scope, Editor, EditorsService) -> 
      EditorsService.list().then -> $scope.full_name = EditorsService.by_id(Editor.id()).full_name
