categoriesTemplate = require('./categories.html.slim')
branchTemplate = require('./branch.html.slim')

angular.module('ui')
  .directive 'rmCategoriesSelector', ->
    restrict: 'E'
    scope:
      document: '='
      categories: '='
      action:  '='
      exclude: '='
    templateUrl: categoriesTemplate
    controller: ($scope, Snipe, $filter, Locker, $element, $timeout, current_document, $rootScope, CategoriesService, PaywallService) ->
      $scope.branchTemplate = branchTemplate

      $scope.categories_list = {}
      $scope.selected = {}
      $scope.tree = []

      $rootScope.disabledCreateDocument = !$scope.document?.id?

      check_option = (c, flag) -> angular.isObject(c) && (flag in c.nested_categories)

      $scope.autoselected = (c) -> check_option(c, 'autoselected')
      $scope.required     = (c) -> check_option(c, 'required')
      $scope.single       = (c) -> check_option(c, 'single')
      $scope.immutable    = (c) -> check_option(c, 'immutable') && $scope.document?.id?
      $scope.enabled      = (c) -> c.enabled

      $scope.prepare_branch = (branch) ->
        for category in branch
          if $scope.autoselected(category) && category.children && category.children.length > 0
            $scope.selected[category.id] = if $scope.single(category) then category.children[0].id else [category.children[0].id]

      import_from = (branch) ->
        for category in branch
          ids = _.map(category.children, 'id')
          found = _.intersection($scope.categories, ids)
          if found.length > 0
            $scope.selected[category.id] = if $scope.single(category) then _.first(found) else found

          import_from(category.children) if category.children?

      Snipe.all('categories/tree').getList().then (tree) ->
        Snipe.one('categories/with_children').get().then (list) ->
          if $scope.categories?
            import_from(tree)
          else
            $scope.prepare_branch(tree)

          $scope.categories_list = list
          $scope.tree = tree

          if $scope.categories?
            paywallSetting = $scope.checkCategoriesForPaywall()
            PaywallService.position = paywallSetting.position
            PaywallService.enabled = paywallSetting.enabled
            if $rootScope.newDocument? && $rootScope.newDocument == true
              $rootScope.$broadcast 'update:categories', paywall: paywallSetting
              delete $rootScope.newDocument


          $scope.export_to()


      reload = -> current_document.reload().then ->
        $scope.categories = current_document.document.category_ids

      $scope.$watch 'categories', -> import_from($scope.tree) if $scope.categories? && $scope.tree?

      $scope.find_by_id = (list, id) -> _.find(list, id: id)

      calculate_forbidden = ->
        new_forbidden = []

        for item in $scope.categories || []
          new_forbidden.push($scope.categories_list[item].forbidden_combinations) if $scope.categories_list[item]?.forbidden_combinations?
        _.uniq(_.flatten(new_forbidden))


      ordered = []

      # Составляет коэффициенты для сортировки
      calculate_orders = (items) ->
        for branch in items
          ordered.push(branch.id) if branch.id in $scope.categories
          unless $scope.single(branch)
            if branch.children?
              list = []
              for category in branch.children
                list.push category.id

              for id in $scope.categories
                ordered.push(id) if id in list

              for category in branch.children
                calculate_orders(category.children) if category.children?

          else
            calculate_orders(branch.children) if branch.children?


      $scope.export_to = ->
        result = []

        angular.forEach $scope.selected, (items) ->
          items = [items] unless angular.isArray(items)

          for id in items
            if id? && (found = $scope.categories_list[id])
              result.push(found.path)
              result.push(found.id)

        categories = _.uniq(_.flatten(result))

        $scope.categories = $filter('filter')(categories, (id) ->
          id not in calculate_forbidden()
        )

        ordered = []
        calculate_orders($scope.tree)

        $scope.categories = _.uniq(ordered)

      $scope.get_required = ->
        _.filter $scope.filter_forbidden($scope.tree), (category) -> $scope.required category

      $scope.checkRequiredFileds = ->
        required = $scope.get_required().filter (c) -> $scope.single c
        _.every required, (c) -> $scope.selected[c.id]?

      $scope.add_item = (category) ->
        $scope.prepare_branch(category) if category.children.length > 0
        $scope.export_to()
        $rootScope.disabledCreateDocument = !$scope.checkRequiredFileds()

      $scope.prepare_values = (category_id) ->
        $scope.selected[category_id] = [null] unless $scope.selected[category_id]?

      $scope.filter_forbidden = (categories) ->
        $filter('filter')(categories, (category) ->
          category.id not in calculate_forbidden()
        )

      $scope.selected_and_allowed = (items, selected) ->
        selected = _.compact(selected)
        filtered = if selected.length > 0
          _.map selected, (id) -> $scope.categories_list[id]
        else
          []
        filtered = $scope.filter_forbidden(filtered)
        CategoriesService.selectedCategories = angular.copy filtered
        filtered.push(null)
        filtered


      intersectWith = (subsequent, main) ->
        _.every subsequent, (value, key) ->
          return false unless main[key]?
          return true if value.length == 0 && main[key]?
          _.intersection(main[key], value).length == value.length

      $scope.getSelectedSlugs = (categories) ->
        slugs = {}
        fullCategories = categories.map (category) -> $scope.categories_list[category]

        fullCategories.forEach (category) ->
          if category.children.length > 0
            category.children.forEach (children) ->
              if categories.indexOf(children) != -1
                if category.slug not in slugs
                  slugs[category.slug] = []
                slugs[category.slug].push $scope.categories_list[children].slug
        slugs


      $scope.checkCategoriesForPaywall = () ->
        paywallSetting = enabled: false, position: 2, slugs: []
        if current_document.document.settings.paywall?
          paywallSettings = current_document.document.settings.paywall

          paywallSetting = _.last paywallSettings if paywallSettings?.length > 0

          if paywallSettings? && paywallSettings
            selectedSlugs = $scope.getSelectedSlugs angular.copy $scope.categories
            for setting in paywallSettings
              if intersectWith setting.slugs, selectedSlugs
                paywallSetting = setting
                break

        PaywallService.enabled = paywallSetting.enabled
        PaywallService.position = paywallSetting.position
        paywallSetting

      remove_branch = (from, category_id) ->
        if angular.isArray(from)
          index = _.indexOf(from, category_id)
          from.splice(index, 1)
        delete $scope.selected[category_id]

        if $scope.categories_list[category_id].children?
          for subcategory_id in $scope.categories_list[category_id].children
            remove_branch($scope.selected[subcategory_id], subcategory_id)

      $scope.remove = (from, category_id) ->
        remove_branch(from, category_id)
        $scope.export_to()

      $scope.only_active = (from, present, selected) ->
        res = $filter('filter')(from, (item) ->
          (item.id == selected || item.id not in present) && item.id not in calculate_forbidden()
        )

      $scope.onlyEnabledOrSelected = (categories, selectedId) ->
        $filter('filter')(categories, (item) ->
          (item.enabled || item.id == selectedId) && item.id not in calculate_forbidden()
        )

      $scope.update = ->
        current_document.document.patch(category_ids: $scope.categories).then ->
          paywallSetting = $scope.checkCategoriesForPaywall()
          PaywallService.position = paywallSetting.position
          PaywallService.enabled = paywallSetting.enabled
          $rootScope.$broadcast 'document.update', id: document.id
          $rootScope.$broadcast 'update:categories'
          $scope.Locker.unlock(true) if $scope.document?.id?

      if $scope.document?.id?
        $scope.Locker = new Locker
          scope: $scope
          element: $element
          id: $scope.document.id
          part: 'categories'
          reload: ->
            reload()
