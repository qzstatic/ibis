angular.module('ui')
  .directive 'datepickerLocaldate', ($parse) ->
      restrict: 'A'
      require: [ 'ngModel' ]
      link: (scope, element, attr, ctrls) ->
        ngModelController = ctrls[0]
        ngModelController.$parsers.push (viewValue) ->
          if !viewValue then return null
          viewValue.setMinutes viewValue.getMinutes() - viewValue.getTimezoneOffset()
          new Date(viewValue.toISOString())
        ngModelController.$formatters.push (modelValue) ->
          if !modelValue then return
          dt = new Date(modelValue)
          dt.setMinutes dt.getMinutes() + dt.getTimezoneOffset()
          dt
        return
