uploaderTemplate = require('./uploader.html.slim')

angular.module('ui')
  .directive 'rmUploader', ->

    restrict: 'E'

    scope:
      document: '='
      objforsave: '='
      kind: '@'
      name: '@'
      type: '@'
      no_actions: '@noActions'
      versions: '@'
    templateUrl: uploaderTemplate
    controller: ($scope, Snipe, Agami, doc_settings, Settings, $rootScope) ->

      $scope.attachments_host = Settings.attachments_host
      not_allowed_versions = ['_preview']
      boxes = $rootScope.root_box.children
      $scope.image = {}

      isStandalone = -> $scope.objforsave?

      if 'image' == $scope.type
        if $scope.versions
          versions = JSON.parse $scope.versions
        else
          versions = _.clone(doc_settings.settings.attachments?[$scope.kind]?.versions) || {}
          versions[key] = "#{dimensions.width}x#{dimensions.height}>" for key, dimensions of versions
        versions['_preview'] = '110x68^'
      else
        versions = {}

      # Загружаем аттачмент
      if isStandalone()
        angular.extend $scope.image, $scope.objforsave
      else if $scope.document?[$scope.name]? && $scope.document[$scope.name]
        if $scope.kind == 'gallery_image' && $scope.document.attachment?.versions?
          $scope.image = $scope.document.attachment
        else
          Snipe.one('attachments', $scope.document[$scope.name]).get().then (res) ->
            $scope.image = res
            if $scope.kind == 'title_image'
              $rootScope.title_image = $scope.image

      # Документ или бокс по косвенным признакам
      isDocument = -> $scope.document.settings? && $scope.document.category_ids?

      # Определяет, показывать ли эту версию в списке или нет
      $scope.allow_version = (version_name) -> version_name not in not_allowed_versions

      # Первоначальная загрузка файла
      $scope.select_file = (files) ->
        if box = _.find(boxes, { id: $scope.document.id })
          boxUpdatedAt = box.updated_at
        else
          boxUpdatedAt = null
        Agami
          .upload(_.first(files), false, versions)
          .then (data) ->
            item = if isDocument() then 'documents' else 'boxes'
            struct = {}
            struct[$scope.name] = data.id
            if boxUpdatedAt then struct.updated_at = boxUpdatedAt
            if isDocument()
              Snipe.one(item, $scope.document.id).patch(struct).then (data) =>
                _.map boxes, (item) ->
                  if item.id == data.id then item.updated_at = data.updated_at
            angular.extend($scope.image, data)
            $scope.document.attachment = data
            if isStandalone() then angular.extend($scope.objforsave, data)
            $scope.document[$scope.name] = data.id
            if $scope.kind == 'title_image'
              $rootScope.title_image = $scope.image

      # Обновление единичного файла
      $scope.update_file = (files, image, version) ->
        local_versions = {}
        local_versions[version] = versions[version] if 'image' == $scope.type
        Agami.upload(_.first(files), {
          path: image.directory,
          id:   image.id
        }, local_versions).then (data) ->
          angular.extend($scope.image, data)

      # Сохранение дополнительных атрибутов для бокса или документа
      $scope.update = (document) ->
        item = if isDocument() then 'documents' else 'boxes'
        Snipe.one(item, $scope.document.id).patch(document)

      $scope.kb = (value) -> Math.ceil(value/1024)

      $scope.remove = ->
        item = if isDocument() then 'documents' else 'boxes'
        struct = {}
        struct[$scope.name] = null
        if isDocument()
          Snipe.one(item, $scope.document.id).patch(struct)
        if isStandalone()
          $scope.objforsave = {}
        $scope.image = {}
        $scope.document[$scope.name] = null
