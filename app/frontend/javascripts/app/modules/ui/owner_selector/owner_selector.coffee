ownerSelectorTemplate = require('./owner_selector.html')

angular.module('ui')
  .directive 'rmOwnerSelector', ->
    restrict: 'E'
    replace: true
    templateUrl: ownerSelectorTemplate
    scope:
      document: '='
      key:      '@'
      title:    '@'
    controller: ($scope, Snipe, $filter, EditorsService, GroupsService) ->
      editors = [{ id: 0, last_name: 'ВСЕ'}]
      groups  = [{ id: 0, title: 'ВСЕ'}]
      
      GroupsService.list().then  (all_groups) -> $scope.groups = _.union(groups, all_groups)
      EditorsService.list().then (all_editors) -> $scope.editors = _.union(editors, all_editors)
  
      $scope.add_group  = (id) -> 
        if 0 == id
          $scope.document[$scope.key] = [0]
        else 
          $scope.document[$scope.key] = _.without($scope.document[$scope.key], 0)
          id *= -1
          $scope.document[$scope.key].push(id) if id not in $scope.document[$scope.key]   
      
      $scope.add_editor = (id) -> 
        if 0 == id
          $scope.document[$scope.key] = [0]
        else
          $scope.document[$scope.key] = _.without($scope.document[$scope.key], 0)
          $scope.document[$scope.key].push(id) if id not in $scope.document[$scope.key]   
    
      $scope.name_by_id = (id) ->
          if 0 == id
            'Все'
          else if id > 0
            item = _.find($scope.editors, id: id)
            item.last_name if item? 
          else
            item = _.find($scope.groups, id: Math.abs(id))
            item.title if item?
            
      $scope.remove = (id) ->
        $scope.document[$scope.key] = $filter('filter')($scope.document[$scope.key], (model) ->
          model != id
        )
