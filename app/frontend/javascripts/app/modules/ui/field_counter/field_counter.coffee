fieldTemplate = require('./field_counter.html')

angular.module('ui')
  .directive 'rmFieldCounter', ->
    restrict: 'E'
    scope:
      value: '='
      max:   '@'
      min:   '@'
    templateUrl: fieldTemplate
    controller: ->
