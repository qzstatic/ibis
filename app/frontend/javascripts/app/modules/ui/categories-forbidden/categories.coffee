categoriesTemplate = require('./categories.html')
categoryTemplate = require('./category.html')

angular.module('ui')
  .directive 'rmCategoriesSelectorForbidden', ->
    restrict: 'E'
    scope:
      currentcategory: '='
      action:  '='
      exclude: '='
    templateUrl: categoriesTemplate

    controller: ($scope, filterFilter, Snipe, Notify, CategoriesService) ->
      $scope.categoryTemplate = categoryTemplate
      
      $scope.$parent._loading.addPromise Snipe.one('categories/tree').get().then (categories) -> 
        $scope.categories_as_tree = categories

      $scope.change = (category) ->
        combinations = $scope.currentcategory.forbidden_combinations
        if category.checked
          if !_.includes(combinations, category.id)
            $scope.currentcategory.forbidden_combinations.push category.id
        else
          $scope.currentcategory.forbidden_combinations = _.without combinations, category.id

      $scope.update = (document) ->
        $scope.currentcategory.forbidden_combinations = _.uniq($scope.currentcategory.forbidden_combinations)
        $scope.$parent.update_or_create($scope.currentcategory)
