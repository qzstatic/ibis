checkboxTemplate = require('./checkbox.html')

angular.module('ui')
  .directive 'rmCheckbox', ->
    restrict: 'E'
    scope:
      state: '='
      default: '@'
    templateUrl: checkboxTemplate
    controller: ($scope) ->
      $scope.state = 'true' == $scope.default unless angular.isDefined($scope.state)
      $scope.toggle = -> $scope.state = !$scope.state
