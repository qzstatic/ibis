dynamicForm = require('./dynamic_form.html')
controlsTemplate = require('./controls.html.slim')

angular.module('ui')
  .directive 'rmDynamicForm', ->
    restrict: 'E'
    scope:
      document: '='
      settings: '='
      action:   '@'
      label:    '@'

    templateUrl: dynamicForm

    compile: (element, attrs) ->
      attrs.action = if angular.isDefined(attrs.action) then 'true' == attrs.action else true
      attrs.label  = if angular.isDefined(attrs.label) then 'true' == attrs.label else false

    controller: ($scope, CategoriesService, current_document, Utils, Notify) ->
      $scope.controlsTemplate = controlsTemplate
      $scope.highlightInput = false

      if $scope.document.datetime_and_flag?
        $scope.document.datetime_and_flag = new Date($scope.document.datetime_and_flag)

      if $scope.document.pay_required_expire?
        $scope.document.pay_required_expire = new Date($scope.document.pay_required_expire)

      $scope.part = current_document.document.part

      $scope.align_types = [
        { label: 'влево',     kind: 'left' }
        { label: 'по центру', kind: 'center' }
      ]

      if $scope.part == 'online'
        $scope.align_types = [
          { label: 'по центру', kind: 'center' }
        ]

      $scope.maps = [
        { json: '/static/map2.json', label: 'Россия' }
      ]

      $scope.datepickerOptions =
        minDate: new Date(current_document.document.published_at)

      $scope.checkDependencies = (fields) ->
        unless fields? then return true
        fields.every (field) -> $scope.document[field]? && $scope.document[field]

      # Обновляет бокс или документ
      $scope.update = (document) -> $scope.$parent.update(document)
      $scope.present_in_column = (control, column_number) -> control.layout_column == column_number || !control.layout_column && 1 == column_number

      $scope.currentDate = new Date()

      $scope.getDate = (name) ->
        if name == 'pay_required_expire'
          return new Date(moment().endOf('day', moment()).add(3, 'days').add(3, 'hours'))
        else
          return new Date()

      $scope.correctTimezone = (document, input) ->
        if input?
          document.datetime_and_flag = new Date(input)

      CategoriesService.list_with_children().then (categories) ->
        $scope.categoriest_child_of = (slug) ->
          category = _.find(categories, slug: slug)
          category_list = []
          angular.forEach category.children, (id) -> category_list.push(categories[id])
          category_list

      $scope.addNewLegend = (legend) ->
        if legend.color_domain? && legend.color_range? && legend.legend?
          legend.color_domain.push(0)
          legend.color_range.push('#ffffff')
          legend.legend.push('')
        else
          $scope.document.legend_map =
            legend_title: legend.legend_title
            color_domain: [0],
            color_range: ['#ffffff'],
            legend: ['']

      $scope.removeLegend = (legend, index) ->
        $scope.document.legend_map.color_domain.splice(index, 1)
        $scope.document.legend_map.color_range.splice(index, 1)
        $scope.document.legend_map.legend.splice(index, 1)

      $scope.replaceQuotes = (val, name) ->
        $scope.document[name] = Utils.replaceQuotes(val)

      $scope.$on 'document.checkRequiredFields', (e, highlight) ->
        $scope.highlightInput = highlight
