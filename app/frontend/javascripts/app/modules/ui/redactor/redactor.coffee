redactorTemplate = require('./redactor.html')

angular.module('ui')
  .directive 'rmRedactor', ->
    restrict: 'E'
    replace: true
    templateUrl: redactorTemplate
    scope:
      embed: '='
      buttons: '='
      focus: '='
    controller: ($scope, $element, Settings, Utils) ->
      if $scope.buttons?
        buttons = $scope.buttons
      else
        buttons = ['html', 'bold', 'italic', 'link', 'image']
      focus = if $scope.focus? then $scope.focus else false
      $($element).redactor
        imageUpload: Settings.agami
        lang: 'ru'
        linebreaks: true
        pastePlainText: true
        buttons: buttons
        # plugins: ['alignment']
        linkTooltip: false
        initCallback: ->
          @code.set $scope.embed
          @$editor.focus() if focus
        changeCallback: -> $scope.embed = @code.get()
        imageUploadCallback: (image, json) ->
          $(image).attr('src', Settings.attachments_host + json.versions.original.url)
        blurCallback: ->
          html = @code.get()
          $scope.embed = Utils.replaceQuotes(html)
          @code.set $scope.embed
        pasteCallback: (html) ->
          html = Utils.replaceQuotes(html)
