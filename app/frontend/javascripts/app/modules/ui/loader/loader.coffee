angular.module('ui')
  .directive 'rmLoader', ->
    restrict: 'A'
    template: '
      <div class="b-module">
        <div ng-show="_loading.active()" class="b-loader" />
        <div ng-transclude ng-hide="_loading.active()" />
      </div>
    '
    replace: true
    transclude: 'element'
    priority: 100
    controller: ($scope, promiseTracker) -> $scope._loading = promiseTracker(minDuration: 450)
