categoriesTemplate = require('./categories.html')
categoryTemplate = require('./category.html')

angular.module('ui')
  .directive 'rmCategoriesSelector2', ->
    restrict: 'E'
    scope:
      document: '='
      categories: '='
      action:  '='
      exclude: '='
    templateUrl: categoriesTemplate

    controller: ($scope, filterFilter, Snipe, Notify, CategoriesService) ->
      $scope.categoryTemplate = categoryTemplate
      # Если это новый документ — создаем поле с категориями
      $scope.categories = [] unless $scope.categories?

      update = ->
        $scope.categories = [] unless $scope.categories?

        # Раскладываем выделенные категории в список
        $scope.list = []
        for id in $scope.categories
          if id > 0
            path = _.first(filterFilter($scope.all_categories, id: id)).path
            path.push(id)
            $scope.list.push(path)
        
        $scope.check_tree(category) for category in $scope.categories_as_tree

      # Получаем общий список категорий
      $scope.$parent._loading.addPromise Snipe.all('categories').getList().then (categories) -> 
        $scope.all_categories = categories
        # Получаем древовидный список категорий
        $scope.$parent._loading.addPromise Snipe.one('categories/tree').get().then (categories) -> 
          $scope.categories_as_tree = categories
          $scope.$watch 'categories', -> update()

      $scope.change = (category) ->
        unless $scope.exclude
          if category.checked
            # Добавление категории и всего дерева вверх
            path = _.clone(category.path)
            path.push(category.id)
            
            for index in [1..path.length]
              $scope.list.push(path[0..-1 * index])
          else
            cleaned = []
            for path in $scope.list
              cleaned.push(path) if -1 == _.indexOf(path, category.id)

            $scope.list = cleaned
          
          saved = angular.copy($scope.categories)
          $scope.categories = _.uniq(_.flatten($scope.list))

          for save in saved
            $scope.categories.push(save) if save < 0            
        else
          if category.checked
            $scope.categories.push(-1 * category.id)
          else
            $scope.categories = _.without($scope.categories, -1 * category.id)

        $scope.check_tree(category) for category in $scope.categories_as_tree

      $scope.update = (document) ->
        $scope.categories = _.uniq($scope.categories)
        $scope.$parent.update(document)

      $scope.checked = (category) -> 
        new_category_id = if $scope.exclude then category.id * -1 else category.id
        -1 != _.indexOf($scope.categories, new_category_id)
      
      $scope.check_tree = (category) ->
        category.checked = $scope.checked(category)
        if category.children?
          for children in category.children
            $scope.check_tree(children)
