iaTemplate = require('./instant_article.html')

angular.module('ui')
  .directive 'rmInstantArticle', ->
    scope:
      title: '='
      header: '='
      illustrator: '='
      rootbox: '='
      document: '='
      settings: '='

    templateUrl: -> iaTemplate

    controller: ($scope, $element, Settings, $rootScope, Snipe, $compile, $timeout, $http, $templateCache, Notify, CategoriesService) ->
      $scope.Settings = Settings

      FB.init
        # ibis.dev 759028370898628
        # vedomosti.ru 1052612788156680
        appId      : '1052612788156680',
        cookie     : true
        xfbml      : true
        version    : 'v2.6'


      $scope.boxes = angular.copy $scope.rootbox.children

      _.map $scope.boxes, (box, index) ->

        if _.includes ['inset_image', 'inset_text'], box.type
          Snipe.one('attachments', box.image_id).get().then (attachment) ->
            box.attachment = attachment

        if box.type == 'gallery'
          _.map box.children, (item) ->
            Snipe.one('attachments', item.image_id).get().then (attachment) ->
              item.attachment = attachment

        if box.type == 'inset_link'
          Snipe.one('documents', box.bound_document_id).get().then (res) ->
            box.title = res.title
            Snipe.one('urls', res.url_id).get().then (url) -> box.url = url.rendered

        if box.type == 'inset_document'
          Snipe.one('documents', box.bound_document_id).get().then (res) ->
            if res.part == 'gallery'
              Snipe.one('documents', box.bound_document_id).all('boxes').getList().then (rootBox) ->
                $scope.boxes[index] = rootBox[0].children[0]
                _.map $scope.boxes[index].children, (item) ->
                  Snipe.one('attachments', item.image_id).get().then (attachment) ->
                    item.attachment = attachment
            else
              Snipe.one('urls', res.url_id).get().then (url) -> box.url = url.rendered

        if $scope.document.url_id?
          Snipe.one('urls', $scope.document.url_id).get().then (url) ->
            $scope.documentUrl = url.rendered

      $scope.publishToFacebook = =>
        $scope.authors = $rootScope.authors
        $scope.title_image = $rootScope.title_image
        html = ''

        CategoriesService.list().then (data) =>
          titles = CategoriesService.titles_by_ids($scope.document.category_ids)
          if titles.split(' Рубрики,')[1]?
            rubric = titles.split(' Рубрики,')[1]
            rubric = rubric.split(', Подрубрики')[0]
            # TODO: subrubrics
            # subrubrics = titles.split('Подрубрики, ') if titles.split('Подрубрики, ')[1]?
            # if subrubrics?
            #   subrubrics.shift()
            #   subrubric = _.reduce subrubrics, (el, nextEl) -> el + nextEl
            # $scope.kicker = rubric + (if subrubric then ', ' + subrubric else '')
            $scope.kicker = rubric
          else
            $scope.kicker = ''
          iaTemplate = $templateCache.get('instantArticleTemplate')
          nodes = $compile(iaTemplate.replace(/\n/g, ''))($scope)

          # Отправляем статью в Facebook
          pushArticleToFacebook = (response) ->
            accessToken = response.authResponse.accessToken
            FB.api '/184860593907/instant_articles', 'post', {
              access_token: accessToken
              html_source: html
              development_mode: 'development' == NODE_ENV
            }, (response) ->
              if !response || response.error
                Notify.add msg: "Произошла ошибка", type: 'error'
              else
                Notify.add msg: "Статья отправлена, ее ID: #{response.id}", type: 'success'

          $timeout ->
            html = _.reduce nodes, ((html, next) -> html + next.outerHTML if next.outerHTML?), ''
            html = '<!DOCTYPE html>' + html.replace(/(custom)(html|body|head)/g, '$2')
            html = html.replace(/\s?ng-binding\s?/g, '')
            html = html.replace(/<a>(.+?)<\/a>/g, '$1')
            FB.getLoginStatus (response) ->
              if response.status == 'connected'
                pushArticleToFacebook(response)
              else if response.status == 'not_authorized'
                FB.login (response) ->
                  if response.status == 'connected'
                    pushArticleToFacebook(response)
                , { scope: 'pages_manage_instant_articles,pages_show_list' }
              else
                FB.login (response) ->
                  if response.status == 'connected'
                    pushArticleToFacebook(response)
                , { scope: 'pages_manage_instant_articles,pages_show_list' }
          , true

        null
