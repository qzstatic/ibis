finderTemplate = require('./document_finder.html')

angular.module('ui')
  .directive 'rmDocumentFinder', ->
    restrict: 'E'
    scope:
      link: '='
      filter: '='
    templateUrl: finderTemplate
    controller: ($scope, Snipe, $http, Settings, Search) ->
      $scope.current_document = Snipe.one('documents', $scope.link).get().$object if $scope.link

      $scope.find_documents = (pattern) -> Search.documents(pattern, $scope.filter)

      $scope.select_document = (document) ->
        $scope.link = document.id
        $scope.current_document = document
