listTemplate = require('./index.html')
editTemplate = require('./edit.html')
viewTemplate = require('./view.html')
dialogTemplate = require('../dialogs/dialog.html')

angular.module('app')
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.pages',
        abstract: true,
        url: '/pages'
        data: 
          permissions: 
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'
        
      .state 'logged.pages.list',
        url: '/list'
        auth: true
        resolve:
          pages: (Snipe) -> Snipe.all('pages').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PagesListController'

      .state 'logged.pages.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          page: (Snipe, $stateParams) -> Snipe.one('pages', $stateParams.id).get()
          lists: (Snipe) -> Snipe.all('lists').getList()
          templates: (Snipe) -> Snipe.all('options/template').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'PageEditController'

      # Страница со списками
      .state 'logged.pages.view',
        url: '/:id/view'
        auth: true
        resolve:
          page: (Snipe, $stateParams) -> Snipe.one('pages', $stateParams.id).get()
          categories: (CategoriesService) -> CategoriesService.list() 
          latest_documents: (Snipe, $stateParams) -> Snipe.one('pages', $stateParams.id).all('last_documents').getList()
          lists: (page, $q, Snipe, CategoriesService) ->
            deferreds = []

            angular.forEach page.list_ids, (id) ->
              deferred = $q.defer()
              deferreds.push(deferred.promise)

              Snipe.one('lists', id).get().then (list) ->
                Snipe.one('lists', list.id).all('documents').getList().then (documents) -> 
                  list.category_slugs = CategoriesService.slugs_by_ids(list.category_ids, ',')
                  list.documents = documents

                  deferred.resolve(list)

            $q.all(deferreds)

        views:
          'content@logged':
            templateUrl: viewTemplate
            controller: 'PageViewController'
      
controllers =
  PagesListController: ($scope, pages, Notify, $filter, Snipe, $uibModal) -> 
    $scope.pages = pages

    reorder = ->
      for page, index in $scope.pages
        page.position = (index + 1) * 1000
        page.patch(position: page.position)

    $scope.sortable_handlers =
      orderChanged: -> 
        reorder()
        Notify.add msg: 'Новый порядок страниц сохранён.', type: 'success'

    $scope.to_edit_mode = (page) -> 
      $scope.inplace_current_page = angular.copy(page)
      page.edit = true

    $scope.rename = (page, current_page) ->
      angular.extend(page, current_page)
      page.edit = false

      page.patch().then ->
        Notify.add type: 'success', msg: 'Страница переименована.'

    $scope.remove = (page) -> 
      modal_instance = $uibModal.open
        templateUrl: dialogTemplate
        controller: ($scope, $uibModalInstance) ->
          $scope.title = 'Удаление страницы'
          $scope.text = 'Страница будет удалена со всеми привязанными к ней списками. Продолжить?'

          $scope.ok = -> $uibModalInstance.close('ok');
          $scope.close = -> $uibModalInstance.dismiss('close');

      modal_instance.result.then (state) ->
        if 'ok' == state
          page.remove().then -> 
            $scope.pages = _.without($scope.pages, page)

            Notify.add type: 'success', msg: 'Страница удалена.'

    $scope.create = (page) ->
      Snipe.all('pages').post(attributes: page).then (json) -> 
        Notify.add type: 'success', msg: 'Страница создана.'
        $scope.pages.push(json)
        page.title = ''
        page.slug = ''

        reorder()

  PageEditController: ($scope, page, Snipe, lists, $filter, Settings, ListSettings, $uibModal, templates) ->
    $scope.page = page
    $scope.lists = lists
    $scope.current_list =
      cols: 4
      limit: 10
      category_ids: []
      duration: 0
    

    $scope.templates = templates

    $scope.sortable_handlers =
      orderChanged: -> $scope.page.patch(list_ids: $scope.page.list_ids)


    $scope.list_included = (list) -> 
      list.id in $scope.page.list_ids
    
    $scope.new_list = ->
      $scope.current_list =
        cols: 4
        limit: 10
        category_ids: []
        duration: 0
      

    $scope.include_list = (list) ->
      if list.id in page.list_ids
        page.list_ids = _.without(page.list_ids, list.id)
      else
        page.list_ids.push(list.id)
  
      page.patch(list_ids: page.list_ids)

    $scope.list_by_id = (id) ->
      for list in $scope.lists
        return(list) if list.id == id
          
    $scope.create = (current_list) ->
      if current_list.id?
        Snipe.one('lists', current_list.id).patch(current_list).then -> 
          found = _.find($scope.lists, id: current_list.id)
          angular.extend(found, current_list)
          $scope.current_list =
            cols: 4
            limit: 10
            category_ids: []
            duration: 0
      else
        Snipe.all('lists').post(attributes: current_list).then (json) -> 
          $scope.lists.push(json)
          $scope.current_list = 
            cols: 4
            limit: 10
            category_ids: []
            duration: 0

    $scope.remove = (list) ->
      modal_instance = $uibModal.open
        templateUrl: dialogTemplate
        controller: ($scope, $uibModalInstance) ->
          $scope.title = 'Удаление списка'
          $scope.text = 'Список будет удален. Вот вообще удален, со всех строниц. Продолжить?'

          $scope.ok = -> $uibModalInstance.close('ok');
          $scope.close = -> $uibModalInstance.dismiss('close');

      modal_instance.result.then (state) ->
        if 'ok' == state
          list.remove()
          $scope.lists = $filter('filter')($scope.lists, (model) ->
            model != list
          )

    $scope.edit = (list) ->
      $scope.current_list = angular.copy(list)

  PageViewController: ($scope, page, categories, Snipe, $filter, ListSettings, lists, latest_documents, Notify, Search, CategoriesService) ->
    row_cols = 0

    $scope.latest_documents = latest_documents
    $scope.page             = page
    $scope.lists            = lists

    highlight = (document = null) ->
      if document && document != $scope.latest_document
        document.highlight = true
        document.need_commit = true
        $scope.latest_document.highlight = false if $scope.latest_document
        $scope.latest_document = document
      else if !document && $scope.latest_document
        $scope.latest_document.highlight = false
        $scope.latest_document = null

    $scope.document_kind = (kind) ->
      kinds =
        article: 'статья'
        news: 'новость'
        gallery: 'галерея'
        character: 'интервью'
        column: 'колонка'
        blog: 'блог'
        quote: 'цитата'
        video: 'видео'

      kinds[kind] || kind

    # Поиск документов
    $scope.find_documents = (pattern, list) -> Search.documents(pattern, list.category_slugs.split(','))
    
    $scope.select_document = (list, document) ->
      list.selected_document = null
      if _.find(list.documents, id: document.id)
        Notify.add msg: 'Документ уже находится в этом списке, добавить его еще раз нельзя.', type: 'error'
      else
        Snipe.one('lists', list.id).one('item').get(document_id: document.id).then (list_item) ->
          document.list_item = list_item
          list.documents.push(document)
          list.need_commit = true

          Notify.add msg: 'Документ добавлен в список.', type: 'success'
        , -> Notify.add msg: 'Выбранный документ нельзя добавить в этот список.', type: 'error'

    $scope.change_top = (document, list, direction) ->
      document.list_item.top += direction
      document.need_commit = true
      list.need_commit = true

      highlight(document)

    # Сохранение состояния топов
    $scope.commit_list_changes = (list) ->
      list.need_commit = false

      for document in list.documents
        if document.need_commit
          Snipe.one('list_items', document.list_item.id).patch(top: document.list_item.top).then -> document.need_commit = false

      highlight()
      Notify.add msg: 'Изменения в списке сохранены.', type: 'success'

    # Рассчитывается коэффициент для топа
    $scope.effective_timestamp = (list, document) ->
      new Date(document.published_at).getTime() + (document.list_item.top || 0) * list.duration * 1000 if list? && document?
 
    $scope.order = (list) ->
      return (document) -> $scope.effective_timestamp(list, document)

    # Проверяет заполненность линии 
    $scope.fill_row = (cols) ->
      if row_cols + cols >= 12
        row_cols = 0
        true
      else
        row_cols += cols
        false

angular.module('pages', ['ui']).controller(controllers)
