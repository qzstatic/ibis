listTemplate = require('./list.html')
showTemplate = require('./show.html')

angular.module('app')
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.directories',
        abstract: true,
        url: '/directories'
        data: 
          permissions: 
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'
            
      .state 'logged.directories.list',
        url: '/list'
        auth: true
        resolve:
          directories: (Snipe) -> Snipe.one('options/types').get()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'DirectoriesListController'
        
      .state 'logged.directories.show',
        url: '/:id/edit'
        auth: true
        resolve:
          directories: (Snipe) -> Snipe.one('options/types').get()
          directory: (Snipe, $stateParams) -> Snipe.all("options/#{$stateParams.id}").getList()
        views:
          'content@logged':
            templateUrl: showTemplate
            controller: 'DirectoriesShowController'
      
controllers =
  DirectoriesShowController: ($scope, $stateParams, directory, directories, Snipe) ->
    $scope.directory      = directory
    $scope.directories    = directories
    $scope.directory_slug = $stateParams.id

    $scope.current_directory_type = _.find(directories, type: $stateParams.id)

    $scope.reset = ->
      $scope.current_item   =
        type: $stateParams.id

    $scope.reset()

    $scope.edit = (item) -> item.edit = true

    $scope.update = (item) ->
      Snipe.one('options', item.id).patch(item)
      item.edit = false

    $scope.create = (new_item) ->
      new_item.position = _.last($scope.directory).position + 1000
      Snipe.all('options').post(attributes: new_item).then (created_item) ->
        $scope.directory.push created_item
        $scope.reset()

    reorder = ->
      for item, index in $scope.directory
        item.position = ( index + 1 ) * 1000
        Snipe.one('options', item.id).patch(item)

    $scope.sortable_handlers =
      orderChanged: ->
        reorder()

  DirectoriesListController: ($scope, directories) ->
    $scope.directories = directories

angular.module('directories', []).controller(controllers)
