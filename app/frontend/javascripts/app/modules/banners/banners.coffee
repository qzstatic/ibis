listTemplate = require('./list.html.slim')
newTemplate = require('./new.html.slim')
editTemplate = require('./edit.html.slim')
formTemplate = require('./_form.html.slim')

angular.module('banners', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.banners',
        abstract: true,
        url: '/banners'
        data:
          permissions:
            except: ['EDITOR']
            redirectTo: 'errors.403'
      .state 'logged.banners.list',
        url: '/list/:page'
        auth: true
        resolve:
          banners: ($stateParams, Aquarium) -> Aquarium.one('mailing/banners').get(page: $stateParams.page || 1).then (banners) -> banners
          clicks: (Aquarium) -> Aquarium.one("mailing/stats/banners_clicks").get()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'BannersListController'

      .state 'logged.banners.new',
        url: '/new'
        auth: true
        resolve:
          types: (Aquarium) -> Aquarium.all('mailing/mail_types').getList()
          banner: (Aquarium, $stateParams) ->
            return {
              type: 'text',
              embed: ''
            }
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'BannersNewOrEditController'

      .state 'logged.banners.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          banner: (Aquarium, $stateParams) -> Aquarium.one('mailing/banners', $stateParams.id).get().then (banner) ->
            banner.start_date = new Date banner.start_date
            banner.end_date = new Date banner.end_date
            banner
          types: (Aquarium) -> Aquarium.all('mailing/mail_types').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'BannersNewOrEditController'

  .controller 'BannersListController', ($scope, $state, banners, clicks, $localStorage, $http, Aquarium, Search, Notify, Settings) ->
    $scope.Settings = Settings
    $scope.meta = banners.meta
    $scope.to_page = () ->
      $state.go('logged.banners.list', page: $scope.meta.current_page)

    $scope.clear_search = () ->
      $scope.search = {}
      $scope.find()

    $scope.find = () ->
      Aquarium.one('mailing/banners').get($scope.search).then (data) ->
        $scope.banners = data.banners
        $scope.meta = data.meta
        updateClicks $scope.banners

    $scope.banners = banners.banners
    updateClicks = (banners) ->
      _.map banners, (banner) ->
        _.map clicks.clicks_by_banner, (click) ->
          if banner.id == +click.key then banner.clicks = click.doc_count
    updateClicks banners.banners

  .controller 'BannersNewOrEditController', ($element, $scope, $localStorage, Aquarium, Search, Notify, types, $state, Settings, Agami, banner, $stateParams) ->

    $scope.formTemplate = formTemplate

    $scope.types = types
    $scope.banner = banner

    $scope.attachments_host = Settings.attachments_host
    $scope.bannerTypes = ['text', 'picture', 'embed']

    $scope.uploadImage = (files) ->
      Agami
        .upload(_.first(files))
        .then (data) ->
          $scope.banner.picture = data.versions.original

    $scope.createOrUpdate = (banner) ->
      type = banner.type

      if type == 'text'
        banner.embed = ''
        banner.picture = {}
      if type == 'embed'
        banner.body = ''
        banner.picture = {}
      if type == 'picture'
        banner.body = ''
        banner.embed = ''

      if $stateParams.id
        banner
          .patch()
          .then (newBanner) ->
            Notify.add msg: 'Баннер изменен.', type: 'success'
            $state.go('logged.banners.list')
      else
        Aquarium
          .all('mailing/banners')
          .post(attributes: banner)
          .then (createdBanner) ->
            Notify.add msg: 'Баннер добавлен.', type: 'success'
            $state.go 'logged.banners.list'
