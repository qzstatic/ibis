indexTemplate = require('./index.html')

angular.module('app')

  .config ($stateProvider) ->

    $stateProvider

      .state 'logged.wiki',
        url: '/wiki'
        auth: true
        views:
          'content@logged':
            templateUrl: indexTemplate
            controller: 'WikiController'
  
controllers =
  WikiController: -> 

angular.module('wiki', []).controller(controllers)
