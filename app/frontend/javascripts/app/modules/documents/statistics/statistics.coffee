statisticsTemplate = require('./statistics.html.slim')

angular.module('documents')
  .directive 'rmStatisticsModule', ->
    templateUrl: statisticsTemplate
  .controller
    DocumentsStatisticsController: ($scope, Barbus, $timeout, $interval, document) ->
      ru_RU =
        decimal: ",",
        thousands: "\u00A0",
        grouping: [3],
        currency: ["", " руб."],
        dateTime: "%A, %e %B %Y г. %X",
        date: "%d.%m.%Y",
        time: "%H:%M:%S",
        periods: ["AM", "PM"],
        days: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        shortDays: ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
        months: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
        shortMonths: ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]

      $scope.document = document

      $scope.chart =
        api: {}
        data: []
        config:
          refreshDataOnly: true
        options:
          chart:
            interpolate: 'monotone'
            duration: 500
            type: 'lineChart'
            height: 500
            margin:
              bottom: 80
            useInteractiveGuideline: true
            noData: 'Нет данных'
            xAxis:
              tickFormat: (d) -> d3.locale(ru_RU).timeFormat($scope.format)(new Date(d))
              rotateLabels: -65
              showMaxMin: false
            yAxis:
              axisLabel: 'Количество просмотров'
            forceY: [0]
            x: (d) -> new Date(d.x)
            y: (d) -> d.y

      $scope.type = 'common'
      $scope.format = '%Y-%b-%d %H:%M'

      $scope.showStatistics = true
      $scope.spinLoader = false

      $scope.startDate = new Date(moment(document.published_at).format('YYYY-MM-DD'))
      $scope.endDate = new Date(moment().format('YYYY-MM-DD'))
      $scope.interval = '5m'
      $scope.allowedIntervals = ['1h', '5m']

      $scope.datepickerOptions =
        minDate: moment(new Date(document.published_at))
        maxDate: moment(new Date())

      $scope.checkAllowedInterval = (interval) ->
        interval in $scope.allowedIntervals

      $scope.setType = (type) ->
        $scope.type = type
        $scope.renderChart()

      $scope.getTickValues = (series) ->
        # чтобы сетка не разъезжалась, тултипы дат собираются и назначаются вручную (не более 20)
        allTickLabels = _.map series[0].values, (value) -> new Date(value.x)
        if allTickLabels.length <= 20 then return allTickLabels

        step = Math.ceil(allTickLabels.length / 20)
        tickLables = []
        allTickLabels.forEach (value, i) -> if i % step == 0 || i == 0 then tickLables.push value
        tickLables

      $scope.chart.options.chart.xAxis.tickValues = $scope.getTickValues

      $scope.spin = () ->
        $scope.spinLoader = true
        setTimeout (=> $scope.spinLoader = false), 500

      $scope.isAllowedToUpdate = ->
        moment($scope.endDate).isBetween moment().startOf('day', moment()), moment().endOf('day', moment())

      $scope.updateChart = ->
        if $scope.isAllowedToUpdate()
          $scope.renderChart()

      $scope.renderChart = () ->
        $scope.spin()
        $interval.cancel $scope.updateInterval
        startDate = angular.copy $scope.startDate
        endDate = angular.copy $scope.endDate
        interval = angular.copy $scope.interval
        type = angular.copy $scope.type

        endDate = moment(endDate).add(1, 'day').subtract(1, 'seconds')

        if moment(startDate).diff(endDate, 'hours') >= -48
          $scope.allowedIntervals = ['1h', '5m']
          $scope.interval = interval = '1h' if !$scope.checkAllowedInterval interval
        else if moment(startDate).diff(endDate, 'hours') >= -168
          $scope.allowedIntervals = ['1d', '1h']
          $scope.interval = interval = '1h' if !$scope.checkAllowedInterval interval
        else if moment(startDate).diff(endDate, 'months') > -5
          $scope.allowedIntervals = ['1w', '1d', '1h']
          $scope.interval = interval = '1d' if !$scope.checkAllowedInterval interval
        else if moment(startDate).diff(endDate, 'months') <= -5
          $scope.allowedIntervals = ['1M', '1w', '1d']
          $scope.interval = interval = '1w' if !$scope.checkAllowedInterval interval


        $scope.format = switch interval
          when '1d' or '1w' then '%d %b %Y'
          when '1M' then '%b %Y'
          else '%d %b %H:%M'

        seriesNames = switch type
          when 'common' then ['Просмотры', 'Соц. сети']
          when 'visits' then ['Просмотры']
          when 'social' then ['Twitter', 'Facebook', 'VK']
          else null

        colors = ['#2ca02c', '#7777ff', '#ff7f0e']

        startDate = moment(startDate).utc().subtract(3, 'hours').format()
        endDate = moment(endDate).utc().subtract(3, 'hours').format()

        # Собираем данные
        buildData = (series) ->
          splittedInterval = interval.split('')
          data = []

          series.forEach (item, i) ->
            seriesData = []
            deltaDate = angular.copy startDate
            count = 0

            # баг сервиса, возвращающего данные: если интервал >= 1d, даты приходят со смещением в 3 часа
            if splittedInterval[1] == 'w'
              deltaDate = moment(deltaDate).startOf('week').add(3, 'hours')
            else if splittedInterval[1] == 'M'
              deltaDate = moment(deltaDate).startOf('month').add(3, 'hours')
            else if splittedInterval[1] == 'd'
              deltaDate = moment(deltaDate).add(3, 'hours')

            # формирование сетки
            while moment(endDate).unix() >= moment(deltaDate).unix()
              formattedDeltaDate = moment(deltaDate).utc().format('YYYY-MM-DDTHH:mm:ss.000') + 'Z'
              foundDateIndex = _.findIndex item, key_as_string: formattedDeltaDate
              # заполняем нулями значения для дат, не пришедших с сервера, либо отрицательных значений ( и такое может быть:) )
              seriesData.push({y: (if foundDateIndex != -1 && item[foundDateIndex].doc_count > 0 then item[foundDateIndex].doc_count else 0), x: new Date deltaDate})
              count += if foundDateIndex != -1 then item[foundDateIndex].doc_count else 0
              deltaDate = moment(deltaDate).add(+splittedInterval[0], splittedInterval[1])

            data.push values: seriesData, key: "#{seriesNames[i]} (#{count})", color: colors[i]

          # График полностью обновляется, когда количество линий или точек становится больше
          # Это нужно, чтобы избежать искажений в отображении гайдлайна и значений на линиях при наведении на график
          if $scope.chart.api.refreshWithTimeout
            refresh = data.length > $scope.chart.data.length || $scope.chart.data[0]? && data[0].values.length > $scope.chart.data[0].values.length
            $scope.chart.data = data
            $scope.chart.api.refreshWithTimeout 5 if refresh
          else
            $scope.chart.data = data

        if type == 'common'
          Barbus
            .one('documents', document.id)
            .one('statistics')
            .all("visits?date_from=#{startDate}&date_to=#{endDate}&interval=#{interval}")
            .getList()
            .then (visits) ->
              Barbus
                .one('documents', document.id)
                .one('statistics')
                .all("social?date_from=#{startDate}&date_to=#{endDate}&interval=#{interval}")
                .getList()
                .then (social) ->
                  socialPrepared = social.map (item) -> key_as_string: item.key_as_string, doc_count: (item.fb_comment_count.value + item.fb_share_count.value + item.vk_share_count.value + item.twitter_share_count.value)
                  buildData [visits, socialPrepared]
        else
          Barbus
            .one('documents', document.id)
            .one('statistics')
            .all("#{$scope.type}?date_from=#{startDate}&date_to=#{endDate}&interval=#{interval}")
            .getList()
            .then (res) ->
              if type == 'visits' then return buildData [res]

              #Данные из Facebook нужно отображать в сумме
              fields = [['twitter_share_count'], ['fb_comment_count', 'fb_share_count'], ['vk_share_count']]
              buildData fields.map (fieldSet, i) -> res.map (item) ->
                doc_count = 0
                fieldSet.forEach (field) -> doc_count += item[field].value
                doc_count: doc_count, key_as_string: item.key_as_string


        $scope.updateInterval = $interval $scope.updateChart, 60*1000

      $scope.renderChart()

      $scope.$on '$destroy', () =>
        $interval.cancel $scope.updateInterval
