historyTemplate = require('./history.html')

angular.module('documents')
  .directive 'rmDocumentHistory', ->
    restrict: 'E'
    replace: true
    templateUrl: historyTemplate
    scope:
      document: '='
    controller: ($scope, Snipe, Restangular, $state, Notify) ->
      $scope.range = []

      $scope.records = Snipe.one('documents', $scope.document.id).all('snapshots').getList().$object

      $scope.disabled = (record) -> 2 == $scope.range.length && record != $scope.range[0] && record != $scope.range[1]

      $scope.change_point = (record) ->
        if record.selected
          $scope.range.push(record) if $scope.range.length < 2
          $scope.range = _.sortBy($scope.range, (model) -> model.created_at)

          if 2 == $scope.range.length
            ids = _.map($scope.range, 'id')
            Snipe.all("snapshots/#{ids[0]}..#{ids[1]}").customGET().then (html) -> $scope.diff = html
        else
          $scope.range = _.without($scope.range, record)
          $scope.diff = null

      $scope.checkRecord = (record, $index) ->
        if $state.current.name != 'logged.documents.history-proofread'
          return
        if $index == 0 || record.action == 'document.proofread'
          record.selected = true
          $scope.change_point(record)
      
      $scope.restoreBox = (record) ->
        Snipe
          .one('boxes', record.subject_id)
          .one('restore')
          .patch()
          .then () ->
            Notify.add msg: "Бокс восстановлен!", type: 'success'
