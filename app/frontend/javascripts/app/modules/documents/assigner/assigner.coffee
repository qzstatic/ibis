assignerTemplate = require('./assigner.html')
dialogTemplate = require('./dialog.html')

angular.module('documents')
  .directive 'rmAssignerDocumentModule', ->
    restrict: 'E'
    replace: true
    scope:
      document: '='
    templateUrl: assignerTemplate
    controller: ($scope, Snipe, $filter, Notify, $uibModal, Editor, EditorsService) ->
      # Сохраняем текущего редактора
      prev_editor_id = $scope.document.assigned_editor_id
      
      # Формирует название групп
      $scope.group_from = (editor) -> if editor.enabled then 'Активные' else 'Неактивные'  
      
      EditorsService.list().then (editors) ->    
        $scope.editors = $filter('orderBy')(editors, 'full_name')
        $scope.editors = $filter('orderBy')($scope.editors, 'enabled', true)
      
      # Сохраняет редактора в базе
      set_editor = (editor, status = 'disabled') ->
        $scope.document.assigned_editor_id = editor.id
        Snipe.one('documents', $scope.document.id).one('assign').patch(editor_id: editor.id).then ->
          Notify.add(type:'success', msg: "У документа установлен редактор: #{editor.full_name}.")          
          prev_editor_id = $scope.document.assigned_editor_id
          $scope.document.assignee_status = 'enabled'
      
      $scope.update_status = ->
        $scope.document.one('complete').patch().then ->
          Notify.add msg: 'Редактирование завершено.', type: 'success'
          $scope.document.assignee_status = 'finished'

      # Обновление редактора
      $scope.update = (new_editor_id) ->
        editor = _.first($filter('filter')($scope.editors, { id: new_editor_id }))

        if editor?
          if editor.enabled
            set_editor(editor)
          else
            modal_handler = $uibModal.open
              templateUrl: dialogTemplate
              controller: ($scope, $uibModalInstance) ->
                $scope.close = -> $uibModalInstance.close(false)
                $scope.ok    = -> $uibModalInstance.close(true)
                  
            modal_handler.result.then (selected) ->
              if selected
                set_editor(editor)
              else
                $scope.document.assigned_editor_id = prev_editor_id

      # При публикации документа нам нужно добавить к нему редактора, если редактора еще нет
      $scope.$on 'document.publish', (e, args) ->
        unless $scope.document.assigned_editor_id?
          Snipe.one('documents', $scope.document.id).one('assign').patch(editor_id: Editor.id()).then ->
            $scope.document.assigned_editor_id = Editor.id()
