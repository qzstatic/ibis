listsTemplate = require('./lists_excluder.html')

angular.module('documents')
  .directive 'rmListsExcluder', ->
    restrict: 'E'
    replace: true
    templateUrl: listsTemplate
    scope:
      document: '='
    controller: ($scope, Snipe) ->
      $scope.lists = []
      reload = -> Snipe.one('documents', $scope.document.id).one('matching_lists').getList().then (lists) -> $scope.lists = lists 
      reload()

      $scope.$on 'document.settings.update', -> reload()

      $scope.toggle = (list) ->
        if $scope.excluded(list)
          $scope.document.exclude_list_ids = _.without($scope.document.exclude_list_ids, list.id)
        else
          $scope.document.exclude_list_ids.push(list.id)

        Snipe.one('documents', $scope.document.id).patch(exclude_list_ids: $scope.document.exclude_list_ids)

      $scope.excluded = (list) -> list.id in $scope.document.exclude_list_ids
