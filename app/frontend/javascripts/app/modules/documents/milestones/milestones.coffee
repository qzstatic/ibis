milestonesTemplate = require('./milestones.html')

angular.module('documents')
  .directive 'rmDocumentMilestones', ->
    restrict: 'E'
    replace: true
    templateUrl: milestonesTemplate
    scope:
      document: '='
    controller: ($scope, EditorsService) ->
      $scope.records = _.map $scope.document.milestones, (milestone, key) ->
        angular.extend milestone, type: key, editor: EditorsService.by_id(milestone.by).full_name
