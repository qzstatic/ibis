angular.module('documents').factory 'doc_settings', ()->
  class DocumentSettings
    set: (document) -> 
      @settings = {} unless @settings?
      angular.copy(document.settings, @settings)
      
  new DocumentSettings
