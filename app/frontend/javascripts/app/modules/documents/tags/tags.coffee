tagsTemplate = require('./tags.html')

angular.module('documents')
  .directive 'rmTagsDocumentModule', ->
    restrict: 'E'
    replace: true
    scope:
      document: '='
    templateUrl: tagsTemplate
    controller: ($scope, $q, Snipe, Notify, $filter) ->
      $scope.new_tags = []
      $scope.taggings = {}
      $scope.index = 0
      
      # Загружаем данные — группы тегов
      $scope.$parent._loading.addPromise Snipe.one('documents', $scope.document.id).all('taggings').getList().then (taggings) ->
        angular.forEach taggings, (tagging) ->
          $scope.taggings[tagging.type] = tagging
 
          # Теги для каждой группы
          $scope.$parent._loading.addPromise Snipe.one('taggings', tagging.id).all('tags').getList().then (tags) ->
            tagging.tags = tags

      $scope.update_tagging = (tagging, found_tag) ->
        # Определяем, возможно такой тег уже есть в группе
        if -1 == _.indexOf(tagging.tag_ids, found_tag.id)
          tagging.tag_ids.push(found_tag.id)
          tagging.tags.push(found_tag) 

          Snipe.one('taggings', tagging.id).patch(tagging)
        else
          Notify.add msg: 'Тег уже есть в группе! Не надо так.', type: 'error'

        $scope.new_tags[tagging.type] = ''

      $scope.add = (type, new_tag) ->
        # Тег должен быть введен
        unless '' == new_tag
          # Ищем или создаем тег
          Snipe.all('tags').get('find_by_title', title: new_tag).then (found_tag) ->
              deferred = $q.defer()
              deferred.resolve(found_tag)

              deferred.promise
            , -> Snipe.all('tags').post(attributes: { title: new_tag })
          .then (found_tag) ->
            # Создаем группу если ее нет или используем уже созданную
            if (tagging = $scope.taggings[type])?
              $scope.update_tagging(tagging, found_tag)
            else
              Snipe.one('documents', $scope.document.id).all('taggings').post(attributes: { type: type }).then (tagging) ->
                $scope.taggings[type] = angular.extend(tagging, tags: [])
                $scope.update_tagging(tagging, found_tag)

      # Удаление тега из группы
      $scope.remove = (type, tag) ->
        tagging         = $scope.taggings[type]
        tagging.tags    = _.without(tagging.tags, tag)
        tagging.tag_ids = _.without(tagging.tag_ids, tag.id)

        Snipe.one('taggings', tagging.id).patch(tagging)
      
      $scope.find_tags = (tag, section) ->
        Snipe.all('tags/search').getList(search_string: tag).then (found_tags) -> 
          $filter('filter')(found_tags, (tag) ->
            tags_for_section = $scope.taggings[section].tag_ids
            tag.id not in tags_for_section
          )
