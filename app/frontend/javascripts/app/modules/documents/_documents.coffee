newTemplate = require('./new.html')
editTemplate = require('./edit.html.slim')
historyTemplate = require('./history.html')
dialogTemplate = require('./dialog.html')
dialogWithRequiredFieldsTemplate = require('./dialog-required-fields.html.slim')
statisticsTemplate = require('./statistics/statistics.html.slim')

angular.module('documents', ['ui.router', 'boxes', 'ui'])
  .config ($stateProvider) ->

    checkRevision = ($http, $window, $rootScope) ->
      $http
        .get('/revision')
        .then (res) ->
          if $rootScope.revision
            if $rootScope.revision != res.data.revision
              $window.location.reload()
          else
            $rootScope.revision = res.data.revision

    $stateProvider

      .state 'logged.documents',
        abstract: true
        url: '/documents'
        data:
          permissions:
            except: ['SUBSCRIPTION']
            redirectTo: 'errors.403'

      .state 'logged.documents.new',
        url: '/new'
        auth: true
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'DocumentsNewController'
        onEnter: checkRevision

      .state 'logged.documents.new_from_categories',
        url: '/new/:categories'
        auth: true
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'DocumentsNewController'

      .state 'logged.documents.edit',
        url: '/:id/edit'
        auth: true
        onEnter: checkRevision
        onExit: (Chatter, current_document, BoxesService) ->
          Chatter.unsubscribe_document(current_document.prev_document_id)
        resolve:
          document: (current_document, $stateParams) -> current_document.load($stateParams.id)
          root_box: (Snipe, $stateParams) ->
            Snipe.one('documents', $stateParams.id).one('root_box').get().then ->
              # Получаем рутовый бокс со всейми остальными
              Snipe.one('documents', $stateParams.id).all('boxes').getList()
          deletedBoxes: (Snipe, $stateParams) ->
            Snipe.one('documents', $stateParams.id).all('boxes?deleted=true').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'DocumentsEditController'

      .state 'logged.documents.history',
        url: '/:id/history'
        auth: true
        resolve:
          document: (current_document, $stateParams) -> current_document.load($stateParams.id)
        views:
          'content@logged':
            templateUrl: historyTemplate
            controller: 'DocumentsHistoryController'

      .state 'logged.documents.history-proofread',
        url: '/:id/history-proofread'
        auth: true
        resolve:
          document: (current_document, $stateParams) -> current_document.load($stateParams.id)
        views:
          'content@logged':
            templateUrl: historyTemplate
            controller: 'DocumentsHistoryController'

      .state 'logged.documents.statistics',
        url: '/:id/statistics'
        auth: true
        views:
          'content@logged':
            templateUrl: statisticsTemplate
            controller: 'DocumentsStatisticsController'
        resolve:
          document: (current_document, $stateParams) -> current_document.load($stateParams.id)

  .controller

    DocumentsHistoryController: ($scope, document) -> $scope.document = document

    # Создание документа
    DocumentsNewController: ($scope, $rootScope, $state, Snipe, $stateParams, $timeout, DocumentSettings) ->
      $scope.document = {}
      $scope.title = DocumentSettings.title

      if $stateParams.categories?
        categories = angular.copy($stateParams.categories).toString()
        $scope.document.category_ids = categories.split(',').map (category_id) -> +category_id

      $scope.create = (document) ->
        $scope.in_progress = true
        $scope.feed = Snipe.all('documents').post(attributes: document).then (created_document) ->
          $rootScope.newDocument = true
          $state.go('logged.documents.edit', id: created_document.id)

    # Редактирование документа
    DocumentsEditController: (Chatter, $timeout, EditorsService, $scope, document, Editor, Snipe, Notify, $uibModal, root_box, $rootScope, current_document, $document, Settings, BoxesService, $state, PartsService, deletedBoxes, LinksService) ->

      # Проверяем, есть ли удаленные боксы которые не опубликованы
      if deletedBoxes.length
        _.map deletedBoxes[0].children, (box) ->
          if !_.isEmpty box.packed_box then BoxesService.addDeleted box.id

      stopTriggerStateChange = false
      isDialogShowed = false

      # Диалог если есть неопубликованные/несохраненные боксы
      showDialogBeforeLeave = (event, toState, toParams) ->
        isDialogShowed = true
        dialogBeforeLeave = $uibModal.open
          templateUrl: dialogTemplate
          controller: ($scope, $uibModalInstance) ->
            $scope.title = 'Вы уходите!'
            text = ''
            if BoxesService.totalUnpublishedBoxes() > 0
              text += 'У вас есть неопубликованные боксы. <br>'
            if BoxesService.totalUnsavedBoxes() > 0
              text += 'У вас есть несохраненные боксы. <br>'
            if BoxesService.totalDeletedBoxes() > 0
              text += 'Вы удалили несколько боксов и не опубликовали документ. <br>'
            if PartsService.totalUnpublished() > 0
              text += 'У вас есть неопубликованные поля документа. <br>'
            if PartsService.totalUnsaved() > 0
              text += 'У вас есть несохраненные поля документа. <br>'
            if LinksService.totalUnpublished() > 0
              text += 'У вас есть неопубликованные ссылки. <br>'
            $scope.text = text + 'Вы действительно хотите уйти?'
            $scope.okText = 'Уйти'
            $scope.cancelText = 'Остаться'
            $scope.ok = -> $uibModalInstance.close('ok')
            $scope.close = ->
              $uibModalInstance.dismiss('close')
              isDialogShowed = false
        dialogBeforeLeave.result.then (state) ->
          if 'ok' == state
            stopTriggerStateChange = true
            BoxesService.clearUnsaved()
            BoxesService.clearUnpublished()
            BoxesService.clearDeleted()
            PartsService.clearUnsaved()
            PartsService.clearUnpublished()
            LinksService.clearUnpublished()
            LinksService.clearEmpty()
            $state.go(toState, toParams)

      $scope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
        hasUnpublished = BoxesService.totalUnpublishedBoxes() > 0 || PartsService.totalUnpublished() > 0 || LinksService.totalUnpublished() > 0
        hasUnsaved = BoxesService.totalUnsavedBoxes() > 0 || PartsService.totalUnsaved() > 0
        hasDeleted = BoxesService.totalDeletedBoxes() > 0
        if (hasUnpublished || hasUnsaved || hasDeleted) && !stopTriggerStateChange
          event.preventDefault()
          if !isDialogShowed then showDialogBeforeLeave(event, toState, toParams)
        return

      $scope.bound_documents = document.all('bound').getList().$object
      $scope.document = document
      $rootScope.root_box = $scope.root_box = _.first(root_box)
      $scope.parts = {}

      Chatter.subscribe_to_document(document.id)
      current_document.prev_document_id = document.id

      $scope.to_twitter = ($event, data) ->
        $event.stopPropagation()

        leftvar = ( screen.width - 800 ) / 2;
        topvar = ( screen.height - 600 ) / 2;
        params = 'toolbar=0,status=0, top=' + topvar + ', left=' + leftvar + ',width=800,height=600';
        twindow = window.open('/', 'twitwin', params);

        Snipe.one('urls', current_document.document.url_id).get().then (url) ->
          Snipe.one('documents', $scope.document.id).one('title_data').get().then (data) ->
            twindow.document.location.href = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(data.data.title) + '&url=http://' + url.rendered;

      requestsWithoutResponse = 0
      $scope.isDisablePublishButton = false

      $rootScope.$on 'document.disablePublishButton', ($event, status) ->
        if (status)
          requestsWithoutResponse += 1
          if requestsWithoutResponse == 1 then $scope.isDisablePublishButton = status
        else
          requestsWithoutResponse -= 1
          if !requestsWithoutResponse then $scope.isDisablePublishButton = status

      # Перезагрузка текущего документа
      reload = ->
        current_document.reload().then ->
          $scope.document = current_document.document

      $scope.reload = reload

      $scope.preview = (document) ->
        document.all('preview').patch().then ->
          window.document.location.href = "#{Settings.shark_url}preview/#{document.id}"

      # Снятие документа с публикации
      $scope.unpublish = (document) ->
        modal_instance = $uibModal.open
          templateUrl: dialogTemplate
          controller: ($scope, $uibModalInstance) ->
            $scope.title = 'Перемещение в черновики'
            $scope.text = 'После помещения в черновики документ будет недоступен. Продолжить?'

            $scope.ok = -> $uibModalInstance.close('ok');
            $scope.close = -> $uibModalInstance.dismiss('close');

        modal_instance.result.then (state) ->
          if 'ok' == state
            document.one('unpublish').patch().then ->
              reload().then -> $scope.$broadcast 'document.unpublish', id: $scope.document.id
              Notify.add type: 'success', msg: 'Документ снят с публикации.'

      # Отслеживаем изменения данных в title, header и illustrator
      $scope.$on 'document.partDataLoaded', (e, data) ->
        $scope.parts[data.part] = angular.copy data

      # Проверяем, есть ли незаполненные поля
      $scope.checkRequiredFieldsBeforePublish = (document) ->
        checkTest = (box) ->
          emptyFields = []
          wrongQuestions =
            choices:
              data: []
              postLabel: ' меньше 2-ух вариантов ответа'
              preLabel: 'В вопросах № '
            text:
              data: []
              postLabel: ' не заполнено поле "Текст вопроса"'
              preLabel: 'В вопросах № '
            rightText:
              data: []
              postLabel: ' не заполнено поле "Текст при правильном ответе"'
              preLabel: 'В вопросах № '
            wrongText:
              data: []
              postLabel: ' не заполнено поле "Текст при неправильном ответе"'
              preLabel: 'В вопросах № '
            resultText:
              data: []
              postLabel: ' не заполнено поле "Cообщение о результате"'
              preLabel: 'В результатах № '
            weight:
              data: []
              postLabel: ' не указаны веса'
              preLabel: 'В результатах № '

          box.json.questions.forEach (question, i) ->
            if question.choices.length < 2 then wrongQuestions.choices.data.push(i + 1)
            if !question.title then wrongQuestions.text.data.push(i + 1)
            if !question.righttext then wrongQuestions.rightText.data.push(i + 1)
            if !question.wrongtext then wrongQuestions.wrongText.data.push(i + 1)

          box.json.results.forEach (result, i) ->
            if !result.text then wrongQuestions.resultText.data.push(i + 1)
            if !result.max_weight || !result.min_weight && result.min_weight != 0 then wrongQuestions.weight.data.push(i + 1)

          _.forEach wrongQuestions, (wrongQuestion) ->
            if wrongQuestion.data.length != 0
              text = wrongQuestion.preLabel
              text += wrongQuestion.data.join(', ')
              text += wrongQuestion.postLabel
              emptyFields.push label: text

          emptyFields

        requiredFieldsParts = []
        docRequiredFields = []
        # Сначала проверяем в title, header и illustrator, затем проходимся по всем боксам
        _.forEach $scope.parts, (part) ->
          if part.attributes?
            # Проверяем атрибуты
            part.attributes.filter((attr) -> attr.required).forEach (attr) ->
              if !part.data[attr.name] || part.data[attr.name] == ''  then docRequiredFields.push label: attr.label

        # Проверяем урл
        if !document.url_id? then docRequiredFields.push label: 'Ссылка'
        # Проверяем источник
        if document.settings.requirements?.source?.required && (!document.source_url || !document.source_title) then docRequiredFields.push label: 'Источник'
        # Проверяем ссылки
        if document.settings.requirements?.links?.required && LinksService.totalEmpty() > 0
          LinksService.getEmpty().forEach (name) ->
            if name in document.settings.requirements?.links?.conditions?.section
              docRequiredFields.push label: document.settings.links[name].label

        if docRequiredFields.length != 0 || PartsService.totalUnsaved > 0 then requiredFieldsParts.push fields: docRequiredFields, label: 'Документ'


        boxTypesCounter = {}
        if $scope.root_box.children?
          $scope.root_box.children
            .filter (box) ->
              # группируем боксы по типу и присваиваем каждому боксу порядковый номер в типе
              # делаем это в фильтре, чтобы избежать лишних итераций
              if !boxTypesCounter[box.type]?
                boxTypesCounter[box.type] = "#{box.id}": 1, length: 1
              else
                boxTypesCounter[box.type].length += 1
                boxTypesCounter[box.type][box.id] = boxTypesCounter[box.type].length

              # отбрасываем все неактивные боксы
              box.enabled

            .forEach (box, i) ->
              if box.type == 'test'
                requiredFields = checkTest box
              else
                requiredFields = document.settings.boxes[box.type].attributes.filter (attr) ->
                  attr.required && (!box[attr.name] || box[attr.name] == '')

              if requiredFields.length != 0 || !BoxesService.checkUnsaved box.id
                boxNo = if boxTypesCounter[box.type].length > 1 then "#{boxTypesCounter[box.type][box.id]} " else ''
                requiredFieldsParts.push
                  label: "#{boxNo}Врез #{document.settings.boxes[box.type].label}"
                  id: box.id
                  fields: requiredFields

        # Если пустых полей нет, то публикуем документ и отменяем подсвечивание
        if requiredFieldsParts.length == 0
          $scope.$broadcast 'document.checkRequiredFields', false
          return $scope.publish document

        $scope.$broadcast 'document.checkRequiredFields', true

        # Показываем модальное окно с боксами с незаполненными полями
        parentScope = $scope
        modalInstance = $uibModal.open
          templateUrl: dialogWithRequiredFieldsTemplate
          controller: ($scope, $uibModalInstance) =>
            parentScope.$watch 'isDisablePublishButton', () ->
              $scope.status = parentScope.isDisablePublishButton
            $scope.title = 'Есть незаполненные или несохраненные поля!'
            $scope.parts = requiredFieldsParts
            $scope.close = -> $uibModalInstance.dismiss('close')
            $scope.forcePublish = -> $uibModalInstance.close('publish')
        modalInstance.result.then (res) ->
          if res == 'publish'
            $scope.$broadcast 'document.checkRequiredFields', false
            $scope.publish document

      # Публикация документа
      $scope.publish = (document) ->
        parentScope = $scope
        podcast = _.find $scope.root_box.children, {type: 'newsrelease_podcast'}
        modal_instance = $uibModal.open
          templateUrl: dialogTemplate
          controller: ($scope, $uibModalInstance) ->
            parentScope.$watch 'isDisablePublishButton', () ->
              $scope.status = parentScope.isDisablePublishButton
            $scope.title = 'Публикация документа'

            $scope.text = 'После публикации ссылки документа будут активны и доступны извне. Продолжить?<br>'
            if BoxesService.totalUnsavedBoxes() > 0
              $scope.text += '<span class="label label-danger">Есть несохраненные боксы!</span> '
            if PartsService.totalUnsaved() > 0
              $scope.text += '<span class="label label-danger">Есть несохраненные поля документа!</span>'
            if podcast && !podcast.podcast_id
              $scope.text += '<span class="label label-danger">У вас есть пустые подкасты!</span>'
            $scope.ok = -> $uibModalInstance.close('ok')
            $scope.close = -> $uibModalInstance.dismiss('close')
        modal_instance.result.then (state) ->
          if 'ok' == state
            # Опубликовать можно документ, у которого есть ссылка
            if document.url_id?
              Snipe.one('documents', document.id).one('publish').patch().then ->
                reload().then -> $scope.$broadcast 'document.publish', id: $scope.document.id
                Notify.add type: 'success', msg: 'Документ успешно опубликован.'
                BoxesService.clearDeleted()
            else
              Notify.add type: 'error', msg: 'У документа отсутствует ссылка!'

      # Обновление всего документа и перезагрузка его контента
      $scope.update = (document) ->
        document.patch().then ->
          reload().then -> $scope.$broadcast 'document.update', id: document.id
          Notify.add type: 'success', msg: 'Документ сохранен.'

      $scope.editor_full_name_with_icon = (editor_id) ->
        full_name = EditorsService.full_name_by_id(editor_id)
        icon = switch full_name
          when 'Konstantin Savelyev' then 'piggy-bank'
          when 'Тимур Жидков' then 'fire'
          when 'Aleksey Kurepin' then 'tree-deciduous'
          when 'Ксения Гохгут' then 'volume-off'
          when 'Vsevolod Romashov' then 'sunglasses'
          when 'Антон Федоров' then 'wrench'
          when 'Екатерина Дербилова' then 'queen'
          when 'Татьяна Лысова' then 'king'
          when 'Егор Фирсов' then 'tent'

        if icon
          "<span class='glyphicon glyphicon-#{icon}'></span>&nbsp;#{full_name}"
        else
          full_name
