partTemplate = require('./part.html')

angular.module('documents')
  .directive 'rmPart', ->
    restrict: 'E'
    templateUrl: partTemplate
    scope:
      part: '@'
      id: '='
    controller: ($scope, Snipe, Chatter, $rootScope, $timeout, $element, DocumentSettings, Locker, Notify, current_document, PartsService) ->
      $scope.part_data = {}
      $scope.forms = {}

      $scope.$watch 'forms.partForm.$dirty', (dirty) ->
        if dirty
          PartsService.addUnsaved $scope.part

      $scope.$on 'document.publish', ->
        PartsService.removeUnpublished $scope.part
        $scope.isUnpublished = false

      reload = ->
        Snipe
          .one('documents', $scope.id)
          .one("#{$scope.part}_data")
          .get()
          .then (res) ->
            if res.data.finished?
              $rootScope.$emit 'document.finishedBroadcastUpdate', res.data.finished
            extendAttributes res.settings[$scope.part].attributes
            angular.extend($scope.part_data, res)
            checkPublishedPart()

      reload()

      # Проверка, опубликована ли part
      checkPublishedPart = ->
        doc = current_document.document
        _.isEqualWith doc, doc.packed_document, (obj, packed) ->
          if _.isEmpty packed
            $scope.isUnpublished = true
            PartsService.addUnpublished $scope.part
            return
          for key, val of packed
            if key == 'published_at' then continue
            if obj[key]? && packed[key] != obj[key]
              if _.has $scope.part_data.data, key
                PartsService.addUnpublished $scope.part
                $scope.isUnpublished = true
                return
          PartsService.removeUnpublished $scope.part
          $scope.isUnpublished = false

      $scope.$watch '[part_data.data, part_data.settings[part].attributes]', () ->
        if $scope.part_data.data
          $scope.$emit 'document.partDataLoaded',
            data: angular.copy $scope.part_data.data
            attributes: angular.copy $scope.part_data.settings[$scope.part].attributes
            part: $scope.part
      , true

      $scope.$on 'update:categories', ($event, data) ->
        Snipe
          .one('documents', $scope.id)
          .one("#{$scope.part}_data")
          .get()
          .then (response) ->
            extendAttributes response.settings[$scope.part].attributes
            angular.extend($scope.part_data.settings, response.settings)
          .then () ->
            if data?.paywall? && _.find($scope.part_data.settings[$scope.part].attributes, name: 'pay_required')
              $scope.Locker.lockPart()
              $scope.part_data.data.pay_required = data.paywall.enabled
              if data.paywall.enabled == true
                $scope.part_data.data.pay_required_expire = new Date(moment().endOf('day', moment()).add(3, 'days').add(3, 'hours'))
              $scope.update($scope.part_data, $scope.forms.partForm)
                .then (response) ->
                  $rootScope.$broadcast 'paywall.settings', data.paywall


      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: $scope.id
        part: $scope.part
        reload: -> reload()

      extendAttributes = (attributes) ->
        if current_document.settings.requirements?[$scope.part]?
          attributes.map (attr) ->
            src = current_document.settings.requirements[$scope.part][attr.name]
            if src
              angular.extend(attr, src)

      $scope.update = (part, form) ->
        $scope.part_data
          .patch(data: part.data, meta: part.meta || {} )
          .then (data) ->
            if part.data.finished?
              $rootScope.$broadcast 'document.finishedBroadcastUpdate', part.data.finished
            $scope.part_data.meta = data.meta
            $rootScope.$broadcast 'document.title.changed', $scope.part_data.data.title
            if $scope.part_data.data.pay_required?
              $rootScope.$broadcast 'document.pay_required.changed', $scope.part_data.data.pay_required
            $scope.Locker.unlock(true)
            form.$setPristine()
            PartsService.removeUnsaved $scope.part
            PartsService.addUnpublished $scope.part
            $scope.isUnpublished = true
          , ->
            Notify.add type: 'error', msg: 'Документ был обновлен другим редактором, перезагрузите страницу.'
