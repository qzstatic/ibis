urlTemplate = require('./url.html')
urlsTemplate = require('./urls.html')

angular.module('documents')
  .directive 'rmUrl', ->
    restrict: 'A'
    scope:
      url: '='
    replce: true
    templateUrl: urlTemplate
    controller: ($scope, Snipe, Notify, $element, $http, Settings, $filter, $timeout, doc_settings, $rootScope, current_document, CategoriesService) ->
      $scope.mask          = doc_settings.settings.urls.mask
      $scope.view_settings = doc_settings.settings.urls.attributes
      $scope.isEmptySlug = true

      checkSlugLength = ->
        $scope.isEmptySlug = _.without($scope.url.slug, 'null').length == 0

      $scope.$on 'document.settings.update', (event, settings) ->
        angular.copy(settings.urls.attributes, $scope.view_settings)
        reload_sources()

      $scope.sources =
        domain: $scope.$parent.domains

      domain_id_by_slug = (slug) ->
        found = _.find($scope.sources.domain, slug: slug)
        found.id if found?.id?

      reload_sources = ->
        _.each $scope.view_settings, (control) ->
          if control.source? #&& 'domain' != control.name
            $http.get("#{Settings.snipe_api_url.replace(/\/v\d/, '')}#{control.source}?parent_id=#{domain_id_by_slug($scope.url.domain)}")
              .then (resp) ->
                $scope.sources[control.name] = $filter('filter') resp.data, (item) ->
                  item.id in $scope.$parent.document.category_ids

                if control.name == 'domain' then return
                # Если это новая ссылка, нужно установить значение по умолчанию
                unless $scope.url.id
                  if _.first($scope.sources[control.name])?
                    if control.name == 'rubric'
                      $scope.selectedCategories = CategoriesService.selectedCategories
                      $scope.url[control.name] = _.first($scope.selectedCategories).slug
                    else
                      $scope.url[control.name] = _.first($scope.sources[control.name]).slug

      $scope.$watch 'url.domain', (value) -> reload_sources() if value? && !$scope.url.fixed

      updateScope = (title) ->
        unless $scope.url.fixed
          translit = []
          slug = []
          if !title then return
          _.each title.split(' '), (word) -> translit.push($filter('translit')(word))
          if $scope.url.keywords
            $scope.url.slug = angular.copy $scope.url.keywords.split(',')
          else
            # Проверка что использовать в качестве слага
            typeIsKeywords = _.find doc_settings.settings.urls.attributes, {name: 'slug', type: 'keywords'}
            if typeIsKeywords
              _.each translit, -> slug.push 'null'
            else
              slug = angular.copy(translit).join('-')
            $scope.url.slug = slug
          checkSlugLength()
          $scope.url.translit = translit

      $scope.$watchCollection 'url.slug', (slug) ->
        if !_.isArray slug then return false
        checkSlugLength()
        $scope.$parent.$parent.isWordsSelected = !$scope.isEmptySlug

      $rootScope.$on 'document.title.changed', (e, title) ->
        updateScope( title )

      $scope.$parent.$watch 'document.title', (title)->
        updateScope( title )

      $rootScope.$on 'update:categories', () ->
        current_document.reload().then ->
          $scope.mask          = doc_settings.settings.urls.mask
          $scope.view_settings = doc_settings.settings.urls.attributes
          reload_sources()

      $scope.tmp = (url) ->
        url.replace(/http:\/\/vedomosti.ru/, Settings.shark_url)

      $scope.$watch 'mask', ->
        splitted_mask = $scope.mask.split(/(:[\w\d_]+)/)
        splitted_mask = _.map splitted_mask, (part) ->
          name = part.split(':').join('')

          if part.match(/^:/)
            view = _.first($filter('filter')($scope.view_settings, name: name))
            part =
              name: name
              view: view
          else
            part =
              name: name
              value: part
              view:
                type: 'text'

        $scope.splitted_mask = splitted_mask

      $scope.compiled_url = (mask, url) ->
        complete_url = if url.fixed
          url.rendered
        else
          if mask? && url?
            url_parts = []
            for part in mask
              if 'datepicker' == part.view.type
                value = moment(url[part.name]).format('YYYY/MM/DD')
              else if 'pseudo_id' == part.view.type
                value = $scope.$parent.pseudo_document_id($scope.$parent.document.id)
              else if 'keywords' == part.view.type
                value = _.without($scope.url.slug, 'null').join('-')
              else
                value = url[part.name] || part.value

              url_parts.push(value)
            url_parts.join('')

        complete_url = 'http://' + complete_url

      $scope.can_delete  = (url) -> $scope.$parent.urls.length > 1 && !url.fixed
      $scope.can_promote = (url) -> url.fixed
      $scope.toggle_mode = (url) -> url.edit_mode = !url.edit_mode
      $scope.remove = (url) ->
        # Можно удалить только нефиксированную ссылку
        if !url.fixed && $scope.url.id?
          Snipe.one('urls', url.id).remove().then -> Notify.add(type: 'success', msg: 'Ссылки удалена.')

          index = _.indexOf($scope.$parent.urls, url)
          $scope.$parent.urls.splice(index, 1)

      $scope.promote = (current_url) ->
        if current_url.fixed
          for url in $scope.$parent.urls
            url.main = url.id == current_url.id

          Snipe.one('documents', $scope.$parent.document.id).patch(url_id: current_url.id).then ->
            $scope.$parent.document.url_id = current_url.id
            Notify.add type: 'success', msg: 'Главная ссылка заменена.'

      $scope.select_for_copy = -> $element.find('input')[1].select()

      $scope.toggleWord = (e, index) ->
        target = $(e.currentTarget)
        target.toggleClass('label label-success')
        if target.hasClass('label-success')
          $scope.url.slug[index] = $scope.url.translit[index]
        else
          $scope.url.slug.splice(index, 1, 'null')
        checkSlugLength()


  .directive 'rmUrlsDocumentModule', ->
    restrict: 'E'
    replace: true
    scope:
      document: '='
    templateUrl: urlsTemplate
    controller: ($scope, Snipe, Notify, $element, Chatter, Locker, $timeout, $rootScope) ->
      $scope.mask          = $scope.$parent.document.settings.urls.mask
      $scope.view_settings = $scope.$parent.document.settings.urls.attributes
      $scope.isWordsSelected = true

      $scope.pseudo_document_id = (id) -> id - 140737488355328 + 123456

      # Перезагрузка ссылок
      reload = ->
        # Загружаем список доменов
        Snipe.all('categories').all('domains').getList().then (domains) ->
          $scope.domains = domains

          Snipe
            .one('documents', $scope.document.id)
            .all('urls')
            .getList()
            .then (urls) ->
              for url in urls
                url.main      = url.id == $scope.document.url_id
                url.edit_mode = false
                url.date = new Date(url.date)

              $scope.urls = urls

              # Если у документа нет ссылок — добавим ему одну пустую
              $scope.add() if $scope.mask && 0 == $scope.urls.length

      # Во время время публикации документа нужно перезагрузить все ссылки
      $scope.$on 'document.publish', (e, args) -> reload()

      # Загружаем информацию о созданных ссылках и
      # маркируем главную исходя из данных документа
      $scope.$parent._loading.addPromise reload()

      $scope.update_raw_url = ->
        $scope.raw_url = $scope.raw_url.replace(/(http|https):\/\/(www\.)?/, '')
        Snipe.one('documents', $scope.document.id).all('urls/raw').post(attributes: { rendered: $scope.raw_url }).then (new_url) ->
          $scope.urls.push(new_url)
          $scope.raw_url = ''

      # Добавляет новую пустую ссылку только в случае если документ уже опубликован или у него нет ни одной ссылки
      $scope.add = ->
        new_url =
          edit_mode: true
          main: false

        # Установка дефолтных значений для новой ссылки
        for part in $scope.document.settings.urls.attributes
          if 'datepicker' == part.type
            new_url[part.name] = new Date()

          if 'pseudo_id' == part.type
            new_url[part.name] = $scope.pseudo_document_id($scope.document.id)

          if 'domain' == part.name
            new_url[part.name] = _.first($scope.domains).slug

        $scope.urls.push(new_url) if $scope.document.published || 0 == $scope.urls.length

      # Проверка полей на корректное заполнение
      validate = (url) -> true

      # Сохраняет все ссылки
      $scope.save = ->
        $timeout ->
          $scope.Locker.unlock(true)
        , 1000
        for url in $scope.urls
          url.edit_mode = false

          # Если ссылка зафиксирована — мы нечего с ней не делаем, просто пропускаем
          unless url.fixed
            if validate(url)
              cloned_url = angular.copy(url)

              if _.isArray url.slug
                cloned_url.keywords = url.slug.join(',')
                slug_for_save = _.without url.slug, 'null'

                if slug_for_save.length
                  cloned_url.slug = slug_for_save.join('-')
                else
                  cloned_url.slug = ''

              for part in $scope.document.settings.urls.attributes
                cloned_url[part.name] = moment(cloned_url[part.name]).format('YYYY-MM-DD') if 'datepicker' == part.type

              # Это старая ссылка — обновляем
              if url.id?
                Snipe
                  .one('urls', cloned_url.id)
                  .patch(cloned_url)
                  .then () ->
                    Notify.add(type: 'success', msg: 'Ссылки успешно обновлены.')
              else
                Snipe
                  .one('documents', $scope.document.id)
                  .all('urls')
                  .post(attributes: cloned_url)
                  .then (created_url) ->
                    Notify.add(type: 'success', msg: 'Ссылка успешно сохранена.')
                    created_url.date = new Date(created_url.date)
                    angular.extend(url, created_url)
                    if cloned_url.keywords
                      url.slug = cloned_url.keywords.split ','
                    # Если это первая ссылка в коллекции
                    if 1 == $scope.urls.length
                      $scope.document.url_id = url.id
                      url.main               = true
                      Snipe.one('documents', $scope.document.id).patch(url_id: url.id)
                    Chatter.unblock_part($scope.id, $scope.part)
                  , (error) ->
                    Notify.add(type: 'error', msg: 'Ссылку не удалось сохранить. Проверьте заполнение полей.')

          # Если документ не опубликован, обновляем ссылку у документа, если её нет
          # if !$scope.document.published && 1 == $scope.urls.length && !$scope.document.url_id?
          #   Snipe.one('documents', $scope.document.id).patch(url_id: _.first($scope.urls).id)

      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: $scope.document.id
        part: 'urls'
        reload: -> reload()
