sourceTemplate = require('./source.html')

angular.module('documents')
  .directive 'rmSourceSelector', ->
    restrict: 'E'
    replace: true
    templateUrl: sourceTemplate
    scope:
      document: '='
    controller: ($scope, Snipe) ->
      $scope.sources = Snipe.all('options').all('source').getList().$object
      $scope.hidden = '' != $scope.document.source_title
      
      $scope.select = (source) ->
        $scope.document.source_title = source.title
        $scope.document.source_url   = source.url
        $scope.hidden = true
        
      $scope.reset = ->
        $scope.document.source_title = null
        $scope.document.source_url   = null
        $scope.document.source_id    = null
        $scope.hidden = false
        
        $scope.document.patch(source_title: null, source_url: null, source_id: null)
        
      $scope.save = ->
        $scope.document.patch(source_title: $scope.document.source_title, source_url: $scope.document.source_url)
