angular.module('documents')
  .factory 'Locker', (Chatter, Editor, EditorsService, Snipe, Notify, Atasus, $interval) ->

    class Locker

      constructor: (options)->
        @options = options
        @options.scope._locked = false
        @options.scope.locked = false
        @options.scope.locked_editor_id = null
        @options.scope.locked_connection_id = null

        @options.element.on 'mousedown', (e) => @lockPart(e)
        @options.element.find('.body').on 'focus', (e) => @lockPart(e)

        @options.scope.$on 'part:block', (e, part) =>
          if part? && part.part == @options.part
            @lock(part.client.editor_id, part.client.connection_id)
            $(@options.element).prevAll('.glyphicon').hide()

        @options.scope.$on 'part:unblock', (e, part) =>
          if part? && part.part == @options.part
            @unlock()
            $(@options.element).prevAll('.glyphicon').show()


      editor: -> EditorsService.full_name_by_id(@options.scope.locked_editor_id)


      lockPart: (e) ->
        # if @options.scope.box then Atasus.add e.type, @options.scope.box
        if !@options.scope.locked && !@options.scope._locked
          Chatter.block_part(@options.id, @options.part)
          @options.scope._locked = true

      lock: (editor_id, connection_id) ->
        if Chatter.connected()
          @options.scope.locked = true
          @options.scope.locked_editor_id = editor_id
          @options.scope.locked_connection_id = connection_id
          $(@options.element).addClass('locked-item')


      lock_by_me: -> true == @options.scope._locked


      unlock: (remote = false) ->
        if Chatter.connected()
          @options.scope._locked = false
          if @options.scope.locked
            if !remote && @options.scope.locked_connection_id != Chatter.get_uuid()
              @options.reload()
            @options.scope.locked = false
            @options.scope.locked_editor_id = null
            @options.scope.locked_connection_id = null
            $(@options.element).removeClass('locked-item')
          if remote
            Chatter.unblock_part(@options.id, @options.part)

    Locker
