angular.module('documents').factory 'current_document', ($rootScope, Snipe, doc_settings, DocumentSettings) ->
  class CurrentDocument

    constructor: ->
      @document = {}
      @settings = {}

    load: (@id) ->
      Snipe.one('documents', @id).get().then (loaded_document) =>

        loaded_document.published_at = moment(loaded_document.published_at).format('YYYY-MM-DD HH:mm:ss')

        # Добавляет title поле к любому документу
        loaded_document.settings.head = { attributes: [] } unless loaded_document.settings.head?
        loaded_document.settings.head.attributes.unshift(DocumentSettings.title)

        loaded_document.source_id = '' if 0 == loaded_document.source_id

        @document = loaded_document
        @settings = loaded_document.settings

        doc_settings.set(@document)

        $rootScope.$broadcast 'document.settings.update', @settings

        @document

    reload: -> @load(@id)

  new CurrentDocument
