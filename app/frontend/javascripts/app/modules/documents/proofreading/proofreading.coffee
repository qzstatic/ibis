proofreadTemplate = require('./proofreading.html')

angular.module('documents')
  .directive 'rmProofreading', ->
    restrict: 'E'
    replace: true
    templateUrl: proofreadTemplate
    scope:
      document: '='
    controller: ($scope, Snipe, Notify) ->
      $scope.proofread = ->
        $scope.document.all('proofread').patch().then -> 
          $scope.$parent.reload()
          Notify.add type: 'success', msg: 'Теперь документ вычитан.'
