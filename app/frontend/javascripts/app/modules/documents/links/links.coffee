linksTemplate = require('./links.html')
panelTemplate = require('./panel.html')

angular.module('documents')
  .directive 'rmLinksModule', ->
    restrict: 'E'
    replace: true
    scope:
      document: '='
    templateUrl: linksTemplate
    controller: ($scope, doc_settings, Snipe, $rootScope, CompaniesService, LinksService) ->
      $scope.settings = doc_settings.settings
      $scope.links = []
      for key, section of $scope.settings.links
        $scope.links[key] = []
      Snipe
        .one('documents', $scope.document.id)
        .all('links')
        .getList()
        .then (all_links) ->
          links = {}
          for link in all_links
            links[link.section] = [] unless links[link.section]?
            links[link.section].push(link)
            if link.section == 'authors'
              $rootScope.authors = [] unless $rootScope.authors?
              $rootScope.authors.push link
            if link.section == 'companies'
              CompaniesService.companyIds = _.uniq(_.map(  links[link.section], 'bound_document.id'))
          $scope.links = links
          _.forEach $scope.settings.links, (setting, name) ->
            if !links[name] || links[name].length == 0
              LinksService.addEmpty name

  .directive 'rmLinksList', ->
    restrict: 'E'
    replace: true
    scope:
      name:     '='
      document: '='
      links:    '='
    templateUrl: panelTemplate
    controller: ($scope, doc_settings, Snipe, $filter, Search, $rootScope, Atasus, CompaniesService, current_document, LinksService) ->
      $scope.settings = doc_settings.settings
      $scope.label    = $scope.settings.links[$scope.name].label

      $scope.links = if $scope.links? then $scope.links else []
      $scope.isUnpublished = false

      isCompanies = $scope.name == 'companies'

      $scope.$watch 'links', (link) ->
        if _.isArray link then checkPublishedLinks()
      , true

      # Проверка опубликованы ли ссылки
      checkPublishedLinks = () ->
        publishedLinks = current_document.document.packed_links[$scope.name]
        if (!publishedLinks && !$scope.links.length)
          $scope.isUnpublished = false
          LinksService.removeUnpublished $scope.name
          return
        if (!publishedLinks && $scope.links.length) || (publishedLinks.length != $scope.links.length)
          $scope.isUnpublished = true
          LinksService.addUnpublished $scope.name
          return
        # Сортируем ссылки по ID для сравнения
        publishedLinks = _.sortBy publishedLinks, (link) -> link.bound_document.id
        links = _.sortBy $scope.links, (link) -> link.bound_document.id
        _.each links, (link, index) ->
          if !publishedLinks[index]
            $scope.isUnpublished = true
            LinksService.addUnpublished $scope.name
            return
          _.isEqualWith link, publishedLinks[index], (l, p) ->
            if p.bound_document.id != l.bound_document.id || (p.position != l.position && !isCompanies)
              $scope.isUnpublished = true
              LinksService.addUnpublished $scope.name
              return
            $scope.isUnpublished = false
            LinksService.removeUnpublished $scope.name

      $scope.add = ->
        $scope.links.unshift(edit: true, bound_document: { title: $scope.value })
        $scope.value = ''

      $scope.create = (link) ->
        link.bound_document.category_slugs = $scope.settings.links[$scope.name]['not_found'] || []

        Snipe.all('documents/fast')
          .post(attributes: link.bound_document)
          .then (created_document) ->
            Snipe.one('documents', $scope.document.id)
              .all('links')
              .post(attributes: { section: $scope.name, bound_document_id: created_document.id})
              .then (created_link) ->
                angular.extend(link, created_link)
                link.edit = false
                $scope.reorder()
                if $scope.links.length > 0 then LinksService.addEmpty $scope.name

      $scope.find_documents = (pattern) ->
        categories = $scope.settings.links[$scope.name].filter
        Search.documents(pattern, categories)

      $scope.select_document = (document) ->
        $scope.value = ''
        $scope.form.$setPristine()
        Snipe
          .one('documents', $scope.document.id)
          .all('links')
          .post(attributes: { section: $scope.name, bound_document_id: document.id})
          .then (link) ->
            if !$scope.links?
              $scope.links = []
            $scope.links.push(link)
            $scope.reorder()
            if $scope.links.length > 0 then LinksService.removeEmpty $scope.name

      $scope.reorder = ->
        for link, index in $scope.links
          link.position = (index + 1) * 100
          Snipe.one('links', link.id).patch(position: link.position)

      $scope.reset = (link) ->
        $scope.links = $scope.links[1..-1]

      $scope.remove = (link) ->
        Snipe.one('links', link.id).remove().then ->
          $scope.links = $filter('filter')($scope.links, (model)->
            model.id != link.id
          )
          checkPublishedLinks()
          if link.section == 'companies'
            CompaniesService.companyIds = _.uniq(_.map($scope.links, 'bound_document.id'))
          if $scope.links.length == 0 then LinksService.addEmpty $scope.name

      $scope.sortable_handlers =
        orderChanged: $scope.reorder

      # Срабаывает при выделении компании в параграфе
      CompaniesService.companyUpdated = (ids) ->
        if isCompanies

          $scope.links = $scope.links || []

          if $scope.links.length
            links = angular.copy $scope.links
            linksIds = _.map(links, 'bound_document.id')
            uniqIds = _.difference(ids, linksIds)
          else
            uniqIds = ids

          for id in uniqIds
            Snipe
              .one('documents', $scope.document.id)
              .all('links')
              .post(attributes: { section: $scope.name, bound_document_id: id })
              .then (link) ->
                $scope.links.unshift(link)
                CompaniesService.companyIds = _.uniq(_.map($scope.links, 'bound_document.id'))
                # Atasus.add 'Компания добавлена', $scope.links
