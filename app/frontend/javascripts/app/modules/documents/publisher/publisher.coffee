publishTemplate = require('./publisher.html')

angular.module('documents')
  .directive 'rmDocumentPublisher', ->
    restrict: 'E'
    replace: true
    templateUrl: publishTemplate
    scope:
      document: '='
    controller: ($scope, Snipe, Notify) ->
      $scope.change_datetime = (document) ->
        Snipe.one('documents', document.id).one('publish').patch(published_at: document.published_at).then ->
          Notify.add type: 'msg', msg: 'Документ переопубликован с новой датой.'
          $scope.form.$setPristine()
