listTemplate = require('./list.html')
editTemplate = require('./edit.html')
newTemplate = require('./new.html')
contactsTemplate = require('./_contacts.html')
formTemplate = require('./form.html')
historyTemplate = require('./history.html')

angular.module('contacts', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.contacts',
        abstract: true,
        url: '/contacts'
        data: 
          permissions: 
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'
            
      .state 'logged.contacts.list',
        url: '/list/:page'
        auth: true
        resolve:
          contacts: ($stateParams, Aquarium) -> Aquarium.all('contacts').getList(page: $stateParams.page || 1)
          users_state: (Aquarium) -> Aquarium.one('contacts/count').get()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'ContactsListController'

      .state 'logged.contacts.search',
        url: '/search/:page?q?status?date_from?date_to'
        auth: true
        resolve:
          contacts: ($stateParams, Search) -> Search.contacts($stateParams)
          users_state: (contacts) ->
            contacts: contacts.stat.total
            per_page: contacts.stat.per_page
            pages: Math.ceil(contacts.stat.total/contacts.stat.per_page)
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'ContactsListController'

      .state 'logged.contacts.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          contact: (Aquarium, $stateParams) -> Aquarium.one('contacts', $stateParams.id).get()
          history: (Aquarium, $stateParams) -> Aquarium.one("contacts", $stateParams.id).one('letters').get()
          mailings: (Aquarium) -> Aquarium.all('mailing/mail_types').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'ContactsEditController'

      .state 'logged.contacts.new',
        url: '/new'
        auth: true
        resolve:
          mailings: (Aquarium) -> Aquarium.all('mailing/mail_types').getList()
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'ContactsCreateController'

  .controller 'ContactsListController', ($scope, Aquarium, $stateParams, contacts, $state, Search, UserSettings, Chatter, users_state) ->
    $scope.contactsTemplate = contactsTemplate
    $scope.filter =
      status:    $stateParams.status
      date_from: $stateParams.date_from
      date_to:   $stateParams.date_to

    $scope.stat  = users_state
    $scope.per_page = 50
    $scope.current_page = $stateParams.page || 1
    $scope.contacts = contacts.found || contacts
    $scope.search_value = $stateParams.q || ''
    $scope.states = UserSettings.states

    $scope.blocked = (contact) -> _.includes(['blocked', 'banned', 'deleted'], contact.contact_status)
    $scope.status_icon = (contact) -> UserSettings.states_icons[contact.contact_status]

    $scope.to_page = ->
      if document.location.href.match(/search/)
        $scope.go($scope.search_value)
      else
        $state.go('logged.contacts.list', page: $scope.current_page)

    $scope.go = (pattern) ->
      full_query =
        q: pattern
        page:  $scope.current_page
        date_from: moment($scope.filter.date_from).format('YYYY-MM-DD') if $scope.filter.date_from
        date_to: moment($scope.filter.date_to).format('YYYY-MM-DD') if $scope.filter.date_to
        status: $scope.filter.status

      $state.go('logged.contacts.search', full_query)

    $scope.reset = -> $state.go('logged.contacts.list', page: 1)

  .controller 'ContactsEditController', ($scope, contact, Aquarium, UserSettings, Notify, OrdersSettings, $state, mailings, history, $stateParams) ->
    $scope.contact = contact
    $scope.statuses = UserSettings.states
    $scope.mailings = mailings
    $scope.formTemplate = formTemplate
    $scope.historyTemplate = historyTemplate
    $scope.history = history
    $scope.filter = {}
    
    $scope.search = ->
      Aquarium
        .one("contacts", $stateParams.id)
        .customGET('letters', {
          start_date: $scope.filter.start_date
          end_date: $scope.filter.end_date
          type: $scope.filter.type
        })
        .then (res) -> $scope.history = res

    $scope.checkError = (id) ->
      Aquarium
        .one("contacts", $stateParams.id)
        .one('letters', id)
        .one('details')
        .get()
        .then (res) -> alert res.data.mail
    
    $scope.create_or_update = (contact) ->
      Aquarium.one('contacts', contact.id).patch(contact).then ->
        Notify.add msg: 'Контакт изменен.', type: 'success'
        
    $scope.destroy = (contact) ->
      Aquarium.one('contacts', contact.id).remove().then ->
        Notify.add msg: 'Контакт удалён.', type: 'success'
        $state.go('logged.contacts.list', page: $scope.current_page)

  .controller 'ContactsCreateController', ($scope, Aquarium, UserSettings, $state, Notify, mailings) ->
    $scope.statuses = UserSettings.states
    $scope.contact =
      contact_status: $scope.statuses[0].label
      mailing_ids: []
    $scope.mailings = mailings
    $scope.formTemplate = formTemplate
    $scope.historyTemplate = historyTemplate
      
    $scope.create_or_update = (contact) ->
      Aquarium.all('contacts').post(attributes: contact).then (created_user) ->
        Notify.add msg: 'Контакт создан.', type: 'success'
        $state.go('logged.contacts.edit', id: created_user.id)
