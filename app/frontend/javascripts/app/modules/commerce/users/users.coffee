listTemplate = require('./list.html.slim')
editTemplate = require('./edit.html.slim')
mobileEditTemplate = require('./mobile_edit.html.slim')
newTemplate = require('./new.html.slim')
usersTemplate = require('./_users.html.slim')
mobileUsersTemplate = require('./_mobile_users.html.slim')
formTemplate = require('./form.html.slim')
mobileFormTemplate = require('./mobile_form.html.slim')
ordersTemplate = require('./_orders.html.slim')

angular.module('users', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.users',
        abstract: true,
        url: '/users'
        data:
          permissions:
            except: ['COMMERS']
            redirectTo: 'errors.403'
      .state 'logged.users.list',
        url: '/list/:page'
        auth: true
        resolve:
          users: ($stateParams, Aquarium) -> Aquarium.all('users').getList(page: $stateParams.page || 1)
          users_state: (Aquarium) -> Aquarium.one('users/count').get()
          type: -> 'users'
          UsersTemplateName: -> usersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'UsersListController'

      .state 'logged.users.search',
        url: '/search/:page?q?status?date_from?date_to'
        auth: true
        resolve:
          users: ($stateParams, Search) -> Search.users($stateParams)
          users_state: (users) ->
            users: users.stat.total
            per_page: users.stat.per_page
            pages: Math.ceil(users.stat.total/users.stat.per_page)
          type: -> 'users'
          UsersTemplateName: -> usersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'UsersListController'

      .state 'logged.users.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          type: -> 'users'
          formTemplateName: -> formTemplate
          user_state: (Aquarium, $stateParams) -> Aquarium.one('users', $stateParams.id).customGET('full')
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'UsersEditController'

      .state 'logged.users.new',
        url: '/new'
        auth: true
        resolve:
          formTemplateName: -> formTemplate
          type: -> 'users'
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'UsersCreateController'

      .state 'logged.users.mobile',
        abstract: true
        url: '/mobile'
        data:
          permissions:
            except: ['COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.users.mobile.list',
        url: '/list/:page'
        auth: true
        resolve:
          users: ($stateParams, Aquarium) -> Aquarium.all('mobile_users').getList(page: $stateParams.page || 1)
          users_state: (Aquarium) -> Aquarium.one('mobile_users/count').get().then (res) -> users: res.mobile_users, pages: res.pages
          type: -> 'users.mobile'
          UsersTemplateName: -> mobileUsersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'UsersListController'

      .state 'logged.users.mobile.search',
        url: '/search/:page?q?status?date_from?date_to'
        auth: true
        resolve:
          users: ($stateParams, Search) -> Search.mobile_users($stateParams)
          users_state: (users) ->
            users: users.stat.total
            per_page: users.stat.per_page
            pages: Math.ceil(users.stat.total/users.stat.per_page)
          type: -> 'users.mobile'
          UsersTemplateName: -> mobileUsersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'UsersListController'

      .state 'logged.users.mobile.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          user_state: (Aquarium, $stateParams) -> Aquarium.one('mobile_users', $stateParams.id).get().then (res) -> user: res
          formTemplateName: -> mobileFormTemplate
          type: -> 'users.mobile'
        views:
          'content@logged':
            templateUrl: mobileEditTemplate
            controller: 'UsersEditController'

      .state 'logged.users.mobile.new',
        url: '/new'
        auth: true
        resolve:
          formTemplateName: -> mobileFormTemplate
          type: -> 'users.mobile'
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'UsersCreateController'


  .controller 'UsersListController', ($scope, Aquarium, UsersTemplateName, $stateParams, type, users, users_state, $state, Search, UserSettings, Chatter) ->
    $scope.usersTemplate = UsersTemplateName
    $scope.type = type
    $scope.newUserLink = "logged.#{type}.new"
    $scope.editUserLink = "logged.#{type}.edit"
    $scope.filter =
      status:    $stateParams.status
      date_from: $stateParams.date_from
      date_to:   $stateParams.date_to

    $scope.stat  = users_state
    $scope.current_page = $stateParams.page || 1
    $scope.per_page = users_state.per_page || 50
    $scope.users = users.found || users
    $scope.search_value = $stateParams.q || ''
    $scope.states = UserSettings.states

    $scope.blocked = (user) -> _.includes(['blocked', 'banned', 'deleted'], user.status)
    $scope.status_icon = (user) -> UserSettings.states_icons[user.status]
    $scope.autopay_icon = (user) ->
      status = if null == user.autopayment then false else user.autopayment
      if status then 'star' else 'star-empty'

    $scope.to_page = ->
      if document.location.href.match(/search/)
        $scope.go($scope.search_value)
      else
        $state.go("logged.#{type}.list", page: $scope.current_page)

    $scope.go = (pattern) ->
      full_query =
        q: pattern
        page:  $scope.current_page
        date_from: moment($scope.filter.date_from).format('YYYY-MM-DD') if $scope.filter.date_from
        date_to: moment($scope.filter.date_to).format('YYYY-MM-DD') if $scope.filter.date_to
        status: $scope.filter.status

      $state.go("logged.#{type}.search", full_query)

    $scope.reset = ->
      $state.go("logged.#{type}.list", page: 1)


  .controller 'UsersEditController', ($scope, user_state, type, formTemplateName, Aquarium, UserSettings, Notify, OrdersSettings, $state) ->
    $scope.formTemplate = formTemplateName
    $scope.ordersTemplate = ordersTemplate
    $scope.user_state = user_state
    $scope.user = user_state.user
    $scope.statuses = UserSettings.states
    $scope.accounts = user_state.accounts
    $scope.acceess_rights = user_state.acceess_rights
    $scope.new_orders = []
    $scope.old_orders = []

    if $scope.acceess_rights?
      for acceess_right in $scope.acceess_rights
        if acceess_right.order?
          $scope.old_orders.push acceess_right.order

        if acceess_right.payment?
          $scope.new_orders.push acceess_right.payment

    $scope.create_or_update = (user) ->
      url = if type == 'users' then 'users' else 'mobile_users'
      Aquarium.one(url, user.id).patch(user).then ->
        Notify.add msg: 'Пользователь изменен.', type: 'success'
      , ->
        Notify.add msg: 'Не удалось сохранить изменения, попробуйте позже', type: 'error'

    $scope.autopay_icon = (order) ->
      status = if null == order.autopayment then false else order.autopayment
      if status then 'star' else 'star-empty'

    $scope.slx_icon = (order) ->
      if order.synced then 'star' else 'star-empty'

    $scope.localize = (key, value) ->
      if OrdersSettings[key]?[value]?
        OrdersSettings[key][value]
      else
        value

    $scope.destroy = (user) ->
      url = if type == 'users' then 'users' else 'mobile_users'
      Aquarium.one(type, user.id).remove().then ->
        Notify.add msg: 'Пользователь удалён.', type: 'success'
        $state.go("logged.#{type}.list", page: $scope.current_page)


  .controller 'UsersCreateController', ($scope, type, formTemplateName, Aquarium, UserSettings, $state, Notify) ->
    $scope.user = {}
    $scope.statuses = UserSettings.states
    $scope.listUsersLink = "logged.#{type}.list"
    $scope.formTemplate = formTemplateName

    $scope.create_or_update = (user) ->
      url = if type == 'users' then 'users' else 'mobile_users'
      Aquarium.all(url).post(attributes: user).then (created_user) ->
        Notify.add msg: 'Пользователь создан.', type: 'success'
        $state.go("logged.#{type}.edit", id: created_user.id)
