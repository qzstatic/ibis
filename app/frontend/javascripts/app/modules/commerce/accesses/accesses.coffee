showTemplate = require('./show.html')

angular.module('accesses', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.accesses',
        abstract: true,
        url: '/accesses'
        
      .state 'logged.accesses.list',
        url: '/list'
        auth: true
        views:
          'content@logged':
            templateUrl: showTemplate
            controller: 'AccessesListController'
            
  .controller 'AccessesListController', (Aquarium) ->
