showTemplate = require('./show.html')

angular.module('reports', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.reports',
        abstract: true,
        url: '/reports'
        data: 
          permissions: 
            except: ['COMMERS']
            redirectTo: 'errors.403'
      .state 'logged.reports.list',
        url: '/list'
        auth: true
        resolve:
          reports: (Aquarium) -> Aquarium.all('reports').getList()
        views:
          'content@logged':
            templateUrl: showTemplate
            controller: 'ReportsListController'

  .controller 'ReportsListController', ($scope, Aquarium, reports, Notify, $rootScope) ->
    $scope.reports = reports

    $scope.reports_list = {
      'paywall_chargers':     'Ударившиеся в пейволл',
      'paywall_subscriptions': 'Ударившиеся в пейволл на странице газеты',
      'new_users':            'Новые регистрации пользователей',
      'subscription_steps':   'Проход по трём шагам подписки',
      'authors_rating':       'Статистика по авторам',
      'mailing_issue':        'Статистика рассылки "Свежий номер"',
      'subscription_orders':  'Заходы и заказы',
      'payments_sales':       'Оплаченные заказы',
      'new_payments_sales':       'Отчет о всех заказах'
	
    }
    
    if $rootScope.auth.isEditor()
      $scope.reports_list = {
        'authors_rating':      'Статистика по авторам',
        'mailing_issue':       'Статистика рассылки "Свежий номер"',
      }

    $scope.generate = (filter) ->
      Aquarium.all('reports').post(attributes: filter).then (new_report) ->
        $scope.reports.unshift new_report
      , -> Notify.add type: 'error', msg: 'Во время генерации отчета произошла ошибка. Возможно, вы не заполнили дату.'
