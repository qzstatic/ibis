listTemplate = require('./list.html')
editTemplate = require('./edit.html')
newTemplate = require('./new.html')
ordersTemplate = require('./_orders.html')
formTemplate = require('./_form.html')
usersTemplate = require('../users/_users.html.slim')

angular.module('subscription')
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.subscription',
        abstract: true,
        url: '/subscription'
        data:
          permissions:
            except: ['EDITOR', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.subscription.list',
        url: '/list/:page'
        auth: true
        resolve:
          orders: ($stateParams, Aquarium) -> Aquarium.all('orders').getList(page: $stateParams.page || 1)
          orders_state:  (Aquarium) -> Aquarium.one('orders/count').get()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'SubscriptionListController'

      .state 'logged.subscription.search',
        url: '/search/:page?q?payment_status?payment_method?date_from?date_to?product'
        auth: true
        resolve:
          orders: ($stateParams, Search) -> Search.orders($stateParams)
          orders_state: (orders) ->
            orders: orders.stat.total
            per_page: orders.stat.per_page
            pages: Math.ceil(orders.stat.total/orders.stat.per_page)
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'SubscriptionListController'

      .state 'logged.subscription.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          order: (Aquarium, $stateParams) -> Aquarium.one('orders', $stateParams.id).get()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'SubscriptionEditController'


      .state 'logged.subscription.new',
        url: '/new'
        auth: true
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'SubscriptionCreateController'

  .controller 'SubscriptionListController', ($scope, orders, orders_state, $stateParams, $state, Search, OrdersSettings) ->
    $scope.ordersTemplate = ordersTemplate

    $scope.filter =
      q:              $stateParams.q || ''
      payment_status: $stateParams.payment_status
      payment_method: $stateParams.payment_method
      product:        $stateParams.product
      date_from:      $stateParams.date_from
      date_to:        $stateParams.date_to
      page:           $stateParams.page || 1

    $scope.orders = orders.found || orders
    $scope.stat = orders_state
    $scope.per_page = orders_state.per_page || 50
    $scope.states = OrdersSettings.states

    $scope.methods = OrdersSettings.methods
    $scope.products = OrdersSettings.products

    $scope.to_page = ->
      if document.location.href.match(/search/)
        $scope.go()
      else
        $state.go('logged.subscription.list', page: $scope.filter.page)

    $scope.go = ->
      full_query = angular.copy($scope.filter)
      full_query.date_from = moment($scope.filter.date_from).format('YYYY-MM-DD') if $scope.filter.date_from
      full_query.date_to = moment($scope.filter.date_to).format('YYYY-MM-DD') if $scope.filter.date_to

      $state.go('logged.subscription.search', full_query)

    $scope.autopay_icon = (order) ->
      status = if null == order.autopayment then false else order.autopayment
      if status then 'star' else 'star-empty'

    $scope.slx_icon = (order) ->
      if order.synced then 'star' else 'star-empty'

    $scope.localize = (key, value) ->
      if OrdersSettings[key]?[value]?
        OrdersSettings[key][value]
      else
        value

    $scope.reset = ->
      $state.go('logged.subscription.list', page: 1)

  .controller 'SubscriptionEditController', ($scope, order, UserSettings, Notify, OrdersSettings) ->
    $scope.formTemplate = formTemplate
    $scope.usersTemplate = usersTemplate
    $scope.settings = OrdersSettings
    $scope.statuses = OrdersSettings.states

    $scope.order = order
    $scope.users = order.users

    $scope.blocked = (user) -> _.includes(['blocked', 'banned', 'deleted'], user.status)
    $scope.status_icon = (user) -> UserSettings.states_icons[user.status]
    $scope.autopay_icon = (user) ->
      status = if null == user.autopayment then false else user.autopayment
      if status then 'star' else 'star-empty'

    $scope.create_or_update = (order) ->
      order.patch().then (new_order) ->
        $scope.order = new_order
        Notify.add msg: 'Заказ изменен.', type: 'success'

  .controller 'SubscriptionCreateController', ($scope, UserSettings, Notify, Search, Aquarium, $state, OrdersSettings) ->
    $scope.settings = OrdersSettings
    $scope.statuses = OrdersSettings.states
    $scope.formTemplate = formTemplate

    $scope.order =
      reader:
        reader_type: 'User'

    $scope.find_users = (pattern) -> Search.users(q: pattern).then (json) -> json['found'][0..10]

    $scope.select_user = (user) -> $scope.order.reader.reader_id = user.id

    $scope.create_or_update = (order) ->
      Aquarium.all('orders').post(attributes: order).then (created_order) ->
        Notify.add msg: 'Заказ создан.', type: 'success'
        $state.go('logged.subscription.edit', id: created_order.id)
