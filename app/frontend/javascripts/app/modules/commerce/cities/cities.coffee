listTemplate = require('./list.html')

angular.module('catalogs', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.cities',
        abstract: true,
        url: '/cities'
        data: 
          permissions: 
            except: ['EDITOR', 'COMMERS']
            redirectTo: 'errors.403'
            
      .state 'logged.cities.list',
        url: '/list'
        auth: true
        resolve:
          cities: (Aquarium) -> Aquarium.all('cities').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'CitiesListController'

  .controller 'CitiesListController', ($scope, cities, Aquarium, $stateParams) ->
    $scope.cities = cities

    $scope.reset = ->
      $scope.current_item   =
        position: 0

    $scope.reset()

    $scope.edit = (item) -> item.edit = true

    $scope.update = (item) ->
      Aquarium.one('cities', item.id).patch(item)
      item.edit = false

    $scope.create = (new_item) ->
      if $scope.cities.length
        new_item.position = _.last($scope.cities).position + 1000
      else
        new_item.position = 0
      Aquarium.all('cities').post(attributes: new_item).then (created_item) ->
        $scope.cities.push created_item
        $scope.reset()

    reorder = ->
      for item, index in $scope.cities
        item.position = ( index + 1 ) * 1000
        Aquarium.one('cities', item.id).patch(item)

    $scope.sortable_handlers =
      orderChanged: ->
        reorder()

    $scope.remove = (city) ->
      Aquarium.one('cities', city.id).remove().then -> $scope.cities = _.without($scope.cities, city)
