listTemplate = require('./list.html.slim')
newTemplate = require('./new.html.slim')
editTemplate = require('./edit.html.slim')
formTemplate = require('./form.html.slim')


angular.module('prices', [])

  .value 'Shared', ($scope, Aquarium, periods, paymethods, products, namespaces, $state, Notify) ->
    select_product: (product) ->
      d1 = $.Deferred()
      d2 = $.Deferred()

      if product?.id?
        all_products   = Aquarium.one('products', product.id).all('subproducts').getList()
        all_namespaces = Aquarium.one('products', product.id).all('namespaces').getList()

        $scope.kits_for_product = Aquarium.one('products', product.id).all('active_payment_kits').getList().$object

        all_products.then   -> d1.resolve()
        all_namespaces.then -> d2.resolve()

        $scope.subproducts = all_products.$object
        $scope.namespaces = all_namespaces.$object

      $.when(d1, d2)

    select_namespace: (namespace) ->
      if namespace?.id?
        $scope.kits_for_namespace = Aquarium.one('namespaces', namespace.id).all('payment_kits').getList().$object

    select_subproduct: (subproduct) ->
      if subproduct?.id?
        $scope.kits_for_subproduct = Aquarium.one('subproducts', subproduct.id).all('payment_kits').getList().$object

    name: (product) ->
      [product.product.title, product.subproduct.title, product.namespace.title, product.period.title, product.price + ' руб.'].join(', ')

    set_to_main_namespace: (namespace) ->
      Aquarium.one('namespaces', namespace.id).all('set_base').patch().then -> namespace.base = true;

    save: (kit) ->
      kit.product_id = kit.product.id
      kit.namespace_id = kit.namespace.id
      kit.subproduct_id = kit.subproduct.id
      kit.period_id = kit.period.id
      kit.renewal_id = if kit.renewal? then kit.renewal.id else null
      kit.upsell_id = if kit.upsell? then kit.upsell.id else null

      kit.paymethod_ids = []
      kit.paymethod_ids.push(method.id) for method in kit.paymethods

      kit.alternate_ids = []
      kit.alternate_ids.push(alternate.id) for alternate in kit.alternates

      if kit.id?
        Aquarium.one('payment_kits', kit.id).patch(kit).then (new_kit) ->
          kit = new_kit
          Notify.add type: 'success', msg: 'Вариант подписки успешно изменен.'
      else
        Aquarium.all('payment_kits').post(attributes: kit).then (new_kit) ->
          Notify.add type: 'success', msg: 'Вариант подписки успешно создан.'
          $state.go('logged.prices.edit', payment_kit_id: new_kit.id)

    default_new:
      period:
        duration_number: 1
        measure_unit: 'months'
        title: 'новый период'
        slug: 'month'
      paymethod:
        title: 'новый метод оплаты'
        slug:  'by_coins'
      product:
        title: 'новый продукт'
        slug:  'new_product'
      namespace:
        title: 'новый неймспейс'
        slug:  'new_namespace'
      subproduct:
        title: 'новый подпродукт'
        slug:  'new_subproduct'

    editors:
      period: false
      paymethod: false
      product: false
      namespace: false
      subproduct: false

    cancel: (step) ->
      $scope.new =  angular.copy $scope.default_new
      $scope.editors[step] = false

    edit_item: (step, item) ->
      $scope.new[step] = angular.copy item
      $scope.editors[step] = true

    new_item: (collection_name, single_name, item, push_to, set_to) ->
      if item.redirect_to?
        item.redirect_to_id = item.redirect_to.id

      if 'subproduct' == single_name || 'namespace' == single_name
        item.product_id = set_to.product.id

      if item.id?
        Aquarium.one(collection_name, item.id).patch(item).then (updated_item) ->
          found = _.find(push_to, id: updated_item.id)
          _.extend(found, updated_item)
          $scope.cancel(single_name)
      else
        Aquarium.all(collection_name).post(attributes: item).then (created_item) ->
          push_to.push(created_item)
          set_to[single_name] = _.last(push_to)
          $scope.cancel(single_name)
          $scope.select_namespace(created_item) if 'namespace' == single_name

  .config ($stateProvider) ->

    $stateProvider

      .state 'logged.prices',
        abstract: true,
        url: '/prices'
        data:
          permissions:
            except: ['EDITOR', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.prices.list',
        url: '/list'
        auth: true
        resolve:
          products: (Aquarium) -> Aquarium.all('products').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList().then (namespaces) ->
            namespaces.map (namespace) ->
              namespace.start_date = new Date namespace.start_date
              namespace.end_date = new Date namespace.end_date
              namespace
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PricesListController'

      .state 'logged.prices.new',
        url: '/:product_id/:namespace_id/new'
        auth: true
        resolve:
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          conditions: (Aquarium) -> Aquarium.all('users/payment_conditions').getList()
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'PricesNewController'

      .state 'logged.prices.edit',
        url: '/:payment_kit_id/edit'
        auth: true
        resolve:
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          payment_kit: (Aquarium, $stateParams) -> Aquarium.one('payment_kits', $stateParams.payment_kit_id).get().then (res) ->
            res.namespace.start_date = new Date res.namespace.start_date
            res.namespace.end_date = new Date res.namespace.end_date
            res
          related: (Aquarium, $stateParams) -> Aquarium.one('payment_kits', $stateParams.payment_kit_id).all('associations').getList()
          conditions: (Aquarium) -> Aquarium.all('users/payment_conditions').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'PricesEditController'

  .controller 'PricesListController', ($scope, namespaces, products, Aquarium, $filter) ->
    $scope.hideSortBody = {}
    $scope.payment_kits = {}
    $scope.subproducts = {}

    for namespace in namespaces
      $scope.payment_kits[namespace.id] = Aquarium.one('namespaces', namespace.id).all('payment_kits').getList().$object #then (data) ->

    for product in products
      $scope.subproducts[product.id] = Aquarium.one('products', product.id).all('subproducts').getList().$object
      $scope.hideSortBody[product.id] = true

    $scope.namespaces = namespaces
    $scope.products   = products

    $scope.namespaces_by_product = (product) -> _.filter(namespaces, _.matches({ product_id: product.id }))

    $scope.getTitles = () ->
      titles = []
      $scope.products.forEach (product) ->
        titles = _.concat titles, $scope.namespaces_by_product(product).map (np) -> np.title
      titles

    $scope.getPrices = () ->
      prices = []
      _.forEach $scope.payment_kits, (value, key) ->
        prices = _.uniq _.concat(prices, _.uniqBy(value, 'price').map((kit) -> parseFloat kit.price ))
      prices.sort()

  .controller 'PricesNewController', ($scope, periods, paymethods, products, Aquarium, $stateParams, $state, Shared, Notify, conditions) ->
    namespaces = []
    $scope.formTemplate = formTemplate

    $scope.periods    = periods
    $scope.paymethods = paymethods
    $scope.products   = products
    $scope.namespaces = namespaces
    $scope.conditions = conditions

    _.extend($scope, Shared($scope, Aquarium, periods, paymethods, products, namespaces, $state, Notify))
    $scope.new = angular.copy $scope.default_new

    $scope.kit =
      autopay_timeout_hours: 72
      syncable: false
      autoconfirm: true
      crossed_price: '0000'
      price: '0000'
      paymethods: []
      alternates: []
      texts:
        title: 'Название'
        autopayment: 'Текст для автоплатежа'

    # Если указан продукт и неймспейс, заполняем их в пеймент ките
    if $stateParams.product_id? && $stateParams.namespace_id?
      $scope.kit.product = _.find(products, id: +$stateParams.product_id)
      $scope.select_product($scope.kit.product).done ->
        $scope.kit.namespace = _.find($scope.namespaces, id: +$stateParams.namespace_id)
        $scope.select_namespace($scope.kit.namespace)

    # http://aquarium.dev/ibis/subproducts/:subproduct_id/payment_kits

  .controller 'PricesEditController', ($scope, payment_kit, periods, paymethods, products, Aquarium, Shared, $state, Notify, related, conditions) ->
    namespaces = []
    $scope.formTemplate = formTemplate

    $scope.periods    = periods
    $scope.paymethods = paymethods
    $scope.products   = products
    $scope.namespaces = namespaces
    $scope.related    = related
    $scope.conditions = conditions

    _.extend($scope, Shared($scope, Aquarium, periods, paymethods, products, namespaces, $state, Notify))
    $scope.new = angular.copy $scope.default_new

    $scope.select_product(payment_kit.product).then -> $scope.kit = payment_kit
    $scope.select_namespace(payment_kit.namespace)
    $scope.select_subproduct(payment_kit.subproduct)

  .filter 'pricesNamespaceFilter', () ->
    (input, filter, additions) ->
      return input if !filter

      checkDate = (date, filter, filterField) ->
        return true if !filter[filterField]?

        if _.includes filterField, 'start'
          return +date >= +filter[filterField]
        else if _.includes filterField, 'end'
          return +date <= +filter[filterField]
        else return true

      filtred = input
        .filter (data) ->
          return true if !filter.title
          _.includes(data.title.toLowerCase(), filter.title.toLowerCase()) || _.includes(data.slug.toLowerCase(), filter.title.toLowerCase())
        .filter (data) ->
          return true if !filter.start_date && !filter.end_date
          if filter.start_date? && filter.end_date?
            return checkDate(data.start_date, filter, 'start_date') && checkDate(data.end_date, filter, 'end_date')
          if filter.start_date?
            return true if checkDate data.start_date, filter, 'start_date'
          if filter.end_date?
            return true if checkDate data.end_date, filter, 'end_date'
          false
        .filter (data) ->
          return true if !filter.autopay || filter.autopay == ''
          return false if !additions.payment_kits[data.id]
          if filter.autopay == 'on'
            return additions.payment_kits[data.id].filter((kit) -> kit.autopay_timeout_hours || kit.renewal_id).length > 0
          else
            return additions.payment_kits[data.id].filter((kit) -> !kit.autopay_timeout_hours && !kit.renewal_id).length > 0
        .filter (data) ->
          return true if !filter.price
          return false if !additions.payment_kits[data.id]
          additions.payment_kits[data.id].filter((kit) -> _.includes(kit.price.toString(), filter.price)).length > 0
        .filter (data) ->
          return true if !filter.subproducts || filter.subproducts.length == 0
          return false if !additions.payment_kits[data.id]
          additions.payment_kits[data.id].filter((kit) -> _.some filter.subproducts, id: kit.subproduct_id).length > 0

      filtred
