angular.module 'commerce', [
  'users',
  'contacts',
  'subscription',
  'catalogs',
  'accesses',
  'reports'
]
