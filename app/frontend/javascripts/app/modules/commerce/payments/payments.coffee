listTemplate = require('./list.html.slim')
massiveTemplate = require('./massive.html.slim')
editTemplate = require('./edit.html.slim')
newTemplate = require('./new.html.slim')
exportTemplate = require('./export.html.slim')
ordersTemplate = require('./_orders.html.slim')
mobileOrdersTemplate = require('./_mobile_orders.html.slim')
formTemplate = require('./_form.html.slim')
usersTemplate = require('../users/_users.html.slim')
mobileUsersTemplate = require('../users/_mobile_users.html.slim')

angular.module('subscription', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.payments',
        abstract: true,
        url: '/payments'
        data:
          permissions:
            except: ['EDITOR', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.payments.list',
        url: '/list/:page'
        auth: true
        resolve:
          orders: ($stateParams, Aquarium) -> Aquarium.all('payments').getList(page: $stateParams.page || 1)
          orders_state:  (Aquarium) -> Aquarium.one('payments/count').get()
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          subproducts: (Aquarium) -> Aquarium.all('products/1/subproducts').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          ordersTemplateName: -> ordersTemplate
          type: -> 'payments'
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PaymentsListController'

      .state 'logged.payments.massive',
        url: '/massive'
        auth: true
        resolve:
          namespaces: (Aquarium) -> Aquarium.all('namespaces/unsyncable').getList()
        views:
          'content@logged':
            templateUrl: massiveTemplate
            controller: 'PaymentsMassiveController'

      .state 'logged.payments.search',
        url: '/search/:page?q?product_title?subproduct_title?status_title?paymethod_title?namespace_title?period_title?date_from?date_to'
        auth: true
        resolve:
          orders: ($stateParams, Search) -> Search.payments($stateParams)
          orders_state: (orders) ->
            payments: orders.stat.total
            per_page: orders.stat.per_page
            pages: Math.ceil(orders.stat.total/orders.stat.per_page)
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          subproducts: (Aquarium) -> Aquarium.all('products/1/subproducts').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          type: -> 'payments'
          ordersTemplateName: -> ordersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PaymentsListController'

      .state 'logged.payments.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          order: (Aquarium, $stateParams) -> Aquarium.one('payments', $stateParams.id).get()
          payment_kits: (Aquarium) -> Aquarium.all('payment_kits').getList()
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          usersTemplateName: -> usersTemplate
          type: -> 'payments'
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'PaymentsEditController'

      .state 'logged.payments.new',
        url: '/new'
        auth: true
        resolve:
          payment_kits: (Aquarium) -> Aquarium.all('payment_kits').getList()
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          type: -> 'payments'
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'PaymentsCreateController'

      .state 'logged.payments.export',
        url: '/export'
        auth: true
        views:
          'content@logged':
            templateUrl: exportTemplate
            controller: 'PaymentsExportController'

      .state 'logged.payments.mobile',
        abstract: true
        url: '/mobile'
        data:
          permissions:
            except: ['EDITOR', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.payments.mobile.list',
        url: '/list/:page'
        auth: true
        resolve:
          orders: ($stateParams, Aquarium) -> Aquarium.all('payments').getList(mobile: true, page: $stateParams.page || 1)
          orders_state:  (Aquarium) -> Aquarium.one('payments/count').get(mobile: true)
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          subproducts: (Aquarium) -> Aquarium.all('products/1/subproducts').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          ordersTemplateName: -> mobileOrdersTemplate
          type: -> 'payments.mobile'
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PaymentsListController'

      .state 'logged.payments.mobile.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          order: (Aquarium, $stateParams) -> Aquarium.one('payments', $stateParams.id).get(mobile: true)
          payment_kits: (Aquarium) -> Aquarium.all('payment_kits').getList()
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          usersTemplateName: -> mobileUsersTemplate
          type: -> 'payments.mobile'
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'PaymentsEditController'

      .state 'logged.payments.mobile.search',
        url: '/search/:page?q?product_title?subproduct_title?status_title?paymethod_title?namespace_title?period_title?date_from?date_to'
        auth: true
        resolve:
          orders: ($stateParams, Search) ->
            params = $stateParams
            params.mobile = true
            Search.payments(params)
          orders_state: (orders) ->
            payments: orders.stat.total
            per_page: orders.stat.per_page
            pages: Math.ceil(orders.stat.total/orders.stat.per_page)
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          products: (Aquarium) -> Aquarium.all('products').getList()
          subproducts: (Aquarium) -> Aquarium.all('products/1/subproducts').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList()
          paymethods: (Aquarium) -> Aquarium.all('paymethods').getList()
          periods: (Aquarium) -> Aquarium.all('periods').getList()
          type: -> 'payments.mobile'
          ordersTemplateName: -> mobileOrdersTemplate
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'PaymentsListController'

      .state 'logged.payments.mobile.new',
        url: '/new'
        auth: true
        resolve:
          payment_kits: (Aquarium) -> Aquarium.all('payment_kits').getList()
          statuses: (Aquarium) -> Aquarium.one('payments').all('statuses').getList()
          type: -> 'payments.mobile'
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'PaymentsCreateController'


  .controller 'PaymentsMassiveController', ($scope, namespaces, $localStorage, Aquarium, Search, Notify) ->
    $scope.namespaces = namespaces
    $scope.subscriptions = []
    $scope.selectedNamespace = $localStorage.selectedNamespace

    defaultParams =
      reader:
        reader_type: 'User'
      start_date: new Date()
      status: 'paid'

    $scope.findUsers = (pattern) ->
      Search.users(q: pattern).then (json) -> json['found'][0..10]

    $scope.selectNamespace = ->
      $localStorage.selectedNamespace = $scope.selectedNamespace
      Aquarium
        .all("namespaces/#{$scope.selectedNamespace.id}/unsyncable_payment_kits")
        .getList()
        .then (data) -> $scope.products = data
      if !$scope.subscriptions.length
        $scope.subscriptions.push angular.copy defaultParams

    if $localStorage.selectedNamespace then $scope.selectNamespace()

    $scope.buildName = (product) ->
      [product.product.title || product.title, product.subproduct.title, product.period.title].join(' ')

    $scope.selectProduct = (subscribe) ->
      period = subscribe.payment_kit.period
      subscribe.payment_kit_id = subscribe.payment_kit.id
      dateTo = moment()
        .add(period.duration_number, period.measure_unit)
        .subtract(1, 'days')
        .format('DD-MMMM-YYYY')
      subscribe.date_to = dateTo
      if _.last( $scope.subscriptions ).reader.reader_id
        $scope.subscriptions.push angular.copy defaultParams

    $scope.changeStartDate = (subscribe) ->
      period = subscribe.payment_kit.period
      subscribe.date_to = moment(subscribe.start_date)
        .add(period.duration_number, period.measure_unit)
        .subtract(1, 'days')
        .format('DD-MMMM-YYYY')

    $scope.subscribeAll = () ->
      _.each $scope.subscriptions, (order) ->
        if order.reader.reader_id? && order.payment_kit_id?
          Aquarium
            .all('payments')
            .post(attributes: order)
            .then (created_order) ->
              Notify.add msg: 'Пользователь подписан.', type: 'success'
            , (data) ->
              Notify.add msg: "Пользователь ##{order.reader.reader_id} не подписан.", type: 'error'


  .controller 'PaymentsListController', ($scope, orders, type, orders_state, $stateParams, ordersTemplateName, $state, Search, OrdersSettings, statuses, products, subproducts, namespaces, paymethods, periods, Aquarium) ->
    $scope.ordersTemplate = ordersTemplateName
    $scope.paymentsEditLink = "logged.#{type}.edit"
    $scope.type = type

    $scope.filter =
      q:              $stateParams.q || ''
      status_title: $stateParams.status_title
      product_title: $stateParams.product_title || products[0]?.title
      paymethod_title: $stateParams.paymethod_title
      namespace_title:        $stateParams.namespace_title
      subproduct_title:        $stateParams.subproduct_title
      period_title:        $stateParams.period_title
      date_from:      $stateParams.date_from || moment(new Date()).startOf('month').format('YYYY-MM-DD')
      date_to:        $stateParams.date_to || new Date()
      page:           $stateParams.page || 1

    $scope.today = new Date()

    $scope.orders = orders.found || orders
    $scope.stat = orders_state
    $scope.per_page = orders_state.per_page || 50
    $scope.statuses = statuses

    $scope.methods  = paymethods
    $scope.products = products
    $scope.subproducts = subproducts
    $scope.namespaces = namespaces
    $scope.periods = periods

    $scope.changeProductTitle = () ->
      product = _.find products, { title: $scope.filter.product_title }
      if product?
        Aquarium.all("products/#{product.id}/subproducts").getList().then (subproducts) ->
          $scope.subproducts = subproducts

    $scope.to_page = ->
      if document.location.href.match(/search/)
        $scope.go()
      else
        $state.go("logged.#{type}.list", page: $scope.filter.page)

    $scope.go = ->
      full_query = angular.copy($scope.filter)
      full_query.date_from = moment($scope.filter.date_from).format('YYYY-MM-DD') if $scope.filter.date_from
      full_query.date_to = moment($scope.filter.date_to).format('YYYY-MM-DD') if $scope.filter.date_to

      $state.go("logged.#{type}.search", full_query)

    $scope.autopay_icon = (order) ->
      status = if null == order.autopayment then false else order.autopayment
      if status then 'star' else 'star-empty'

    $scope.slx_icon = (order) ->
      if order.synced then 'star' else 'star-empty'

    $scope.localize = (key, value) ->
      if OrdersSettings[key]?[value]?
        OrdersSettings[key][value]
      else
        value

    $scope.reset = ->
      $state.go("logged.#{type}.list", page: 1)


  .controller 'PaymentsEditController', ($scope, order, type, UserSettings, usersTemplateName, Notify, OrdersSettings, payment_kits, statuses) ->
    $scope.settings = OrdersSettings
    $scope.editUserLink = "logged.users#{_.replace(type, 'payments', '')}.edit"
    $scope.newUserLink = "logged.users#{_.replace(type, 'payments', '')}.new"
    $scope.statuses = statuses
    $scope.payment_kits = payment_kits
    $scope.formTemplate = formTemplate
    $scope.usersTemplate = usersTemplateName

    $scope.order = order

    if $scope.order.start_date?
      $scope.order.start_date = new Date($scope.order.start_date)
    if $scope.order.end_date?
      $scope.order.end_date = new Date($scope.order.end_date)
    userField = if type == 'payments' then 'user' else 'mobile_user'
    $scope.users = [order[userField]]

    $scope.naming = (kit) ->
      [kit.namespace.title, kit.subproduct.title, kit.product.title, kit.period.title].join(' ')

    $scope.blocked = (user) -> _.includes(['blocked', 'banned', 'deleted'], user.status) if user
    $scope.status_icon = (user) -> UserSettings.states_icons[user.status] if user
    $scope.autopay_icon = (user) ->
      return 'star-empty' unless user?
      status = if null == user.autopayment then false else user.autopayment
      if status then 'star' else 'star-empty'

    $scope.create_or_update = (order) ->
      order.patch().then (new_order) ->
        if new_order.start_date? then new_order.start_date = new Date(new_order.start_date)
        if new_order.end_date? then new_order.end_date = new Date(new_order.end_date)
        $scope.order = new_order
        Notify.add msg: 'Заказ изменен.', type: 'success'


  .controller 'PaymentsCreateController', ($scope, type, UserSettings, Notify, Search, Aquarium, $state, OrdersSettings, payment_kits, statuses) ->
    $scope.settings = OrdersSettings
    $scope.statuses = statuses
    $scope.payment_kits = payment_kits
    $scope.formTemplate = formTemplate

    $scope.naming = (kit) ->
      [kit.namespace.title, kit.subproduct.title, kit.product.title, kit.period.title].join(' ')

    $scope.order =
      reader:
        reader_type: if type == 'payments.mobile' then 'MobileUser' else 'User'

    $scope.find_users = (pattern) ->
      if type == 'payments.mobile'
        Search.mobile_users(q: pattern).then (json) -> json['found'][0..10]
      else
        Search.users(q: pattern).then (json) -> json['found'][0..10]

    $scope.select_user = (user) -> $scope.order.reader.reader_id = user.id

    $scope.create_or_update = (order) ->
      order.status = 'paid'
      Aquarium.all('payments').post(attributes: order).then (created_order) ->
        Notify.add msg: 'Заказ создан.', type: 'success'
        $state.go("logged.#{type}.edit", id: created_order.id)


  .controller 'PaymentsExportController', ($scope, $http, Editor, Settings) ->

    $scope.download = ->
      dateStart = moment($scope.dateStart).format()
      dateEnd = moment($scope.dateEnd).format()
      $http
        .get "#{Settings.aquarium_api_url}/payments/export?start_date=#{dateStart}&end_date=#{dateEnd}",
          headers:
            "X-Access-Token": Editor.access_token()
          responseType: 'blob'
        .then (data) ->
          if navigator.appVersion.toString().indexOf('.NET') > 0
            file = new Blob [ data.data ], 'type': data.headers('Content-Type')
            window.navigator.msSaveOrOpenBlob file, file.name
          else
            file = new Blob [ data.data ], 'type': data.headers('Content-Type')
            $('#download').attr
              'href': URL.createObjectURL(file)
              'download': file.name or 'file-' + $.now()
            .get(0)
            .click()
