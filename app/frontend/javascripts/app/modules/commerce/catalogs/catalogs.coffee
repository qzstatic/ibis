listTemplate = require('./list.html')
showTemplate = require('./show.html')

angular.module('catalogs', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.catalogs',
        abstract: true,
        url: '/catalogs'
        
      .state 'logged.catalogs.list',
        url: '/list'
        auth: true
        resolve:
          catalogs: (Aquarium) -> Aquarium.all('options/types').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'CatalogsListController'

      .state 'logged.catalogs.show',
        url: '/:id/edit'
        auth: true
        resolve:
          catalog: (Aquarium, $stateParams) -> Aquarium.all("options/by_type").getList(type: $stateParams.id)
        views:
          'content@logged':
            templateUrl: showTemplate
            controller: 'CatalogsShowController'
            
  .controller 'CatalogsListController', ($scope, catalogs) ->
    $scope.catalogs = catalogs

  .controller 'CatalogsShowController', ($scope, $stateParams, catalog, Aquarium) ->
    $scope.catalog        = catalog
    $scope.catalog_slug = $stateParams.id

    $scope.reset = ->
      $scope.current_item   =
        type: $stateParams.id

    $scope.reset()

    $scope.edit = (item) -> item.edit = true

    $scope.update = (item) ->
      Aquarium.one('options', item.id).patch(item)
      item.edit = false

    $scope.create = (new_item) ->
      new_item.position = _.last($scope.catalog).position + 1000
      Aquarium.all('options').post(attributes: new_item).then (created_item) ->
        $scope.catalog.push created_item
        $scope.reset()

    reorder = ->
      for item, index in $scope.catalog
        item.position = ( index + 1 ) * 1000
        Aquarium.one('options', item.id).patch(item)

    $scope.sortable_handlers =
      orderChanged: ->
        reorder()
