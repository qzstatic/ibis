listTemplate = require('./list.html')
showTemplate = require('./show.html')

angular.module('inline_reports', [])

  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.inline_reports',
        abstract: true,
        url: '/inline_reports'
        
      .state 'logged.inline_reports.list',
        url: '/list'
        auth: true
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'InlineReportsListController'

      .state 'logged.inline_reports.show',
        url: '/show/:kind?:start_date&:end_date'
        auth: true
        views:
          'content@logged':
            templateUrl: showTemplate
            controller: 'InlineReportsShowController'
            
  .controller 'InlineReportsListController', ($scope, $state) ->
    $scope.reports_list = {
      'authors':   'Отчет по авторам заметок за период'
    }

    $scope.generate = (filter) ->
      $state.go('logged.inline_reports.show', $scope.filter)


  .controller 'InlineReportsShowController', ($scope, $stateParams, $http, Editor) ->
    $scope.content = 'Загрузка'

    if 'authors' == $stateParams.kind
      $http.get("/reports/authors?start_date=#{$stateParams.start_date}&end_date=#{$stateParams.end_date}&token=#{Editor.access_token()}").then (resp) ->
        $scope.content = resp.data 
