loggedTemplate = require('./layouts/logged.html.slim')
loginFormTemplate = require('./login_form.html')
notLoggedTemplate = require('./layouts/not_logged.html')
errorsTemplate = require('./layouts/errors.html')

angular.module('auth', ['services'])
  .config ($stateProvider) ->

    $stateProvider

      .state 'errors',
        abstract: true
        url: '/errors',
        views:
          main:
            templateUrl: errorsTemplate

      .state 'errors.404',
        url: '/404',
        views:
          content:
            template: '404: Страница не найдена'
            
      .state 'errors.403',
        url: '/403',
        views:
          content:
            template: '403: Доступ запрещен'
    
      .state 'logged',
        abstract: true
        resolve:
          editors: (EditorsService) -> EditorsService.list()
        views:
          main:
            templateUrl: loggedTemplate

      .state 'logged.logout',
        url: '/logout',
        views:
          content:
            controller: ($scope, $state, Editor) ->
              Editor.logout() if Editor.is_logged()
              $state.go('not_logged.login')

      .state 'not_logged',
        abstract: true
        views:
          main:
            templateUrl: notLoggedTemplate
      
      .state 'not_logged.login',
        url: '/login'
        views:
          form:
            templateUrl: loginFormTemplate
            controller: 'LoginFormController'

      .state 'not_logged.main',
        url: '/'
        views:
          form:
            controller: (Editor, $state, Settings) -> 
              $state.go(if Editor.is_logged() then 'logged.sandboxes.list' else 'not_logged.login')

  .directive 'rmAuth', ->
    restrict: 'AE'
    controller: ($rootScope, $state, Editor, Settings, GroupsService, $scope) ->
    
      $rootScope.active_link = (url) -> url? && window.location.href.match(url)
      isEditorInGroup = (group) -> if group then _.includes group.editor_ids, Editor.id()
      
      auth = {}
      
      auth.isEditor = ->
        group = _.find GroupsService.groups, {'slug': 'web_editors'}
        isEditorInGroup group
        
      auth.isCommers = ->
        group = _.find GroupsService.groups, {'slug': 'advertisers'}
        isEditorInGroup group
        
      auth.isSubscription = ->
        group = _.find GroupsService.groups, {'slug': 'subscription'}
        isEditorInGroup group
        
      auth.isAdmin = ->
        group = _.find GroupsService.groups, {'slug': 'administrators'}
        isEditorInGroup group

      $rootScope.auth = auth

      $rootScope.$on '$stateChangeStart', (event, to_state, to_params, from_state, from_params) ->
        if to_state.auth && !Editor.is_logged()
          event.preventDefault()
          $state.go('not_logged.login')

  .controller 'LoginFormController', ($scope, $state, promiseTracker, Editor, $templateCache, Settings) ->
    $scope.requestTracker = promiseTracker()
    
    $scope.can_login = (editor) ->
      !$scope.requestTracker.active() && editor? && editor.login? && editor.password? && 
      editor.login.length && editor.password.length

    $scope.authenticate = (editor) ->
      $scope.form.$setValidity('error', true)
      promise = Editor.authenticate(editor)
        .success -> 
          document.location.href = Settings.root_url
        .error -> 
          $scope.form.$setValidity('error', false)
      $scope.requestTracker.addPromise(promise)
    
    $scope.login_is_empty  = (login) -> 
      $scope.form.login.$pristine || ( login? && 0 == login.length )
    
    $scope.password_is_empty = (password) -> 
      $scope.form.password.$pristine || ( password? && 0 == password.length )
    
    $scope.remove_has_error_class = -> 
      $scope.form.$setValidity('error', true)
