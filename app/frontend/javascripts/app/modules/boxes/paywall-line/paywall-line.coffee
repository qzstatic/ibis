paywallBorderTemplate = require('./paywall-line.html.slim')

angular.module('boxes')
  .directive 'rmBoxPaywallLine', ->
    restrict: 'E'
    replace: true
    templateUrl: paywallBorderTemplate
    scope:
      box: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
    controller: ($scope, $rootScope, $element, doc_settings) ->
      $scope.title = doc_settings.settings.boxes[$scope.box.type].label
      $scope.$emit 'boxes.pay_required.changed_state', true
