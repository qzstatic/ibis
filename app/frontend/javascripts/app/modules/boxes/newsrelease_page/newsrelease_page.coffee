newsreleasePageTemplate = require('./newsrelease_page.html')

angular.module('boxes')
  .directive 'rmBoxNewsreleasePage', ->
    restrict: 'E'
    replace: true
    templateUrl: newsreleasePageTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
      scope.editor.bootstrap_box(scope.box)

    controller: ($scope, doc_settings, CategoriesService) ->
      $scope.settings = doc_settings.settings
