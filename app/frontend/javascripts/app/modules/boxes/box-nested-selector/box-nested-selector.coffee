boxNestedSelector = require('./box-nested-selector.html')

angular.module('boxes')
  .directive 'rmBoxNestedSelector', ->
    restrict: 'E'
    templateUrl: boxNestedSelector
    scope:
      box: '='
      point: '='
      place: '@'
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
    controller: ($scope, doc_settings) ->
      $scope.settings = doc_settings.settings

      update_box_list = (types)->
        $scope.child_box_types = []
        for type in (types[$scope.box.type].children || [])
          box = types[type]
          $scope.child_box_types.push(type: type, box: box) if box.label?

      update_box_list($scope.settings.boxes)
      $scope.$on 'document.settings.update', (event, settings) -> update_box_list(settings.boxes)
