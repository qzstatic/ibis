defaultTemplate = require('./default.html')

angular.module('boxes')
  .directive 'rmBoxDefault', ->
    restrict: 'E'
    replace: true
    templateUrl: defaultTemplate
    scope:
      box: '='
      point: '='
      nested: '='
      name: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
      scope.editor.bootstrap_box(scope.box, scope, element)
    controller: ($scope, $element, doc_settings, Locker, current_document) ->
      $scope.settings = doc_settings.settings

      $scope.status = {}

      $scope.$parent.checkPublishedBox $scope

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      if $scope.box.type == 'map' && $scope.box.legend_map
        $scope.box.legend_map = JSON.parse $scope.box.legend_map

      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: current_document.id
        part: "box-#{$scope.box.id}"
        reload: ->
          $scope.editor.reload($scope.box.id).then (box) ->
            $scope.box = box
            $scope.box.view_mode = true

      # Изменение бокса
      $scope.$watch 'box', (curr, prev) ->
        if !curr.view_mode
          $scope.$parent.checkChangedBox curr, prev, $scope
      , true
