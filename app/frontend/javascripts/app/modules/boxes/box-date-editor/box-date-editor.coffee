dateEditorTemplate = require('./box-date-editor.html.slim')

angular.module('boxes')
  .directive 'rmBoxDateEditor', ->
    restrict: 'E'
    replace: true
    templateUrl: dateEditorTemplate
    scope:
      box: '='
      root_box: '=rootBox'
    require: '^rmEditor'
    link: (scope, element, attrs, EditorController) ->
      scope.editor = EditorController
    controller: ($scope, $element, doc_settings, Locker, current_document, $filter, Chatter) ->
      $scope.part = current_document.document.part
      $scope.isDocumentOnline = $scope.part == 'online'
      $scope.reverse = false

      $scope.box.created_at_one_way = angular.copy $scope.box.created_at

      $scope.predicate = (obj) =>
        if $scope.isDocumentOnline
          moment(obj.created_at).unix()
        else
          obj.position

      if !$scope.box.created_at && $scope.isDocumentOnline
        $scope.box.created_at = moment().format()

      $scope.updateBox = ->
        $scope.box.created_at = moment(new Date($scope.box.created_at_one_way)).format()
        $scope.editMode = false
        $scope.status.isBoxChanged = true
        $scope.editor
          .update($scope.box)
          .then ->
            $scope.root_box.children = $filter('orderBy')($scope.root_box.children, $scope.predicate, $scope.reverse)
      
      $scope.updateBoxStatus = ->
        $scope.editor
          .update($scope.box)
          .then ->
            $scope.$parent.$broadcast 'box.updateBoxStatus'
            Chatter.update_part(current_document.id, $scope.box)
      
      $scope.changed = ->
        $scope.box.created_at = moment($scope.box.created_at).format()
        $scope.box.created_at
