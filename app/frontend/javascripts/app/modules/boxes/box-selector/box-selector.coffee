boxSelectorTemplate = require('./box-selector.html')

angular.module('boxes')
  .directive 'rmBoxSelector', ->
    restrict: 'E'
    templateUrl: boxSelectorTemplate
    scope:
      box: '='
      position: '='
    controller: ($scope, doc_settings) ->
      $scope.settings = doc_settings.settings

      $scope.align = (align) -> $scope.new_box = { align: align }

      $scope.add = (type) ->
        $scope.new_box.type = type
        $scope.new_box.text_align = 'left' if 'paragraph' == type
        $scope.$parent.add($scope.new_box, null, $scope.box)
        $scope.reset()

      $scope.addTop = (type) ->
        $scope.new_box.type = type
        $scope.new_box.text_align = 'left' if 'paragraph' == type
        $scope.$parent.add($scope.new_box, null, false, true, 'top')
        $scope.reset()

      $scope.reset = -> $scope.new_box = null

      update_box_list = (types)->
        $scope.only_root_level = []
        if !types['root'] then return
        for type in (types['root'].children || [])
          box = types[type]
          $scope.only_root_level.push(type: type, box: box) if box.label? && type != 'paywall_line'

      update_box_list($scope.settings.boxes)
      $scope.$on 'document.settings.update', (event, settings) ->
        update_box_list(settings.boxes)



