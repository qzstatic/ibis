testTemplate = require('./test.html')

angular.module('boxes')
  .directive 'rmBoxTest', ->
    restrict: 'E'
    replace: true
    templateUrl: testTemplate
    scope:
      box: '='
    controller: ($scope, $element, Agami, Settings, Notify, current_document) ->

      $scope.Settings = Settings

      $scope.box.json = if $scope.box.json then JSON.parse($scope.box.json) else {}
      $scope.box.json.questions = [] if !$scope.box.json.questions
      $scope.box.json.results = [] if !$scope.box.json.results

      $scope.isVariantsVisible = true
      $scope.isResultVisible = true

      $scope.document = current_document.document

      $scope.hightlightInput = false

      questionSchema =
        title: null
        righttext: null
        wrongtext: null
        image: {}
        choices: []
        type: 'grid'
        isQuestionVisible: true

      variantSchema =
        text: null
        weight: 0
        image: {}

      resultSchema =
        text: null
        min_weight: 0
        max_weight: 0
        sharing_title: null
        sharing_text: null
        image: {}

      maxWeights = []

      $scope.getMaxWeight = -> _.sum maxWeights

      calculateMaxWeight = (choices) ->
        if choices.length && max = _.maxBy(choices, (choice) -> choice.weight)
          max.weight
        else
          0

      $scope.$watch 'box.json.questions', (questions) ->
        _.each questions, (item, index) -> maxWeights[index] = calculateMaxWeight item.choices
      , true


      $scope.addQuestionTop = -> $scope.box.json.questions.unshift angular.copy questionSchema

      $scope.addQuestionBottom = -> $scope.box.json.questions.push angular.copy questionSchema


      $scope.addVariantTop = (choices) -> choices.unshift angular.copy variantSchema

      $scope.addVariantBottom = (choices) -> choices.push angular.copy variantSchema


      $scope.addResultTop = -> $scope.box.json.results.unshift angular.copy resultSchema

      $scope.addResultBottom = -> $scope.box.json.results.push angular.copy resultSchema


      $scope.removeQuestion = (index) -> $scope.box.json.questions.splice(index, 1)

      $scope.removeVariant = (index, choices) -> choices.splice(index, 1)

      $scope.removeResult = (index) -> $scope.box.json.results.splice(index, 1)


      $scope.uploadImage = (files, item) ->
        Agami
          .upload(_.first(files), false)
          .then (data) -> item.image = data.versions.original.url

      $scope.saveQuestion = ->
        box = angular.copy $scope.box
        box.json = JSON.stringify $scope.box.json
        $scope.$parent.update(box, null).then (data) ->
          $scope.box.updated_at = box.updated_at
          Notify.add msg: "Тест сохранен!", type: 'success'

      $scope.$on 'document.checkRequiredFields', (e, highlight) ->
        $scope.highlightInput = highlight
