editorTemplate = require('./editor.html.slim')

angular.module('boxes', ['ngSanitize', 'ui'])
  .directive 'rmEditor', ->
    restrict: 'E'
    replace: true
    templateUrl: editorTemplate
    scope:
      root_box: '=rootBox'
      document: '='
    controller: ($scope, $rootScope, Snipe, $timeout, $filter, Notify, doc_settings, current_document, EditorSettings, Chatter, Locker, $q, Atasus, BoxesService, PaywallService) ->

      @document = -> current_document.document
      view_settings = doc_settings.settings
      $scope.isDocumentOnline = @document().part == 'online'
      $scope.reverse = false
      typesWithoutCheckPublishedBox = ['gallery_image', 'test']

      getPaywallDefaultPosition = -> PaywallService.position - 1

      # Проверка опубликован ли бокс
      $scope.checkPublishedBox = (scope, isPublish = false) ->
        if isPublish
          BoxesService.removeUnpublished scope.box.id
          scope.status.isUnpublished = false
          return
        if !scope.box.enabled && $scope.isDocumentOnline
          BoxesService.addUnpublished scope.box.id
          scope.status.isUnpublished = true
          return
        _.isEqualWith scope.box, scope.box.packed_box, (obj, packed) ->
          if _.isEmpty packed
            BoxesService.addUnpublished scope.box.id
            scope.status.isUnpublished = true
            return
          for key, val of packed
            if key == 'created_at' && !packed[key] then continue
            if packed.bound_document?.id?
              if obj.bound_document_id != packed.bound_document.id
                BoxesService.addUnpublished scope.box.id
                scope.status.isUnpublished = true
                return
            if obj[key]? && packed[key] != obj[key]
              # Сравниваем дочерние боксы если есть
              if packed.children?
                _.each packed.children, (child, i) ->
                  for key2, val2 of child
                    if obj.children[i][key2]? && packed.children[i][key2] != obj.children[i][key2]
                      BoxesService.addUnpublished scope.box.id
                      scope.status.isUnpublished = true
                      return
                return
              else
                BoxesService.addUnpublished scope.box.id
                scope.status.isUnpublished = true
                return
          BoxesService.removeUnpublished scope.box.id
          scope.status.isUnpublished = false

      # Проверка изменен ли бокс
      $scope.checkChangedBox = (curr, prev, scope) ->
        if (curr != prev)
          isChanged = true
          if (!curr.view_mode? || !prev.view_mode?) || (curr.view_mode != prev.view_mode)
            isChanged = false
          else if (curr.moved_at != prev.moved_at)
            isChanged = false
        else
          isChanged = false
        scope.status.isBoxChanged = isChanged
        if isChanged
          BoxesService.addUnsaved(curr.id)
        else
          BoxesService.removeUnsaved(curr.id)

      # Сортируем по дате создания, или по позиции
      $scope.predicate = (obj) =>
        if $scope.isDocumentOnline
          moment(obj.created_at).unix()
        else
          obj.position

      # Находим индекс бокса линии пейвола
      $scope.getPaywallIndex = () -> _.findIndex $scope.root_box.children, type: 'paywall_line'

      $scope.paywall =
        getDefaultPosition: getPaywallDefaultPosition
        defaultPosition: getPaywallDefaultPosition()
        getIndex: $scope.getPaywallIndex
        index: $scope.getPaywallIndex()

      $scope.paywallLineIndex = $scope.getPaywallIndex()

      # Функции проверки нахождения линии пейвола для отрисовки границ
      $scope.beforePaywallLine = (index) ->
        index < $scope.paywall.getIndex()

      $scope.afterPaywallLine = (index) ->
        paywallLineIndex = $scope.paywall.getIndex()
        index > paywallLineIndex && paywallLineIndex != -1

      $scope.$on 'document.finishedBroadcastUpdate', (event, finished) ->
        if !$scope.isDocumentOnline then return
        if finished == 'false' then finished = false
        if finished == 'true' then finished = true
        if $scope.reverse == finished then return
        $scope.reverse = finished
        $scope.root_box.children = $filter('orderBy')($scope.root_box.children, $scope.predicate, $scope.reverse)

      # Пересортировывает все параграфы по списку и текущему состоянию массива
      $scope.reorder = (boxes) =>
        defer = $q.defer()
        boxesParams = []
        _.map boxes, (box, index) ->
          box.position = (index + 1) * EditorSettings.step
          boxesParams.push
            id: box.id,
            position: box.position
        Snipe
          .all("boxes/multi_move")
          .post(attributes:
            boxes: boxesParams,
            moved_at: @document().box_move_at || null
          )
          .then (response) =>
            @document().box_move_at = response.moved_at
            for box, index in boxes
              box.index = index
              box.moved_at = response.moved_at
              Chatter.move_part current_document.id, box
            defer.resolve boxes
          , () =>
            defer.reject boxes
        defer.promise

      load_attachments = (box) ->
        # Загружаем информацию о картинках
        if view_settings[box.type]?.attributes?
          for control in view_settings[box.type].attributes
            if 'image' == control.type && box[control.name] > 0
              box.attachments = {}
              Snipe.one('attachments', box[control.name]).get().then (attachment) ->
                box.attachments[control.name] = attachment

      # Редактор пустой или нет?
      $scope.empty = (point = $scope.root_box.children) ->
        !point || (angular.isArray(point) && 0 == point.length)

      @set_view_mode = (box, view_mode = null) ->
        if view_mode? then box.view_mode = view_mode else box.view_mode = !box.view_mode

      @bootstrap_box = (box, scope = null, element = null) ->
        unless 'paragraph' == box.type
          @set_view_mode(box, true) unless box.view_mode?
          box.width = 'w1' if box.width? && '' == box.width

        # Устанавливаем значение по умолчанию для выравнивая текста в боксе
        box.text_align = 'left' if box.text_align? && '' == box.text_align

        load_attachments(box)

      @box_align = (box, align) ->
        box.align = align
        Snipe.one('boxes', box.id).patch({align: align, updated_at: box.updated_at}).then (newBox) =>
          box.updated_at = newBox.updated_at

      @box_width = (box, width) ->
        box.width = width
        Snipe.one('boxes', box.id).patch({width: width, updated_at: box.updated_at}).then (newBox) =>
          box.updated_at = newBox.updated_at

      $scope.$on 'part:create', ($event, data) ->
        box = _.find $scope.root_box.children, { id: data.id }

        if data.type == 'paywall_line'
          $scope.paywall.index = data.fixed_at
          data.index = data.fixed_at

        if !box
          $scope.addHtml data

        if $scope.document.pay_required? && $scope.document.pay_required == true && data.type != 'paywall_line'
          paywallBox = $scope.root_box.children[$scope.paywall.getIndex()]
          if paywallBox
            paywallBox.index = paywallBox.fixed_at
            moveBox paywallBox


      $scope.$on 'part:update', ($event, data) ->
        box = _.find $scope.root_box.children, { id: data.id }
        if box
          $scope.update data
          if data.enabled?
            box.enabled = data.enabled

          if data.type == 'paywall_line' && $scope.document.pay_required? && $scope.document.pay_required == true
            box.fixed_at = data.fixed_at
            $scope.paywall.index = data.fixed_at

          if $scope.document.pay_required? && $scope.document.pay_required == true && data.type != 'paywall_line'
            paywallBox = $scope.root_box.children[$scope.paywall.getIndex()]
            if paywallBox
              paywallBox.index = paywallBox.fixed_at
              moveBox paywallBox

      $scope.$on 'part:delete', ($event, data) =>
        if _.find $scope.root_box.children, { id: data.id }
          $scope.removeHtml data

        if $scope.document.pay_required? && $scope.document.pay_required == true && data.type != 'paywall_line'
          paywallBox = $scope.root_box.children[$scope.paywall.getIndex()]
          if paywallBox
            paywallBox.index = paywallBox.fixed_at
            moveBox paywallBox

      $scope.$on 'part:moved', ($event, data) ->
        if _.find $scope.root_box.children, { id: data.id }
          if data.type == 'paywall_line' && $scope.document.pay_required? && $scope.document.pay_required == true
            data.index = data.fixed_at
            $scope.paywall.index = data.fixed_at
          moveBox data

      # Обновление галереи по websocket
      $scope.$on 'part:galleryUpdated', ($event, data) ->
        $scope.reload(data.galleryId).then (savedBox) ->
          boxes = $scope.root_box.children
          _.each boxes, (box, index) ->
            if box.id == savedBox.id then boxes[index] = savedBox

      $scope.$on 'document.settings.update', ($event) ->
        $scope.paywallDefaultPosition = $scope.paywall.getDefaultPosition()

      # Обновление линии пейвола, когда создано несколько параграфов за один раз
      $scope.$on 'paragraph.multparagraphsAdded', (e) ->
        if $scope.document.pay_required? && $scope.document.pay_required == true
          $scope.updatePaywallPosition(current_document.document.box_move_at)

      # Добавляет html бокса
      $scope.addHtml = (box) =>
        box.view_mode = false
        $scope.root_box.children = [] unless $scope.root_box.children
        index = _.sortedIndex $scope.root_box.children, box, 'position'
        if box.index
          $scope.root_box.children.splice box.index, 0, box
        else
          $scope.root_box.children.splice index, 0, box

      # Добавляет бокс определенного типа
      $scope.add = (box = { type: 'paragraph', kind: 'plain', text_align: 'left' }, point = $scope.root_box, prev_box, set_focus = true, place = 'bottom') ->
        if box.type == 'paywall_line' && $scope.root_box.children? && $scope.root_box.children.filter((b) -> b.type == 'paywall_line').length > 1
          return false

        $rootScope.appendedParagraph = ''
        point.children = [] unless point.children?
        box.parent_id  = point.id

        # Бокс будет активным если это параграф или если документ не опубликован или если это не трансляция
        box.enabled = 'paragraph' == box.type || !$scope.document.published || !$scope.isDocumentOnline
        box.created_at = moment().format()
        prev_index = _.indexOf(point.children, prev_box) if prev_box?

        # Выбор правильной позиции для вставки бокса
        if $scope.empty(point.children)
          box.position = EditorSettings.step
        else
          if prev_box && prev_box != _.last(point.children)
            box.position = prev_box.position + Math.ceil((point.children[prev_index + 1].position - prev_box.position)/2)
          else
            if 'top' == place
              box.position = _.first(point.children).position - EditorSettings.step
            else
              box.position = _.last(point.children).position + EditorSettings.step

        Snipe.all('boxes').post(attributes: box).then (new_box) ->
          new_box.view_mode = false

          if prev_box
            $rootScope.currentParagraphIndex = prev_index + 1 if 'paragraph' == box.type
            point.children.splice(prev_index + 1, 0, new_box)
          else
            if 'top' == place
              point.children.unshift(new_box)
            else
              point.children.push(new_box)
            $rootScope.currentParagraphIndex = point.children.length if 'paragraph' == box.type

          new_box.index = $rootScope.currentParagraphIndex

          if new_box.type == 'gallery_image' then new_box.galleryId = point.id

          Chatter.create_part(current_document.id, new_box)

          $scope.updatePaywallPosition(current_document.document.box_move_at) if $scope.document.pay_required? && $scope.document.pay_required == true

          # TODO: удалить если все ок
          # Айдишники для сортировки поехали, нужно сделать реордер
          # $scope.reorder(point.children) if prev_box? && point.children[prev_index + 2]? && box.position == point.children[prev_index + 2].position

          if set_focus
            $timeout (-> $scope.$broadcast 'editor.focus_to', boxId: new_box.id), 0

      # Удаляет HTML бокса
      $scope.removeHtml = (box) ->
        boxes = []
        boxes = $filter('filter')($scope.root_box.children, (model) -> model.id != box.id)
        $scope.root_box.children = boxes

      # Удаляет бокс
      $scope.remove = (point = $scope.root_box, box) ->
        Snipe.one('boxes', box.id).remove().then ->
          index = _.indexOf(point.children, box) || 1
          point.children = $filter('filter')(point.children, (model)-> model.id != box.id)
          if box.type == 'gallery_image' then box.galleryId = point.id
          if box.type == 'paywall_line' then paywallLineOnEventUpdateDispatcher false
          Chatter.delete_part(current_document.id, box)
          BoxesService.addDeleted(box.id)
          BoxesService.removeUnsaved(box.id)

          if $scope.document.pay_required? && $scope.document.pay_required == true && $scope.root_box.children.length > 1
            $scope.updatePaywallPosition(current_document.document.box_move_at)

      # Удаление бокса по клику на иконку
      $scope.removeManual = (point, box) ->
        if result = confirm("Вы действительно хотите удалить #{box.type}?")
          $scope.remove(point, box)

      # Перемещаем бокс на его индекс
      moveBox = (box) =>
        if $rootScope.currentParagraphId && $("#external-#{$rootScope.currentParagraphId}").length
          offsetWindow = $("#external-#{$rootScope.currentParagraphId}")[0].getBoundingClientRect().top
        $scope.removeHtml(box)
        boxes = $scope.root_box.children
        boxes.splice(box.index, 0, box)
        @document().box_move_at = box.moved_at
        if $rootScope.currentParagraphId && boxes[$rootScope.currentParagraphIndex]?
          $timeout ->
            element = $("#external-#{$rootScope.currentParagraphId}")
            offsetDocument = element.offset().top
            scrollTop = offsetDocument - offsetWindow
            $(window).scrollTop(scrollTop)
          , true
        if box.type == 'paywall_line'
          $scope.paywall.index = $scope.paywall.getIndex()

      # Изменение бокса
      $scope.update = (box, notify = true, status = {}) ->
        if box.legend_map?
          if box.legend_map.color_domain.length == box.legend_map.color_range.length
            removedDomain = box.legend_map.color_domain.pop()
          box.legend_map.color_domain = box.legend_map.color_domain.map (str) -> +str
          box.legend_map = JSON.stringify box.legend_map
        Snipe.one('boxes', box.id)
          .patch(box)
          .then (newBox) =>
            box.updated_at = newBox.updated_at
            if box.legend_map?
              box.legend_map = JSON.parse box.legend_map
              if removedDomain?
                box.legend_map.color_domain.push(removedDomain)
            if box.type != 'paragraph'
              Chatter.unblock_part(current_document.id, "box-#{box.id}")
              Notify.add(type: 'success', msg: 'Бокс успешно изменен.') if notify
              box.view_mode = true
              load_attachments(box)
              status.isBoxChanged = false
            if box.type not in typesWithoutCheckPublishedBox
              $scope.checkPublishedBox {status: status, box: box}
            BoxesService.removeUnsaved(box.id)

            if box.type == 'paywall_line'
              $scope.paywall.index = $scope.paywall.getIndex()

            if newBox.type == 'inset_link'
              correctPaywallPosition()

      $scope.reload = (id) -> Snipe.one('boxes', id).get()

      updateBoxPosition = (currentBox, boxes) =>
        Snipe
          .all("boxes/#{currentBox.id}/move")
          .post(attributes:
            position: currentBox.position,
            moved_at: @document().box_move_at || null
          ).then (response) =>
            currentBox.moved_at = response.moved_at
            @document().box_move_at = response.moved_at
            Chatter.move_part current_document.id, currentBox

          , console.error

      boxPositionUpdateDispatcher = (newBoxIndex) ->
        boxes = $scope.root_box.children
        currentBox = boxes[newBoxIndex]
        prevBox = boxes[newBoxIndex - 1]
        nextBox = boxes[newBoxIndex + 1]
        currentBox.index = newBoxIndex
        if prevBox
          if currentBox != _.last(boxes)
            if Math.ceil((nextBox.position - prevBox.position) / 2) in [0,1]
              $scope.reorder boxes
            else
              currentBox.position = prevBox.position + Math.ceil((nextBox.position - prevBox.position) / 2)
              updateBoxPosition currentBox
          else
            currentBox.position = prevBox.position + EditorSettings.step
            updateBoxPosition currentBox
        else
          currentBox.position = nextBox.position - EditorSettings.step
          updateBoxPosition currentBox



      addPaywall = ->
        boxes = $scope.root_box.children
        if boxes?.filter((box) -> box.type == 'paywall_line').length > 1
          return false
        box = type: 'paywall_line', fixed_at: $scope.paywall.getDefaultPosition()
        if boxes?.length >= $scope.paywall.getDefaultPosition()
          prevBox = boxes[$scope.paywall.getDefaultPosition()]
          if PaywallService.isBoxExcluded(prevBox)
            prevBox = boxes[$scope.paywall.getDefaultPosition() - 1]
          $scope.add box, null, prevBox
        else
          if boxes?.length > 0
            prevBox = _.last boxes
            if PaywallService.isBoxExcluded(prevBox)
              prevBox = boxes[box.length - 2]
            $scope.add box, null, prevBox
          else
            $scope.add box, null, false, true, 'top'
        return true

      # Обновляем позицию пейвола
      $scope.updatePaywallPosition = (box_move_at) ->
        currentPaywallIndex = $scope.paywall.getIndex()
        paywallIndex = $scope.paywall.index
        boxes = $scope.root_box.children

        if $scope.paywall.index == currentPaywallIndex && currentPaywallIndex != -1
          if !PaywallService.isBoxExcluded(boxes[currentPaywallIndex - 1])
            return false

        if $scope.document.pay_required? && $scope.document.pay_required == true && currentPaywallIndex == -1
          if !addPaywall() then return
          currentPaywallIndex = $scope.paywall.getIndex()
          boxes = $scope.root_box.children
        else return false if !$scope.document.pay_required

        paywallBox = boxes[$scope.paywall.getIndex()]
        return false unless paywallBox?
        if paywallBox.fixed_at == $scope.paywall.getDefaultPosition()
          if boxes.length < $scope.paywall.getDefaultPosition()
            $scope.paywall.index = boxes.length - 1
          else
            $scope.paywall.index = $scope.paywall.getDefaultPosition()
        else
          if paywallBox.fixed_at > boxes.length - 1
            $scope.paywall.index = boxes.length - 1
          else
            $scope.paywall.index = paywallBox.fixed_at

        if boxes?.length > 1
          paywallBox.index = $scope.paywall.index
          paywallBox.moved_at = box_move_at if box_move_at?
          moveBox paywallBox
          boxPositionUpdateDispatcher $scope.paywall.index
            .then ->
              $scope.paywall.index = paywallBox.fixed_at
              correctPaywallPosition()

      correctPaywallPosition = () =>
        currentIndex = $scope.paywall.getIndex()
        boxes = $scope.root_box.children
        index = currentIndex
        while true
          if boxes?[index - 1]? && PaywallService.isBoxExcluded(boxes[index - 1])
            index -= 1
          else
            break

        if index != currentIndex
          paywallBox = boxes[currentIndex]
          paywallBox.index = index
          paywallBox.moved_at = @document().box_move_at
          moveBox paywallBox
          boxPositionUpdateDispatcher index
          $scope.paywall.index = index


      paywallLineOnEventUpdateDispatcher = (pay_required) ->
        pay_required = true if pay_required == 'true'
        pay_required = false if pay_required == 'false'
        if pay_required? && current_document.document.pay_required != pay_required
          if !$scope.root_box.children? || $scope.root_box.children.length == 0
            $scope.paywall.index = $scope.paywall.getDefaultPosition()
            $scope.document.pay_required = pay_required
            return
          if pay_required == true
            if $scope.root_box.children?.filter((box) -> box.type == 'paywall_line').length > 0
              $scope.updatePaywallPosition current_document.document.box_move_at
            else
              if !addPaywall() then return
          else
            box = _.find $scope.root_box.children, type: 'paywall_line'
            $scope.remove($scope.root_box, box) if box?

          $scope.paywallLineIndex = $scope.paywall.getDefaultPosition()
          $scope.paywall.index = $scope.paywall.getDefaultPosition()
          $scope.document.pay_required = pay_required
        else if pay_required == true
          if $scope.root_box.children?.filter((box) -> box.type == 'paywall_line').length > 0
            $scope.updatePaywallPosition current_document.document.box_move_at

      $scope.$on 'document.pay_required.changed', ($event, pay_required) ->
        paywallLineOnEventUpdateDispatcher pay_required

      $scope.$on 'paywall.settings', ($event, data) ->
        $scope.paywallDefaultPosition = data.position
        $scope.paywall.defaultPosition = data.position


      $scope.$on 'boxes.paywall_line.changed_state', ($event, data) ->
        paywallLineOnEventUpdateDispatcher data

      $scope.sort_processor =
        orderChanged: (e) =>
          if $scope.document.pay_required? && $scope.document.pay_required == true
            if $scope.root_box.children[e.dest.index].type == 'paywall_line'
              paywall_line = $scope.root_box.children[e.dest.index]
              paywall_line.fixed_at = e.dest.index
              $scope.update(paywall_line, false).then (res) ->
                $scope.paywall.index = e.dest.index
                boxPositionUpdateDispatcher(e.dest.index)
                  .then ->
                    correctPaywallPosition()
            else
              boxPositionUpdateDispatcher(e.dest.index).then =>
                $scope.updatePaywallPosition(@document().box_move_at)
          else
            boxPositionUpdateDispatcher(e.dest.index)

      @update = $scope.update
      @remove = $scope.remove
      @removeManual = $scope.removeManual
      @add    = $scope.add
      @reload = $scope.reload
      @reorder = $scope.reorder
      @checkPublishedBox = $scope.checkPublishedBox
      @checkChangedBox = $scope.checkChangedBox

      @
