galleryImageTemplate = require('./gallery_image.html')

angular.module('boxes')
  .directive 'rmBoxGalleryImage', ->
    restrict: 'E'
    replace: false
    templateUrl: galleryImageTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: ['^rmEditor', '^rmBoxGallery']
    link: (scope, element, attrs, Controllers) ->
      scope.editor = Controllers[0]
      scope.gallery = Controllers[1]
      scope.editor.bootstrap_box(scope.box)

    controller: ($scope, doc_settings, Snipe, Settings, Locker, current_document, $element, Chatter, Utils, BoxesService) ->
      $scope.attachments_host = Settings.attachments_host
      $scope.settings = doc_settings.settings

      $scope.status =
        isBoxChanged: false

      if $scope.box.image_id > 0
        unless $scope.box.attachment?
          Snipe.one('attachments', $scope.box.image_id).get().then (attachment) -> $scope.box.attachment = attachment

      $scope.update = (box) ->
        $scope.editor.update(box, true, $scope.gallery.status).then ->
          if $scope.box.image_id > 0
            Snipe.one('attachments', box.image_id).get().then (attachment) ->
              $scope.box.attachment = attachment
              BoxesService.removeUnsaved(box.id)
              $scope.editor.checkPublishedBox $scope.gallery
            box.galleryId = $scope.gallery.id
            Chatter.unblock_part(current_document.id, "box-#{box.id}")
          else
            $scope.box.attachment = null

      $scope.$watchCollection 'box', (curr, prev) ->
        if !curr.view_mode
          $scope.editor.checkChangedBox curr, prev, $scope

      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: current_document.id
        part: "box-#{$scope.box.id}"
        reload: ->
          $scope.editor.reload($scope.box.id).then (box) ->
            $scope.box = box
            $scope.box.view_mode = true
            if $scope.box.image_id > 0
              Snipe.one('attachments', $scope.box.image_id).get().then (attachment) -> $scope.box.attachment = attachment
            else
              $scope.box.attachment = null
