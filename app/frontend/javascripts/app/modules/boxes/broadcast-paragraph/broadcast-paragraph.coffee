broadcastParagraphTemplate = require('./broadcast-paragraph.html')

angular.module('boxes')
  .directive 'rmBoxBroadcastParagraph', ->
    restrict: 'E'
    replace: true
    templateUrl: broadcastParagraphTemplate
    scope:
      box: '='
    controller: ($scope, $element, $http, Snipe, Notify, $rootScope, current_document, Chatter, Locker, $timeout, $q, Atasus, $filter, Utils) ->

      # Мигание параграфа
      blinkParagraph = (i = 0) ->
        $element.toggleClass('animate-blink')
        i += 1
        $timeout(blinkParagraph, 500, true, i) if i < 6

      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: current_document.id
        part: "box-#{$scope.box.id}"
        reload: ->
          Snipe.one('boxes', $scope.box.id)
            .get()
            .then (box) ->
              box.position = $scope.box.position
              if box.body != $scope.box.body then blinkParagraph()
              $scope.box   = box

      $targetBox = $($element).find('.body')
      $scope.status =
        isBoxChanged: false
      $scope.showForceMessage = false
      isRedactor = false
      forceBox = null
      backupHtml = null

      $scope.$parent.checkPublishedBox $scope

      # Получаем index текущего параграфа
      getCurrentBoxIndex = ->
        _.findIndex $scope.$parent.root_box.children, ( (box) => box.id == $scope.box.id )

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      # Установка фокуса для параграфа по его идентификатору
      $scope.$on 'editor.focus_to', (e, message) ->
        if message.boxId == $scope.box.id && !$scope.locked
          $scope.createRedactor()

      $scope.$watch 'focused', (state) ->
        $($element).prevAll('.glyphicon').toggle(!state)

      # Обновление параграфа
      updateBox = (html) ->
        if $scope.status.isBoxChanged && html != ''
          $scope.$parent.update($scope.box, true, $scope.status)
            .then ->
              $timeout (-> $scope.status.isBoxChanged = false), true
              $scope.Locker.unlock(true)
              # Atasus.add 'Бокс обновлен.', $scope.box
        else
          $scope.Locker.unlock(true)

      $scope.updateBox = ->
        $scope.status.isBoxChanged = true
        $scope.editMode = false
        updateBox $scope.box.body

      # Показываем сообщение о перезаписи параграфа
      forceMessageHandler = (response) ->
        if response.status == 412
          forceBox = response.data.box
          $scope.showForceMessage = true
          $scope.snapshot = response.data.snapshot

      # Удаление параграфа
      removeBox = (box = $scope.box) -> $scope.$parent.remove(null, box)

      # Redactor Callbacks
      onBlurCallback = (e) ->
        if Offline.state == 'down' then return
        # Atasus.add e.type, $scope.box
        @code.sync()
        $scope.focused = false
        html = @code.get().replace(/<i><\/i>/g, '')
        backupHtml = $scope.box.body = html = Utils.replaceQuotes(html)
        htmlSplitted = html.split('<br>')
        $('body').prop 'spellcheck', false
        $targetBox.redactor('core.destroy')
        $timeout (=> $scope.status.isBoxChanged = false), 100
        if _.trim(htmlSplitted.join('')) != ''
          updateBox(html)
        else
          Chatter.unblock_part($scope.id, $scope.part)
          removeBox()

      onInitCallback = ->
        isRedactor = true
        $scope.focused = true
        $rootScope.currentParagraphIndex = getCurrentBoxIndex()
        $rootScope.currentParagraphId = $scope.box.id
        @code.set $scope.box.body
        $('body').prop 'spellcheck', true
        $timeout ( => @$editor.focus() ), 1

      onClickCallback = (e) ->
        $link = $(e.target)
        entry = $link.attr('entry')
        if entry
          if /-(\d+)-/g.test entry
            $link.attr 'entry', entry.replace /-(\d+)-/g, "$1"
          else
            $link.attr 'entry', entry.replace /(\d+)/g, "-\$1-"
          $scope.status.isBoxChanged = true
          @code.sync()
          return
        else if $link.attr('href')
          window.open $link.attr('href'), '_blank'

      # Создание редактора
      $scope.createRedactor = ->
        $targetBox = $($element).find('.body')
        if isRedactor then return
        $targetBox.redactor
          toolbarExternal: "#external-#{$scope.box.id}"
          plugins: ['formatting', 'alignment']
          lang: 'ru'
          focus: false
          linebreaks: true
          pastePlainText: true
          buttons: ['html', 'bold', 'italic', 'link']
          linkTooltip: false

          initCallback: onInitCallback

          destroyCallback: ->
            isRedactor = false
            $($element).find('.external-toolbar').empty()

          changeCallback: (html) -> $scope.status.isBoxChanged = true

          blurCallback: onBlurCallback

          clickCallback: onClickCallback

          pasteCallback: (html) ->
            Utils.replaceQuotes(html)

        null

      # Принудительное обновление параграфа
      $scope.updateForce = ($event) ->
        # Atasus.add $event.type, $scope.box
        $scope.box.updated_at = forceBox.updated_at
        $scope.status.isBoxChanged = true
        updateBox $scope.box.body
        $scope.showForceMessage = false

      # Отменение своих изменений
      $scope.updateRevert = ($event) ->
        # Atasus.add $event.type, $scope.box
        $scope.box = angular.copy forceBox
        $scope.showForceMessage = false
