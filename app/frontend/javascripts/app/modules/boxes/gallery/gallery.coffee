galleryTemplate = require('./gallery.html')

angular.module('boxes')
  .directive 'rmBoxGallery', ->
    restrict: 'E'
    replace: true
    templateUrl: galleryTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
      scope.editor.bootstrap_box(scope.box)

    controller: ($scope, doc_settings, Agami, EditorSettings, Snipe) ->
      $scope.settings = doc_settings.settings

      versions = _.clone(doc_settings.settings.attachments?['gallery_image']?.versions) || {}
      versions[key] = "#{dimensions.width}x#{dimensions.height}>" for key, dimensions of versions
      versions['_preview'] = '110x68^'

      $scope.uploadImages = (images, place, prev_box = false) ->
        _.each images, (image) ->
          Agami
            .upload(image, false, versions)
            .then (data) ->
              $scope.editor.add({ type: 'gallery_image', image_id: data.id }, $scope.box, prev_box, false, place)

      $scope.addSlide = (place, prev_box = false) ->
        $scope.editor.add({ type: 'gallery_image' }, $scope.box, prev_box, false, place)

      $scope.slidesLimit = 30

      $scope.showMoreSlides = -> $scope.slidesLimit += 30

      $scope.status = {}

      $scope.$parent.checkPublishedBox $scope

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      @up = (box) ->
        index =  _.indexOf($scope.box.children, box)
        if index - 1 >= 0
          $scope.box.children.splice(index-1, 0, $scope.box.children.splice(index, 1)[0])
          reorder()

      @down = (box) ->
        index =  _.indexOf($scope.box.children, box)
        if index + 1 <= $scope.box.children.length - 1
          $scope.box.children.splice(index + 1, 0, $scope.box.children.splice(index, 1)[0])
          reorder()

      reorder = ->
        for box, index in $scope.box.children
          box.position = ( index + 1 ) * 1000
          $scope.editor.update(box, false, $scope.status)

      updateImagePosition = (currentBox, boxes) =>
        Snipe
          .one("boxes", currentBox.id)
          .patch(currentBox)
          .then (newBox) ->
            currentBox.updated_at = newBox.updated_at

      $scope.sort_processor =
        orderChanged: (e) ->
          boxes = $scope.box.children
          currentBox = boxes[e.dest.index]
          prevBox = boxes[e.dest.index - 1]
          nextBox = boxes[e.dest.index + 1]
          currentBox.index = e.dest.index
          if prevBox
            if currentBox != _.last(boxes)
              if Math.ceil((nextBox.position - prevBox.position) / 2) in [0,1]
                reorder()
              else
                currentBox.position = prevBox.position + Math.ceil((nextBox.position - prevBox.position) / 2)
                updateImagePosition currentBox, boxes
            else
              currentBox.position = prevBox.position + EditorSettings.step
              updateImagePosition currentBox, boxes
          else
            currentBox.position = nextBox.position - EditorSettings.step
            updateImagePosition currentBox, boxes

      @status = $scope.status
      @box = $scope.box
      @uploadImages = $scope.uploadImages
      @addSlide = $scope.addSlide
      @
