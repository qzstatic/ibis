boxHeader = require('./box-header.html.slim')

angular.module('boxes')
  .directive 'rmBoxHeader', ->
    restrict: 'E'
    templateUrl: boxHeader
    scope:
      box: '='
      point: '='
      nested: '='
      no_view_mode: '=noViewMode'
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
    controller: ($scope, doc_settings, $element, current_document) ->
      
      $scope.settings = doc_settings.settings
      
      $scope.updateBoxStatus = (box) ->
        $scope.editor.update(box).then ->
          $scope.$parent.$broadcast 'box.updateBoxStatus'
