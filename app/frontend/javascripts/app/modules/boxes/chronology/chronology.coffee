chronologyTemplate = require('./chronology.html')

angular.module('boxes')
  .directive 'rmBoxChronology', ->
    restrict: 'E'
    replace: true
    templateUrl: chronologyTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller 
      scope.editor.bootstrap_box(scope.box)
    controller: ($scope, doc_settings, BoxesService) ->
      $scope.settings = doc_settings.settings     
    
      $scope.status = {}
      
      $scope.$parent.checkPublishedBox $scope
     
      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope
     
      # Публикация документа
      $scope.$on 'document.publish', -> 
        $scope.$parent.checkPublishedBox $scope, true
     
      # Изменение бокса
      $scope.$watch 'box', (curr, prev) ->
        $scope.$parent.checkChangedBox curr, prev, $scope
      , true
