boxNestedTemplate = require('./box-nested.html')

angular.module('boxes')
  .directive 'rmBoxNested', ->
    restrict: 'E'
    templateUrl: boxNestedTemplate
    scope:
      box: '='
      point: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller 
    controller: ($scope, doc_settings) ->
      $scope.settings = doc_settings.settings      
      
