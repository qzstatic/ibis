newsreleaseArchiveTemplate = require('./newsrelease_archive.html')

angular.module('boxes')
  .directive 'rmBoxNewsreleaseArchive', ->
    restrict: 'E'
    replace: true
    templateUrl: newsreleaseArchiveTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.editor = Controller
      scope.editor.bootstrap_box(scope.box)
    controller: ($scope, doc_settings, $http, Settings, Notify, current_document) ->
      $scope.settings = doc_settings.settings

      $scope.status =
        isBoxChanged: false

      $scope.generate = ->
        $http.post("#{Settings.aquarium_api_url}/generate_flash?document_id=#{current_document.document.id}").then ->
          Notify.add type: 'success', msg: 'Документ поставлен в очередь на генерацию.'

      $scope.$parent.checkPublishedBox $scope

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      $scope.$watch 'box', (curr, prev) ->
        $scope.$parent.checkChangedBox curr, prev, $scope
      , true
