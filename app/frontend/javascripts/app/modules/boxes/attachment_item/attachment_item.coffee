attachmentTemplate = require('./attachment_item.html')

angular.module('boxes')
  .directive 'rmBoxAttachmentItem', ->
    restrict: 'E'
    replace: true
    templateUrl: attachmentTemplate
    scope:
      box: '='
      point: '='
      nested: '='
    require: '^rmEditor'
    link: (scope, element, attrs, Controller) ->
      scope.status = {}
      
      scope.editor = Controller 
      scope.editor.bootstrap_box(scope.box)
      
    controller: ($scope, doc_settings, Locker, current_document, $element) ->
      $scope.settings = doc_settings.settings
      
      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: current_document.id
        part: "box-#{$scope.box.id}"
        reload: ->
          $scope.editor.reload($scope.box.id).then (box) ->
            $scope.box = box
            $scope.box.view_mode = true
      
