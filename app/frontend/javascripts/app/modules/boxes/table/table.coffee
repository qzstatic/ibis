tableTemplate = require('./table.html')
pasteTemplate = require('./pastehtml.html')

angular.module('boxes')
  .directive 'rmBoxTable', ->
    restrict: 'E'
    replace: true
    templateUrl: tableTemplate
    scope:
      box: '='
    controller: ($scope, $element, $http, Snipe, Notify, $uibModal, $timeout, Settings) ->

      $scope.status = {}

      $scope.$parent.checkPublishedBox $scope

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      formatCells = []
      preventAlignCell = false

      if !$scope.box.json
        $scope.box.json =
          columns: [
            { sorting: 'нет', highlight: false },
            { sorting: 'нет', highlight: false },
            { sorting: 'нет', highlight: false } ]
          rows: [
            { title: false, expand: false, highlightColor: false, highlightBold: false },
            { title: false, expand: false, highlightColor: false, highlightBold: false },
            { title: false, expand: false, highlightColor: false, highlightBold: false }
          ]
      else
        $scope.box.json = JSON.parse $scope.box.json

      # Add button menu
      addButtonMenuEvent = (button, menu) ->
        Handsontable.Dom.addEvent button, 'click', (event) ->
          changeTypeMenu = undefined
          position = undefined
          removeMenu = undefined
          document.body.appendChild menu
          event.preventDefault()
          event.stopImmediatePropagation()
          changeTypeMenu = document.querySelectorAll('.changeTypeMenu')
          i = 0
          len = changeTypeMenu.length
          while i < len
            changeTypeMenu[i].style.display = 'none'
            i++
          menu.style.display = 'block'
          position = button.getBoundingClientRect()
          menu.style.top = position.top + (window.scrollY or window.pageYOffset) + 2 + 'px'
          menu.style.left = position.left + 'px'
          removeMenu = (event) ->
            if event.target.nodeName == 'LI' and event.target.parentNode.className.indexOf('changeTypeMenu') != -1
              if menu.parentNode then menu.parentNode.removeChild menu
            return
          Handsontable.Dom.removeEvent document, 'click', removeMenu
          Handsontable.Dom.addEvent document, 'click', removeMenu
          return
        return

      # Create menu
      buildMenu = (activeCellType) ->
        menu = document.createElement('UL')
        sorting = ['да','нет','своя']
        item = undefined
        menu.className = 'changeTypeMenu'
        i = 0
        len = sorting.length
        while i < len
          item = document.createElement('LI')
          if 'innerText' of item
            item.innerText = sorting[i]
          else
            item.textContent = sorting[i]
          item.data = 'colSorting': sorting[i]
          if activeCellType == sorting[i]
            item.className = 'active'
          menu.appendChild item
          i++
        menu

      buildRowCheckboxes = (index, TH) ->
        $(TH).find('input[data-type="title"]')[0]?.checked = $scope.box.json.rows[index].title
        $(TH).find('input[data-type="expand"]')[0]?.checked = $scope.box.json.rows[index].expand
        $(TH).find('input[data-type="highlightColor"]')[0]?.checked = $scope.box.json.rows[index].highlightColor
        $(TH).find('input[data-type="highlightBold"]')[0]?.checked = $scope.box.json.rows[index].highlightBold

      buildColCheckboxes = (index, TH) ->
        $(TH).find('input[data-type="highlight"]')[0].checked = $scope.box.json.columns[index].highlight

      # Create button
      buildButton = ->
        button = document.createElement('BUTTON')
        button.innerHTML = '▼'
        button.className = 'changeType'
        button

      # Set column sorting
      setColumnSorting = (i, sorting, instance) ->
        if $scope.box.json.columns[i]
          $scope.box.json.columns[i].sorting = sorting
        instance.render()
        return

      # Create format cells
      createFormatCells = ->
        _.times $scope.hot.countRows(), (indexRow) ->
          unless formatCells[indexRow]
            formatCells[indexRow] = []
          _.times $scope.hot.countCols(), (indexCol) ->
            unless formatCells[indexRow][indexCol]
              formatCells[indexRow][indexCol] = undefined

      $scope.hot = new Handsontable $($element).find('.b-box-table').get(0),
        data: [ ['','',''], ['','',''], ['','',''] ]
        autoRowSize: false
        colHeaders: (index) ->
          if index == ($scope.box.json.columns.length - 1)
            'Контент для расхлопа'
          else
            '<label><input type="checkbox" data-type="highlight"> Выделить</label><br>Сортировка'
        cells: (row, col, prop) ->
          if $scope.box.json.renderData && $scope.box.json.renderData[row]?[col]?.align? && !preventAlignCell
            @className = $scope.box.json.renderData[row][col].align
          @renderer = Handsontable.renderers.HtmlRenderer
        rowHeaders: (index) ->
          header = ''
          if index == 0
            header = '<label><input type="checkbox" data-type="title"> Заголовок</label><br>'
          else if index == 1
            header = '<label><input type="checkbox" data-type="title"> Подзаголовок</label><br>'
          header + '''<label><input type="checkbox" data-type="expand"> Расхлоп</label><br>
          <label><input type="checkbox" data-type="highlightColor"> Выделить цветом</label><br>
          <label><input type="checkbox" data-type="highlightBold"> Выделить жирным</label>'''
        afterGetRowHeader: (row, TH) ->
          buildRowCheckboxes(row, TH)
          Handsontable.Dom.addEvent TH, 'click', (event) =>
            target = event.target
            if target.nodeName == 'INPUT'
              if $scope.box.json.rows[row]
                $scope.box.json.rows[row][target.dataset.type] = target.checked
        afterGetColHeader: (col, TH) ->
          if col == -1 || col >= ($scope.box.json.columns.length - 1) then return
          if $scope.box.json.columns[col]
            menu = buildMenu($scope.box.json.columns[col].sorting)
          else
            menu = buildMenu('нет')
          button = buildButton()
          addButtonMenuEvent button, menu
          Handsontable.Dom.addEvent menu, 'click', (event) =>
            if event.target.nodeName == 'LI'
              setColumnSorting(col, event.target.data['colSorting'], @)
              if event.target.data['colSorting'] in ['да', 'своя']
                $(TH).addClass('sorted')
              else
                $(TH).removeClass('sorted')
            return
          if TH.firstChild.lastChild.nodeName == 'BUTTON'
            TH.firstChild.removeChild TH.firstChild.lastChild
          TH.firstChild.appendChild button
          if $scope.box.json.columns[col].sorting in ['да', 'своя'] then $(TH).addClass('sorted')
          buildColCheckboxes(col, TH)
          Handsontable.Dom.addEvent TH, 'click', (event) =>
            target = event.target
            if target.nodeName == 'INPUT'
              if $scope.box.json.columns[col]
                $scope.box.json.columns[col][target.dataset.type] = target.checked
        afterCreateCol: (index) ->
          $scope.box.json.columns.splice(index, 0, { sorting: 'нет', highlight: false })
          createFormatCells()
        afterRemoveCol: (index) ->
          $scope.box.json.columns.splice(index, 1)
          createFormatCells()
        afterCreateRow: (index) ->
          $scope.box.json.rows.splice(index, 0, { title: false, expand: false, highlightColor: false, highlightBold: false })
          createFormatCells()
        afterRemoveRow: (index) ->
          $scope.box.json.rows.splice(index, 1)
          createFormatCells()
        beforeCellAlignment: -> preventAlignCell = true
        contextMenu: true
        renderAllRows: true
        copyPaste: true
        manualColumnResize: true
        manualRowResize: true
        rowHeaderWidth: 150
        colWidths: 150
        mergeCells: if $scope.box.json.mergeCells? then $scope.box.json.mergeCells else true
        contextMenu:
          callback: (key, options) ->
            hot = @
            selectedCell = hot.getSelected()
            if selectedCell?
              html = hot.getDataAtCell selectedCell[0], selectedCell[1]
            if key == 'pasteHTML'
              modalInstance = $uibModal.open
                templateUrl: pasteTemplate
                size: 'lg'
                controller: ($scope, $uibModalInstance) ->
                  $scope.html = html
                  $scope.insertHTML = ->
                    hot.setDataAtCell selectedCell[0], selectedCell[1], html
                    $uibModalInstance.close('ok')
                  $scope.close = -> $uibModalInstance.dismiss('close')
              modalInstance.rendered.then () ->
                $('#pasteHTML').redactor
                  imageUpload: Settings.agami
                  lang: 'ru'
                  linebreaks: true
                  pastePlainText: true
                  buttons: ['html', 'bold', 'italic', 'link', 'orderedlist', 'unorderedlist', 'image']
                  linkTooltip: false
                  initCallback: -> @$editor.focus()
                  changeCallback: -> html = @code.get()
                  imageUploadCallback: (image, json) ->
                    $(image).attr('src', Settings.attachments_host + json.versions.original.url)
            else if key == 'setFormat'
              $scope.box.json.columns[selectedCell[1]].sorting = 'своя'
              currentFormat = formatCells[selectedCell[0]][selectedCell[1]]?.format
              format = prompt 'Укажите значение, по которому будет происходить сортировка этой ячейки', currentFormat
              if format
                setColumnSorting selectedCell[1], 'своя', hot
                formatCells[selectedCell[0]][selectedCell[1]] =
                  format: format
                  value: html
          items:
            'row_above':
              name: 'Вставить строку выше'
            'row_below':
              name: 'Вставить строку ниже'
            'hsep1':
              name: '---------'
            'col_left':
              name: 'Вставить колонку слева',
            'col_right':
              name: 'Вставить колонку справа',
            'hsep2':
              name: '---------'
            'remove_row':
              name: 'Удалить строку'
              disabled: -> @.getSelected()[0] == 0
            'remove_col':
              name: 'Удалить колонку'
              disabled: -> @.getSelected()[0] == 0
            'alignment':
              name: 'Выравнивание',
            'hsep3':
              name: '---------'
            'pasteHTML':
              name: 'Редактировать HTML',
            'mergeCells':
              name: 'Объединить / Разбить',
            'setFormat':
              name: 'Значение сортировки'

      if $scope.box.json.settings? then $scope.hot.updateSettings $scope.box.json.settings
      if $scope.box.json.data? then $scope.hot.loadData $scope.box.json.data

      if $scope.box.json.formatCells?
        formatCells = $scope.box.json.formatCells
      else
        createFormatCells()

      $scope.saveTable = () ->
        data = $scope.hot.getData()
        renderData = []
        index = []

        _.map data, (row, rowIndex) ->
          _.map row, (col, colIndex) ->
            cell = $scope.hot.getCell(rowIndex, colIndex)
            if index[rowIndex] && index[rowIndex][colIndex] then return
            if !renderData[rowIndex] then renderData[rowIndex] = []
            renderData[rowIndex][colIndex] =
              rowspan: cell.getAttribute('rowspan') || '1'
              colspan: cell.getAttribute('colspan') || '1'
              value: cell.innerHTML
              align: $scope.hot.getCellMeta(rowIndex, colIndex).className
              colIndex: colIndex
            _.times cell.getAttribute('rowspan') || 1, (i) ->
               _.times cell.getAttribute('colspan') || 1, (n) ->
                  if !index[rowIndex + i] then index[rowIndex + i] = []
                  index[rowIndex + i][colIndex + n] = true

        # Данные для рендера таблицы в админке
        $scope.box.json.settings = $scope.hot.getSettings()
        $scope.box.json.data = data

        # Данные для рендера таблицы на сайте
        $scope.box.json.renderData = renderData
        $scope.box.json.formatCells = formatCells
        $scope.box.json.mergeCells = $scope.hot.mergeCells.mergedCellInfoCollection

        $scope.box.json = JSON.stringify $scope.box.json

        $scope.$parent.update($scope.box, true, $scope.status).then (box) ->
          $scope.box.json = JSON.parse $scope.box.json

      $scope.removeTable = -> $scope.$parent.remove(null, $scope.box)
