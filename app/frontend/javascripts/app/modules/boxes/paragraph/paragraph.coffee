paragraphTemplate = require('./paragraph.html.slim')

angular.module('boxes')
  .directive 'rmBoxParagraph', ->
    restrict: 'E'
    replace: true
    templateUrl: paragraphTemplate
    scope:
      box: '='
    controller: ($scope, $element, $http, Snipe, Notify, $rootScope, current_document, Chatter, Locker, Settings, $timeout, $q, Atasus, EditorsService, Editor, CompaniesService, Utils) ->

      # Мигание параграфа
      blinkParagraph = (i = 0) ->
        $element.toggleClass('animate-blink')
        i += 1
        $timeout(blinkParagraph, 500, true, i) if i < 6

      $scope.Locker = new Locker
        scope: $scope
        element: $element
        id: current_document.id
        part: "box-#{$scope.box.id}"
        reload: ->
          Snipe.one('boxes', $scope.box.id)
            .get()
            .then (box) ->
              box.position = $scope.box.position
              if box.body != $scope.box.body then blinkParagraph()
              $scope.box   = box

      $targetBox = $($element).find('.body')

      $scope.status =
        isBoxChanged: false

      isRedactor = false
      forceBox = null
      lengthBoxNodes = 0
      backupHtml = null
      $scope.showForceMessage = false

      checkIfCuckooAccepted = ->
        linksForChecking = ['companies']
        links = current_document.settings.links
        links? && _.every linksForChecking, (link) -> link in Object.keys(links)

      # Получаем index текущего параграфа
      getCurrentBoxIndex = ->
        _.findIndex $scope.$parent.root_box.children, ( (box) => box.id == $scope.box.id )

      # Параметры при сохранении параграфа
      params =
        method: 'POST'
        url: "#{Settings.cuckoo}/markup",
        data:
          text: ''
          exclude: []
        withCredentials: false

      $scope.$parent.checkPublishedBox $scope

      # Получаем index текущего параграфа
      getCurrentBoxIndex = ->
        _.findIndex $scope.$parent.root_box.children, ( (box) => box.id == $scope.box.id )

      # Обновление ссылок привязанных компаний
      $rootScope.$on 'document.linksCompanyUpdated', ($event, data) ->
        params.data.exclude = _.uniq(_.map(data.links, 'bound_document.id'))

      # Изменение статуса бокса
      $scope.$on 'box.updateBoxStatus', () ->
        $scope.$parent.checkPublishedBox $scope

      # Публикация документа
      $scope.$on 'document.publish', ->
        $scope.$parent.checkPublishedBox $scope, true

      # Установка фокуса для параграфа по его идентификатору
      $scope.$on 'editor.focus_to', (e, message) ->
        if message.boxId == $scope.box.id && !$scope.locked
          if $rootScope.appendedParagraph
            lengthBoxNodes = $($element).find('.body')[0].childNodes.length
            $scope.box.body = "#{$scope.box.body}<i></i> #{$rootScope.appendedParagraph}"
          if message.caretPosition == 'start'
            $scope.createRedactor 'start'
          else if message.caretPosition == 'end'
            $scope.createRedactor 'end'
          else
            $scope.createRedactor()

      $scope.$watch 'focused', (state) ->
        $($element).prevAll('.glyphicon').toggle(!state)

      # Обновление параграфа
      updateBox = (html) ->
        defer = $q.defer()
        if $scope.status.isBoxChanged && html != ''
          params.data.text = html
          params.data.exclude = CompaniesService.companyIds
          $scope.$parent.update($scope.box, true, $scope.status)
            .then updateCuckoo, forceMessageHandler
            .then -> $timeout (-> $scope.status.isBoxChanged = false), true
            .then ->
              $scope.Locker.unlock(true)
              # Atasus.add 'Параграф обновлен.', $scope.box
              lengthBoxNodes = 0
              defer.resolve()
        else
          $scope.Locker.unlock(true)
        defer.promise

      # Обновления cuckoo
      updateCuckoo = () ->
        defer = $q.defer()
        if checkIfCuckooAccepted()
          $http(params)
          .then (response) ->
            $scope.box.body = response.data.text
            if response.data.document_ids.length
              CompaniesService.update response.data.document_ids
              params.data.exclude = response.data.document_ids
            if response.data.text != params.data.text
              $scope.$parent.update($scope.box, true, $scope.status)
            defer.resolve()
          , () -> defer.resolve()
        else
          defer.resolve()
        defer.promise


      updateMultipleCuckoo = (box) ->
        defer = $q.defer()
        if checkIfCuckooAccepted()
          query = angular.copy params
          query.data.text = box.body
          $http(query)
          .then (response) ->
            if response.data.document_ids.length
              CompaniesService.update response.data.document_ids
              params.data.exclude = _.concat params.data.exclude, response.data.document_ids
            defer.resolve(response.data.text)
        else
          defer.resolve(box.body)
        defer.promise

      # Показываем сообщение о перезаписи параграфа
      forceMessageHandler = (response) ->
        if response.status == 412
          forceBox = response.data.box
          $scope.showForceMessage = true
          $scope.snapshot = response.data.snapshot

      # Удаление параграфа
      removeBox = (box = $scope.box) -> $scope.$parent.remove(null, box)

      # Создаем новые параграфы для сохранения
      createMultiParagraphs = (paragraphs, index) ->
        defer = $q.defer()
        newParagraphs = []
        index = getCurrentBoxIndex()
        $scope.$parent.reorder($scope.$parent.root_box.children).then =>
          if index < 0 then index = 0
          position = $scope.$parent.root_box.children[index].position
          angular.forEach paragraphs, (htmlText, index) ->
            if !index then return
            newParagraphs.push
              parent_id: $scope.$parent.root_box.id,
              position:  position + index * 10
              type:      'paragraph',
              body:      _.trim htmlText
          i = 0
          recursiveUpdate = ->
            updateMultipleCuckoo(newParagraphs[i]).then (text) ->
              newParagraphs[i].body = text
              i++
              if newParagraphs.length == i
                defer.resolve newParagraphs
              else
                recursiveUpdate()
          recursiveUpdate()
        , =>
          $scope.box.body = backupHtml
          Notify.add msg: 'Не удалось отсортировать параграфы. Попробуйте еще раз.', type: 'error'
          defer.reject
        defer.promise

      # Сохранение нескольких параграфов
      saveMultiParagraphs = (newParagraphs, redactor) ->
        Snipe
          .all('boxes/multi')
          .post(attributes: newParagraphs)
          .then (boxes) =>
            index = getCurrentBoxIndex()
            for box in boxes
              if box.id?
                box.index = ++index
                Chatter.create_part(current_document.id, box)
                $scope.$parent.root_box.children.splice(index, 0, box)
            $timeout (-> $scope.status.isBoxChanged = false), 100
            redactor.code.set $scope.box.body
            $scope.$emit 'paragraph.multparagraphsAdded'
          , () =>
            $scope.box.body = backupHtml
            Notify.add msg: 'Не удалось сохранить параграфы. Попробуйте еще раз.', type: 'error'

      # Redactor Callbacks
      onBlurCallback = (e) ->
        if Offline.state == 'down' then return
        # Atasus.add e.type, $scope.box
        @code.sync()
        $scope.focused = false
        html = @code.get().replace(/<i><\/i>/g, '')
        backupHtml = html = Utils.replaceQuotes(html)
        htmlSplitted = html.split('<br>')
        paragraphs = _.without htmlSplitted, '', ' ', /\n/, /\r\n/
        firstParagraph = _.trim _.first paragraphs
        $scope.box.body = firstParagraph
        $('body').prop 'spellcheck', false
        $targetBox.redactor('core.destroy')
        $timeout (=> $scope.status.isBoxChanged = false), 100
        if _.trim(htmlSplitted.join('')) != ''
          if paragraphs.length > 1
            updateBox(firstParagraph).then =>
              $scope.$parent._loading.addPromise createMultiParagraphs(paragraphs).then (newParagraphs) =>
                saveMultiParagraphs(newParagraphs, @)
          else
            updateBox(html)
        else
          Chatter.unblock_part($scope.id, $scope.part)
          removeBox()

      onInitCallback = (caretPosition) ->
        isRedactor = true
        $scope.focused = true
        $rootScope.currentParagraphIndex = getCurrentBoxIndex()
        $rootScope.currentParagraphId = $scope.box.id
        @code.set $scope.box.body
        $('body').prop 'spellcheck', true
        $timeout =>
          if caretPosition == 'start' && @$editor.html()
            @caret.setStart @$editor[0]
          else if caretPosition == 'end' && @$editor.html()
            @caret.setEnd @$editor[0]
          else if lengthBoxNodes > 0 && @$editor.html()
            @caret.setAfter @$editor[0].childNodes[lengthBoxNodes]
            $scope.status.isBoxChanged = true
          @$editor.focus()
        , 1

      onKeydownCallback = (e) ->
        boxes = $scope.$parent.root_box.children
        index = $rootScope.currentParagraphIndex
        code = @code.get()
        plainText = @clean.stripTags(code)
        isEmptyBox = '' == _.trim code.split('<br>').join('')
        isPrevBoxParagraph = index > 0 && boxes[index - 1]? && boxes[index - 1].type == 'paragraph'
        isNextBoxParagraph = index >= 0 && boxes[index + 1]? && boxes[index + 1].type == 'paragraph'
        isPrevBoxNotLocked = isPrevBoxParagraph && !$("#external-#{boxes[index - 1].id}").parent().hasClass('locked-item')
        isNextBoxNotLocked = isNextBoxParagraph && !$("#external-#{boxes[index + 1].id}").parent().hasClass('locked-item')
        $rootScope.appendedParagraph = ''

        # Не пустые параграфы
        if !isEmptyBox && !@selection.getText()
          if @caret.getOffset() == 0 && isPrevBoxNotLocked
            if e.keyCode == 8
              $rootScope.appendedParagraph = code
              $scope.box.body = ''
              @code.set ''
              $timeout (=> $rootScope.$broadcast 'editor.focus_to', boxId: boxes[index - 1].id), 10
            if e.keyCode == 38
              $timeout (=> $rootScope.$broadcast 'editor.focus_to', {boxId: boxes[index - 1].id, caretPosition: 'end'}), 10
          if plainText.length <= @caret.getOffset() && isNextBoxNotLocked
            if e.keyCode == 46
              e.preventDefault()
              lengthBoxNodes = @$editor[0].childNodes.length
              @code.set "#{code} <i></i>  #{boxes[index + 1].body}"
              if @$editor[0].childNodes[lengthBoxNodes - 1]
                @caret.setBefore @$editor[0].childNodes[lengthBoxNodes - 1]
              else if @$editor[0].childNodes[lengthBoxNodes - 2]
                @caret.setBefore @$editor[0].childNodes[lengthBoxNodes - 2]
              removeBox boxes[index + 1]
            if e.keyCode == 40
              $timeout (=> $rootScope.$broadcast 'editor.focus_to', {boxId: boxes[index + 1].id, caretPosition: 'start'}), 10

        # Пустые параграфы
        if isEmptyBox && e.keyCode in [8, 46]
          if e.keyCode == 8 && boxes[index - 1]? && isPrevBoxNotLocked
            $timeout =>
              $rootScope.$broadcast 'editor.focus_to',
                boxId: boxes[index - 1].id,
                caretPosition: 'end'
            , 10
          if e.keyCode == 46 && boxes[index + 1]? && isNextBoxNotLocked
            $rootScope.$broadcast 'editor.focus_to',
              boxId: boxes[index + 1].id,
              caretPosition: 'start'

      onClickCallback = (e) ->
        $link = $(e.target)
        entry = $link.attr('entry')
        if entry
          if /-(\d+)-/g.test entry
            $link.attr 'entry', entry.replace /-(\d+)-/g, "$1"
          else
            $link.attr 'entry', entry.replace /(\d+)/g, "-\$1-"
          $scope.status.isBoxChanged = true
          @code.sync()
          return
        else if $link.attr('href')
          window.open $link.attr('href'), '_blank'


      # Создание редактора
      $scope.createRedactor = (caretPosition = null) ->
        $targetBox = $($element).find('.body')
        if isRedactor || Offline.state == 'down' then return
        $targetBox.redactor
          toolbarExternal: "#external-#{$scope.box.id}"
          plugins: ['formatting', 'alignment']
          lang: 'ru'
          focus: false
          linebreaks: true
          pastePlainText: true
          buttons: ['html', 'bold', 'italic', 'link']
          linkTooltip: false

          initCallback: -> onInitCallback.call @, caretPosition

          destroyCallback: ->
            isRedactor = false
            $($element).find('.external-toolbar').empty()

          changeCallback: (html) ->
            $scope.status.isBoxChanged = true

          blurCallback: onBlurCallback

          keydownCallback: onKeydownCallback

          clickCallback: onClickCallback

          pasteCallback: (html) ->
            Utils.replaceQuotes(html)

        null

      # Принудительное обновление параграфа
      $scope.updateForce = ($event) ->
        # Atasus.add $event.type, $scope.box
        $scope.box.updated_at = forceBox.updated_at
        $scope.status.isBoxChanged = true
        updateBox $scope.box.body
        $scope.showForceMessage = false

      # Отменение своих изменений
      $scope.updateRevert = ($event) ->
        # Atasus.add $event.type, $scope.box
        $scope.box = angular.copy forceBox
        $scope.showForceMessage = false
