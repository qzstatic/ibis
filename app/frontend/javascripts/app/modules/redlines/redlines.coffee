listTemplate = require('./list.html')

angular.module('redlines', [])

  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.redlines',
        abstract: true,
        url: '/redlines'
        data: 
          permissions: 
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'
      .state 'logged.redlines.list',
        url: '/list'
        auth: true
        resolve:
          redlines: (Aquarium, $stateParams) -> Aquarium.all('redlines').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'RedlinesListController'

  .controller 'RedlinesListController', ($scope, $state, Aquarium, Editor, redlines) ->
    
    label = 'СРОЧНО'
    $scope.redlines = _.filter redlines, (redline) -> redline if redline.status == 'on' || redline.status == 1
    
    $scope.addRedline = (redline) ->
      redline.status = 1
      redline.label = label
      Aquarium.all('redlines').post(attributes: redline).then (redline) ->
        $scope.redlines.push redline
        
    $scope.updateRedline = (redline) ->
      redline.status = 1
      redline.label = label
      Aquarium.one('redlines', redline.id).patch(redline).then (savedRedline) ->
        redline = savedRedline
        
    $scope.removeRedline = (redline) ->
      redline.status = 0
      Aquarium.one('redlines', redline.id).patch(redline).then (savedRedline) ->
        $scope.redlines.length = 0
