editTemplate = require('./edit.html')
branchTemplate = require('./branch.html')

angular.module('categories', [])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.categories',
        abstract: true,
        url: '/categories'
        data:
          permissions:
            except: ['SUBSCRIPTION', 'COMMERS']
            redirectTo: 'errors.403'

      .state 'logged.categories.edit',
        url: '/edit'
        auth: true
        resolve:
          categoriesTree: (Snipe) -> Snipe.one('categories/tree').get()
          categoriesPlain: (Snipe) -> Snipe.all('categories').getList()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'CategoriesEditController'

  .controller 'CategoriesEditController', ($scope, categoriesTree, Snipe, categoriesPlain) ->
    $scope.categories = categoriesTree
    $scope.parent_id = null
    $scope.showForbiddenCombinations = false
    $scope.branchTemplate = branchTemplate

    $scope.edit = (category) ->
      $scope.current_category.edit = false if $scope.current_category
      category.edit = true
      $scope.current_category = category
      forbiddenTitles = []
      if category.forbidden_combinations
        _.each category.forbidden_combinations, (id) ->
          forbiddenTitles.push _.result(_.find(categoriesPlain, { id: id }), 'title')
        $scope.forbiddenTitles = forbiddenTitles

    $scope.reset = ->
      $scope.current_category.edit = false if $scope.current_category?
      $scope.parent_category.edit = false if $scope.parent_category?

      $scope.parent_category = null
      $scope.current_category = {
        edit: false,
        parent_id: null,
        children: [],
        enabled: true,
        nested_categories_as_string: '',
        setting_keys_as_string: ''
      }

    $scope.reset()

    prepare = (category) ->
      category.nested_categories = if category.nested_categories_as_string.length then _.map(category.nested_categories_as_string.split(','), (item) -> _.trim(item)) else []
      category.setting_keys = if category.setting_keys_as_string.length then _.map(category.setting_keys_as_string.split(','), (item) -> _.trim(item)) else []

      category

    $scope.update_or_create = (category) ->
      $scope.current_category.edit = false if $scope.current_category?
      $scope.parent_category.edit = false if $scope.parent_category?
      $scope.current_category = category
      category = prepare(category)

      if category.id
        Snipe.one('categories', category.id).patch(category).then (created_category) -> $scope.reset()
      else
        if null == $scope.current_category.parent_id
          delete($scope.current_category.parent_id)

        Snipe.all('categories', category.id).post(attributes: category).then (created_category) ->
          if $scope.current_category.parent_id
            $scope.parent_category.children.push created_category
          else
            $scope.categories.unshift created_category

          $scope.reset()

    $scope.set_parent = (category) ->
      $scope.parent_category = category
      $scope.current_category = {
        parent_id: category.id,
        children: [],
        enabled: true,
        nested_categories_as_string: '',
        setting_keys_as_string: ''
      }

    $scope.children = (category) ->
      category.children = [] unless category.children?
      category.nested_categories_as_string = category.nested_categories.join(', ')
      category.setting_keys_as_string = category.setting_keys.join(', ')
      category.children

    $scope.remove = (category) -> Snipe.one('categories', category.id).remove()
