titleTemplate = require('./title.html')

angular.module('blocks')
  .directive 'blockTitle', ->
    restrict: 'E'
    replace: true
    templateUrl: titleTemplate
    scope:
      block: '='
