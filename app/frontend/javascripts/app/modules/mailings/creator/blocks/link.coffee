linkTemplate = require('./link.html')

angular.module('blocks')
  .directive 'blockLink', ->
    restrict: 'E'
    replace: true
    templateUrl: linkTemplate
    scope:
      block: '='
