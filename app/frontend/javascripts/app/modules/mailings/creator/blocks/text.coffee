textTemplate = require('./text.html')

angular.module('blocks')
  .directive 'blockText', ->
    restrict: 'E'
    replace: true
    templateUrl: textTemplate
    scope:
      block: '='
    link: (scope, element, attrs) ->
      $(element).find('.block-text-body').redactor
        lang: 'ru'
        focus: true
        linebreaks: true
        pastePlainText: true
        buttons: ['bold', 'italic', 'deleted', 'unorderedlist', 'link']
        linkTooltip: false
        changeCallback: -> scope.block.text = @code.get()
        initCallback: ->
          if !scope.block.text then scope.block.text = ''
          @code.set scope.block.text
