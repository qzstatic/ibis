imageTemplate = require('./image.html')

angular.module('blocks')
  .directive 'blockImage', ->
    restrict: 'E'
    replace: true
    templateUrl: imageTemplate
    scope:
      block: '='
    link: (scope, element, attrs) ->
    controller: ($scope, Agami, Settings) ->
      $scope.attachments_host = Settings.attachments_host
      $scope.uploadImage = (files) ->
        Agami.upload(_.first(files)).then (data) ->
          $scope.block.image = data.versions.original
