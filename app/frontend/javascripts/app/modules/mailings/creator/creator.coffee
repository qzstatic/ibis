creatorTemplate = require('./creator.html')

angular.module('creator',['blocks'])
  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.creator',
        url: '/creator/:id'
        auth: true
        data: 
          permissions: 
            except: ['EDITOR']
            redirectTo: 'errors.403'
        resolve:
          mailTypes: (Aquarium) -> Aquarium.all('mailing/mail_types').getList()
          namespaces: (Aquarium) -> Aquarium.all('namespaces').getList()
          contactsCount: (Aquarium, $stateParams) ->
            if $stateParams.id
              Aquarium.one("mailings/#{$stateParams.id}/contacts_count").get()
            else
              return { count: 0 }
          mailing: (Aquarium, $stateParams) -> 
            if $stateParams.id
              Aquarium.one('mailings', $stateParams.id).get()
            else 
              return {}
        views:
          'content@logged':
            templateUrl: creatorTemplate
            controller: 'CreatorController'
            
  .controller 'CreatorController', ($scope, $state, Aquarium, Notify, mailTypes, namespaces, mailing, $stateParams, Search, contactsCount) ->
    $scope.mailTypes = mailTypes
    $scope.namespaces = namespaces
    $scope.mailing = mailing
    $scope.contactsCount = contactsCount
    
    # Params schema
    $scope.conditions = [
      {
        type: 'status',
        statuses: []
      },
      {
        type: 'mailing',
        mail_type_ids: []
      },
      {
        type: 'payment_namespace',
        namespace_ids: []
      }
      {
        type: 'register_date',
        start_date: null,
        end_date: null
      },
      {
        type: 'subscription_end',
        start_date: null,
        end_date: null
      }
    ]

    $scope.mailing.template_kit = [] if !$scope.mailing.template_kit?
    $scope.isHtmlActive = false
    
    $scope.setHtmlTemplate = (bool) -> $scope.isHtmlActive = bool
    
    $scope.addBlock = (type) -> $scope.mailing.template_kit.push { type: type }
    $scope.removeBlock = (removedBlock) -> _.remove $scope.mailing.template_kit, (block) -> block == removedBlock

    buildTemplate = (id) ->
      if mailing.template_kit.length && !$scope.isHtmlActive
        Aquarium
          .all("mailings/#{id}/build_template")
          .post(template_kit: mailing.template_kit)
          .then (data) ->
            $scope.mailing.template = data.mailing.template
            Notify.add type: 'success', msg: 'Черновик сохранен'

    $scope.saveDraft = ->
      if $scope.mailing.id
        Aquarium
          .all("mailings/#{$stateParams.id}")
          .patch($scope.mailing)
          .then (data) ->
            Notify.add type: 'success', msg: 'Рассылка сохранена'
            buildTemplate data.id
      else
        Aquarium
          .all("mailings")
          .post(attributes: $scope.mailing)
          .then (data) ->
            Notify.add type: 'success', msg: 'Рассылка создана'
            buildTemplate data.id
            $state.go 'logged.creator', id: data.id, location: true
                
    $scope.previewMailing = () ->
      Aquarium
        .one('mailings', $scope.mailing.id)
        .get()
        .then (mailing) ->
          mailingWindow = window.open("", "Рассылка #{$scope.mailing.id}", "width=1000, height=1000");
          mailingWindow.document.write(mailing.template)

    $scope.sendMailing = () ->
      Aquarium
        .all("mailings/#{$scope.mailing.id}/ready")
        .post(attributes: $scope.mailing)
        .then (data) ->
          $scope.mailing.status = data.status
          $scope.isSending = true
          Notify.add type: 'success', msg: 'Рассылка будет отправлена в установленное время'

    $scope.cancelSending = () ->
      Aquarium
        .all("mailings/#{$scope.mailing.id}/not_ready")
        .post(attributes: $scope.mailing)
        .then (data) ->
          $scope.mailing.status = data.status
          $scope.isSending = false
          Notify.add type: 'success', msg: 'Отправка рассылки была отменена'

    $scope.sendTestMailing = () ->
      emails = _.map $scope.emails.split(','), (str) -> _.trim str
      Aquarium
        .all("mailings/#{$scope.mailing.id}/deliver_test")
        .post(emails: emails)
        .then (data) ->
          Notify.add type: 'success', msg: 'Тестовая рассылка успешно отправлена'

    $scope.linkContacts = () ->
      query = angular.copy $scope.conditions
      
      if query[4].start_date? && query[4].end_date?
        query[4].start_date = moment(query[4].start_date).format('YYYY-MM-DD')
        query[4].end_date = moment(query[4].end_date).format('YYYY-MM-DD')
      else
        query.splice 4, 1
      
      if query[3].start_date? && query[3].end_date?
        query[3].start_date = moment(query[3].start_date).format('YYYY-MM-DD')
        query[3].end_date = moment(query[3].end_date).format('YYYY-MM-DD')
      else
        query.splice 3, 1
        
      Aquarium
        .all("mailings/#{$scope.mailing.id}/attach_contacts")
        .post(conditions: query)
        .then (data) -> 
          $scope.mailing.contacts_status = 'contacts_linking'
