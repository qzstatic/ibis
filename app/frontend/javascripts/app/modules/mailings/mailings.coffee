listTemplate = require('./list.html')
messagesTemplate = require('./messages.html')

angular.module('mailings', ['creator'])

  .config ($stateProvider) ->
    $stateProvider
      .state 'logged.mailings',
        abstract: true,
        url: '/mailings'
        data: 
          permissions: 
            except: ['EDITOR']
            redirectTo: 'errors.403'
      .state 'logged.mailings.list',
        url: '/list'
        auth: true
        resolve:
          mailings: (Aquarium) -> Aquarium.all('mailings').getList()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'MailingsListController'
      .state 'logged.mailings.messages',
        url: '/messages'
        auth: true
        views:
          'content@logged':
            templateUrl: messagesTemplate
            controller: 'MailingsMessagesController'
            
  .controller 'MailingsListController', ($scope, $state, $http, Aquarium, Editor, mailings) ->
    linksClicks = {}
    $scope.mailings = mailings
    
    drawClicks = (template) ->
      _.each linksClicks.clicks_by_path, (link) ->
        $(template)
          .find("a[href$='#{link.key}']")
          .prepend("<span class='badge'>#{link.doc_count}</span>")
      $(template)
        .find('head')
        .append('''
          <style>
            .badge {
              display: inline-block;
              min-width: 10px;
              padding: 3px 7px;
              font-size: 12px;
              font-weight: 700;
              line-height: 1;
              color: #fff;
              text-align: center;
              white-space: nowrap;
              vertical-align: middle;
              background-color: #777;
              border-radius: 10px;
              margin-right: 5px;
              margin-bottom: 5px;
            }
          </style>
        ''')
    
    $scope.previewMailing = (id) ->
      Aquarium
        .one("mailing/stats/#{id}/links_clicks")
        .get()
        .then (clicks) -> linksClicks = clicks
        .then ->
          Aquarium
            .one('mailings', id)
            .get()
            .then (mailing) ->
              mailingWindow = window.open("", "Рассылка #{id}", "width=1000, height=1000");
              mailingWindow.document.write(mailing.template)
              drawClicks(mailingWindow.document)

  .controller 'MailingsMessagesController', ($scope, Regulus, Aquarium) ->

    $scope.filter = 
      date_from: moment().subtract(1, 'month').format('YYYY-MM-DD')
      date_to: moment().format('YYYY-MM-DD')
    
    $scope.$watch 'filter', (newVal, oldVal) ->
      Regulus
        .all("letters/domains")
        .customGET('', {
          date_from: $scope.filter.date_from, 
          date_to: $scope.filter.date_to,
        })
        .then (res) -> $scope.domains = res
      Regulus
        .one('letters')
        .customGET('', {
          date_from: $scope.filter.date_from, 
          date_to: $scope.filter.date_to,
          domain: $scope.filter.domain
        })
        .then (res) ->
          $scope.messages = res
          Aquarium
            .all('mailing/rules/current')
            .getList()
            .then (res) ->
              $scope.rules = res
              _.map $scope.messages.found, (message) ->
                _.map $scope.rules, (rule) ->
                  if message.key == rule.code
                    message.status = rule.status
                    message.ruleId = rule.id
    , true
    
    $scope.saveRule = (message) ->
      if message.ruleId?
        Aquarium
          .all("mailing/rules/#{message.ruleId}")
          .patch({status: message.status})
      else
        Aquarium
          .all('mailing/rules')
          .post(attributes: {code: message.key, status: message.status})
        
        
