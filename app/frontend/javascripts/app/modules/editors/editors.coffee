listTemplate = require('./index.html')
editTemplate = require('./edit.html')
newTemplate = require('./new.html')

angular.module('editors', [])
  .config ($stateProvider) ->

    $stateProvider
      .state 'logged.editors',
        abstract: true,
        url: '/editors'
        data: 
          permissions: 
            only: ['ADMIN']
            redirectTo: 'errors.403'
            
      .state 'logged.editors.list',
        url: '/list'
        auth: true
        resolve:
          editors: (Snipe) -> Snipe.all('editors').getList()
          groups: (GroupsService) -> GroupsService.list()
        views:
          'content@logged':
            templateUrl: listTemplate
            controller: 'EditorsListController'
      
      .state 'logged.editors.edit',
        url: '/:id/edit'
        auth: true
        resolve:
          editor: (Snipe, $stateParams) -> Snipe.one('editors', $stateParams.id).get()
          groups: (GroupsService) -> GroupsService.list()
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'EditorsEditController'
            
      .state 'logged.editors.new',
        url: '/new'
        auth: true
        views:
          'content@logged':
            templateUrl: newTemplate
            controller: 'EditorsNewController'
  
  .controller 'EditorsListController', ($scope, editors, Notify, $filter, groups) -> 
    $scope.editors = $filter('orderBy')(editors, 'last_name')
    $scope.full_name = (editor) -> [editor.first_name, editor.last_name].join(' ')

    $scope.groups_by_ids = (group_ids) ->
      if group_ids
        titles = []
        titles.push(_.find(groups, id: id).title) for id in group_ids
        titles.join(', ')

    $scope.disable = (editor) ->
      editor.patch(enabled: false).then -> editor.enabled = false

    $scope.enable = (editor) ->
      editor.patch(enabled: true).then -> editor.enabled = true


      
  .controller 'EditorsEditController', ($scope, editor, groups, Notify) -> 
    for group in groups
      group.checked = group.id in editor.group_ids
    
    $scope.editor = editor
    $scope.groups = groups

    $scope.update = (editor) -> 
      $scope.feed = editor.patch().then -> Notify.add type: 'success', msg: 'Редактор успешно обновлен!'
  
    $scope.update_groups = (group) ->
      if group.checked
        # добавили группу
        $scope.editor.group_ids.push(group.id)
      else
        # убрали группу
        $scope.editor.group_ids = _.without($scope.editor.group_ids, group.id)
        
      editor.patch(group_ids: $scope.editor.group_ids)
  
  .controller 'EditorsNewController', ($scope, $state, Snipe, Notify) ->    
    $scope.create = (editor) -> 

      $scope.feed = Snipe.all('editors').post(attributes: editor).then ->
        $state.go('logged.editors.list')
