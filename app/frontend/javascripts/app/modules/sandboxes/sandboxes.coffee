indexTemplate = require('./index.html')
editTemplate = require('./edit.html.slim')

angular.module('sandboxes', ['ui'])
  .config ($stateProvider) ->

    $stateProvider
      .state 'logged.sandboxes',
        abstract: true
        url: '/sandboxes'
        data: 
          permissions: 
            except: ['SUBSCRIPTION']
            redirectTo: 'logged.payments.export'
      # Страница со списком сандбоксов, на ней мы должны определить
      # показываемый сайндбокс и перейти к нему
      .state 'logged.sandboxes.list',
        url: '/list'
        auth: true,
        resolve: 
          sandboxes: (SandboxesService) -> SandboxesService.list()
        views:
          'content@logged':
            controller:  'SandboxesListController'

      .state 'logged.sandboxes.show',
        url: '/list/:id'
        auth: true,
        resolve:
          sandboxes: (SandboxesService, $stateParams) -> SandboxesService.list()
          documents: ($stateParams, Snipe) -> Snipe.one('sandboxes', $stateParams.id).all('documents').getList()
          categories: (CategoriesService) -> CategoriesService.list()
        views:
          'content@logged':
            templateUrl: indexTemplate
            controller: 'SandboxesShowController'

      .state 'logged.sandboxes.edit',
        url: '/list/:id/edit'
        auth: true,
        resolve:
          sandbox: (Snipe, $stateParams) -> Snipe.one('sandboxes', $stateParams.id).get() 
        views:
          'content@logged':
            templateUrl: editTemplate
            controller: 'SandboxesEditController'

  .controller 'SandboxesEditController', ($scope, Snipe, Editor, Notify, $state, sandbox, SandboxesService, $localStorage) ->
    if sandbox.base_date
      sandbox.base_date = moment(sandbox.base_date).format('YYYY-MM-DD')
    else
      sandbox.disable_base_date = true
   
    if sandbox.base_date? && sandbox.date_offset?
      sandbox.offset_date = moment(sandbox.base_date).add(sandbox.date_offset, 'seconds').format('YYYY-MM-DD')

    $scope.sandbox = sandbox
    
    to_sandbox = (sandbox) -> $state.go('logged.sandboxes.show', id: sandbox.id)

    $scope.toggle_published = (sandbox) ->
      sandbox.published_status = !sandbox.published_status
      sandbox.published = null unless sandbox.published_status
        
    $scope.update_sandbox = (sandbox) ->
      sandbox.patch().then ->
        SandboxesService.reload()
        to_sandbox(sandbox)
        Notify.add type: 'success', msg: 'Список изменен.'
          
    $scope.delete_sandbox = (sandbox) ->
      sandbox.remove().then ->
        delete $localStorage.active_sandbox_id
        SandboxesService.reload().then () ->
          $state.go('logged.sandboxes.list')

    $scope.set_offset = (sandbox, base, offset) -> 
      sandbox.date_offset = moment(sandbox.offset_date).unix() - moment(sandbox.base_date).unix()
      
    $scope.toggle_base_date = (sandbox) ->
      if sandbox.disable_base_date
        sandbox.base_date = null
        

  # Выбор сандбокса для перехода — заглавная страница
  # Потенциальная проблема: ситуация, когда ни одного сандбокса нет 
  .controller 'SandboxesListController', ($scope, $state, $localStorage, sandboxes) ->
    # Если в $localStorage записан номер листа, пытаемся перейти к нему
    if $localStorage.active_sandbox_id?
      $state.go('logged.sandboxes.show', id: $localStorage.active_sandbox_id)
    else
      first_sandbox = _.first(sandboxes)
      $state.go('logged.sandboxes.show', id: first_sandbox.id)
      

  # Страница конкретного сандбокса
  .controller 'SandboxesShowController', ($scope, sandboxes, documents, Snipe, SandboxesService, $state, Notify, $localStorage, $stateParams, EditorsService, CategoriesService) ->
    current_sandbox_id = +$stateParams.id

    for sandbox in sandboxes
      sandbox.active = sandbox.id == current_sandbox_id
      $scope.current_sandbox = sandbox if sandbox.active

    $localStorage.active_sandbox_id = current_sandbox_id

    $scope.sandboxes = sandboxes
    $scope.documents = documents
    $scope.sandbox = SandboxesService.by_id(current_sandbox_id)
    
    $scope.orderParams = 
      'Создано': 'created_at',
      'Опубликовано': 'published_at'
      # 'Обновлено': 'updated_at'

    $scope.directionParams =
      'По возрастанию': 'asc',
      'По убыванию': 'desc'

    $scope.sortDocuments =  ->
      Snipe
        .one('sandboxes', $stateParams.id)
        .customGET("documents", {
            sort_column: $scope.column
            sort_direction: $scope.direction
        })
        .then (documents) -> $scope.documents = documents

    $scope.document_kind = (kind) ->
      kinds =
        article: 'статья'
        news: 'новость'
        gallery: 'галерея'
        character: 'интервью'
        column: 'колонка'
        blog: 'блог'
        quote: 'цитата'
        video: 'видео'

      kinds[kind] || kind
    $scope.editor     = (id)  -> EditorsService.full_name_by_id(id) 
    $scope.categories = (ids) -> CategoriesService.slugs_by_ids(ids)

    # Создание нового пустого сандбокса
    $scope.new_sandbox = ->
      Snipe.all('sandboxes').post(attributes: { title: 'Новый список' }).then (created_sandbox)->
        SandboxesService.reload().then ->
          $state.go('logged.sandboxes.show', id: created_sandbox.id)
          Notify.add type: 'success', msg: 'Создан новый список.'
