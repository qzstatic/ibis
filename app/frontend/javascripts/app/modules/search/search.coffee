searchTemplate = require('./index.html')

angular.module('search', [])
  .config ($stateProvider) ->
    $stateProvider

      .state 'logged.search',
        auth: true,
        url: '/search/:page?q?sort'
        data: 
          permissions: 
            except: ['SUBSCRIPTION']
            redirectTo: 'errors.403'
        resolve:
          results: ($stateParams, Search) ->
            if $stateParams.q?
              Search.documents_with_params($stateParams)
            else
              []
        views:
          'content@logged':
            templateUrl: searchTemplate
            controller:  'SearchController'

  .controller 'SearchController', ($scope, $stateParams, Settings, $sce, $state, Search, results) ->
    $scope.found = results.found
    $scope.stats = results.stat
    
    $scope.filter = 
      sort: $stateParams.sort || 'date'
      q: $stateParams.q || null
      page: $stateParams.page || 1

    $scope.set_sort = (order) -> 
      $scope.filter.sort = order
      $scope.go()

    $scope.go = ->  $state.go('logged.search', $scope.filter)

    $scope.title = (record) ->
      title = if record.highlight?.title? && angular.isArray(record.highlight.title)
        _.first(record.highlight.title) 
      else
        record.title
      $sce.trustAsHtml(title)
