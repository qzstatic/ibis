angular.module 'models', []
angular.module 'utilities', []
angular.module 'settings', []
angular.module 'services', []

angular.module('app', [
  'ui.router',
  'restangular',
  'angular-loading-bar',
  'ajoslin.promise-tracker',
  'ngAnimate', 
  'ngCookies', 
  'models',
  'services',
  'auth',
  'settings',
  'utilities'
])

angular.module('app').factory 'Snipe', (Restangular, Settings) ->
  Restangular.withConfig (RestangularConfigurer) ->
    RestangularConfigurer.setBaseUrl Settings.snipe_api_url
