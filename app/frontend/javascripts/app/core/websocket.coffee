angular.module('app')
  .factory 'Chatter', ($websocket, Editor, Settings, $rootScope, Notify, current_document, $timeout, $interval) ->
    uuid          = null
    delay         = 0
    timer         = null
    stream        = $websocket("#{Settings.chatter}/#{Editor.access_token()}")
    last_document = null


    bindCallbacks = ->

      stream.onClose (message) ->
        console.info 'Link to chatter DOWN by disconnect!'
        Notify.add type: 'info', msg: 'Другие пользователи админки сейчас Вас не видят. Но можно продолжать работать, все изменения сохранятся.'
        $timeout ->
          console.info 'Try establish connection to chatter!'
          stream.reconnect()
        , 1000

      stream.onError (error) ->
        console.info 'Link to chatter DOWN with errors!', error
        stream.close()

      stream.onOpen ->
        $interval.cancel(timer) if timer?
        timer = $interval ->
          send({ type: 'ping' })
        , 5000

      stream.onMessage (message) ->
        message = JSON.parse(message.data)

        if 'channel_editors' == message.type
          $rootScope.active_editors = _.uniq(message.editor_ids)

        if 'uuid' == message.type
          if uuid? && uuid != message.uuid
            send({
              type: 'set_uuid',
              attributes: {
                uuid: uuid
              }
            })
            if last_document?
              $timeout ->
                send(last_document)
              , 1000
          else
            uuid = message.uuid

        if 'blocked_items' == message.type
          if _.size(message.items) > 0
            angular.forEach message.items, (item) ->
              $rootScope.$broadcast 'part:block', { part: item.item, client: item } if uuid != item.connection_id

        if 'block_item' == message.type
          $rootScope.$broadcast 'part:block', { part: message.item, client: message } if uuid != message.connection_id

        if 'unblock_item' == message.type
          $rootScope.$broadcast 'part:unblock', { part: message.item, client: message }
          $rootScope.$broadcast 'update:categories' if 'categories' == message.item
          current_document.reload()
          # Notify.add type: 'success', msg: "#{message.item} разблокирован." if uuid != message.connection_id

        if 'channel_talk' == message.type && uuid != message.connection_id
          box = JSON.parse message.text

        if 'box_update' == message.operation && uuid != message.connection_id
          box = JSON.parse message.box
          if box.type == 'gallery_image'
            $rootScope.$broadcast 'part:galleryUpdated', box
          else
            $rootScope.$broadcast 'part:update', box
          Notify.add type: 'success', msg: "Бокс #{box.id} был обновлен."

        if 'box_delete' == message.operation && uuid != message.connection_id
          box = JSON.parse message.box
          if box.type == 'gallery_image'
            $rootScope.$broadcast 'part:galleryUpdated', box
          else
            $rootScope.$broadcast 'part:delete', box
          Notify.add type: 'success', msg: "Бокс #{box.id} был удален."

        if 'box_create' == message.operation && uuid != message.connection_id
          box = JSON.parse message.box
          if box.type == 'gallery_image'
            $rootScope.$broadcast 'part:galleryUpdated', box
          else
            $rootScope.$broadcast 'part:create', box
          Notify.add type: 'success', msg: "Бокс #{box.id} был создан."

        if 'box_moved' == message.operation && uuid != message.connection_id
          box = JSON.parse message.box
          $rootScope.$broadcast 'part:moved', box
          Notify.add type: 'success', msg: "Бокс #{box.id} был перемещен."

        message

    bindCallbacks()

    send = (obj) -> stream.send(JSON.stringify(obj))

    methods = {
      stream: stream
      send:   send
      connected: -> 1 == stream.socket.readyState

      get_uuid: -> uuid

      unsubscribe_document: (document_id) ->
        last_document = null
        send({
          type: 'unregister_channel',
          attributes: {
            channel: "document/#{document_id}"
          }
        })

      subscribe_to_document: (document_id) ->
        last_document = {
          type: 'register_channel',
          attributes: {
            channel: "document/#{document_id}"
          }
        }
        send(last_document)

      blocked_for_document: (document_id) ->
        send({
          type: 'blocked_items',
          attributes: {
            channel: "document/#{document_id}"
          }
        })

      block_part: (document_id, part) ->
        send({
          type: 'block',
          attributes: {
            channel: "document/#{document_id}",
            item: part
          }
        })

      unblock_part: (document_id, part) ->
        if !document_id then return
        send({
          type: 'unblock',
          attributes: {
            channel: "document/#{document_id}",
            item: part
          }
        })

      update_part: (document_id, box) ->
        if !document_id then return
        send({
          type: 'box_update',
          attributes: {
            channel: "document/#{document_id}",
            box: JSON.stringify box
          }
        })

      delete_part: (document_id, box) ->
        if !document_id then return
        send({
          type: 'box_delete',
          attributes: {
            channel: "document/#{document_id}",
            box: JSON.stringify box
          }
        })

      create_part: (document_id, box) ->
        if !document_id then return
        send({
          type: 'box_create',
          attributes: {
            channel: "document/#{document_id}",
            box: JSON.stringify box
          }
        })

      move_part: (document_id, box) ->
        if !document_id then return
        send({
          type: 'box_moved',
          attributes: {
            channel: "document/#{document_id}",
            box: JSON.stringify box
          }
        })

    }

    return methods
