angular.module('app')
  
  .config ($urlRouterProvider, $httpProvider, $locationProvider, cfpLoadingBarProvider, Settings) ->

    $urlRouterProvider.otherwise ($injector, $location) ->
      current_path = $location.path()
      current_path = current_path.replace('/', '')
      current_path = current_path.replace('www.', '')

      $state = $injector.get('$state')

      # Если передан — редиректим на редактирование этого документа
      if (+current_path).toString() == current_path
        $state.go('logged.documents.edit', id: current_path)
      else
        # Если это урл — ищем его в базе и если нашли — редиректим
        Snipe = $injector.get('Snipe')

        Snipe.all('urls').one('find_by_url').get(url: current_path).then (found) ->
          $state.go('logged.documents.edit', id: found.document_id)
        , ->
          $state.go('errors.404')

    $locationProvider.html5Mode true

    cfpLoadingBarProvider.includeSpinner = false

    $httpProvider.defaults.headers.patch =
      'Content-Type': 'application/json; charset=UTF-8'


    # Redactor settings
    $.Redactor.prototype.alignment = ->
      {
        init: ->
          buttonLeft = @button.add(' icon icon-left', 'Влево')
          buttonRight = @button.add(' icon icon-right', 'Вправо')
          @button.addCallback(buttonLeft, @alignment.alignLeft)
          @button.addCallback(buttonRight, @alignment.alignRight)
        alignLeft: ->
          @$editor.removeClass('tright').addClass('tleft')
          angular.element(@$editor).scope().box.text_align = 'left'
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
        alignRight: ->
          @$editor.removeClass('tleft').addClass('tright')
          angular.element(@$editor).scope().box.text_align = 'right'
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
      }
    
    $.Redactor.prototype.formatting = ->
      {
        init: ->
          buttonHeader = @button.add(' icon icon-header', 'Заголовок')
          buttonQuestion = @button.add(' icon icon-question', 'Вопрос')
          buttonText = @button.add(' icon icon-text', 'Обычный текст')
          buttonClear = @button.add(' icon icon-clear', 'Очистить форматирование')
          @button.addCallback(buttonHeader, @formatting.createHeader)
          @button.addCallback(buttonQuestion, @formatting.createQuestion)
          @button.addCallback(buttonText, @formatting.createText)
          @button.addCallback(buttonClear, @formatting.cleanFormatting)
        createHeader: ->
          @$editor.removeClass('kind-Q').addClass('kind-h1')
          html = @clean.stripTags @$editor.html(), '<strong><a><em><del><br>'
          @code.set html
          angular.element(@$editor).scope().box.kind = 'h1'
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
        createQuestion: ->
          @$editor.removeClass('kind-h1').addClass('kind-Q')
          html = @clean.stripTags @$editor.html(), '<strong><a><em><del><br>'
          @code.set html
          angular.element(@$editor).scope().box.kind = 'Q'
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
        createText: ->
          @$editor.removeClass('kind-h1 kind-Q')
          html = @clean.stripTags @$editor.html(), '<strong><a><em><del><br>'
          @code.set html
          angular.element(@$editor).scope().box.kind = 'plain'
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
        cleanFormatting: ->
          html = @$editor.html()
          @code.set @clean.getPlainText html
          setTimeout(@focus.setStart, 100)
          angular.element(@$editor).scope().status.isBoxChanged = true
      }


    # Offline.js settings
    Offline.options = 
      interceptRequests: true
      checks:
        xhr:
          url: '/'
