angular.module('app')
        
  .filter 'translit', ->
    (word) ->
      answer = ""
      a = {}
      a["Ё"] = "YO"
      a["Й"] = "I"
      a["Ц"] = "TS"
      a["У"] = "U"
      a["К"] = "K"
      a["Е"] = "E"
      a["Н"] = "N"
      a["Г"] = "G"
      a["Ш"] = "SH"
      a["Щ"] = "SCH"
      a["З"] = "Z"
      a["Х"] = "H"
      a["Ъ"] = "'"
      a["ё"] = "yo"
      a["й"] = "i"
      a["ц"] = "ts"
      a["у"] = "u"
      a["к"] = "k"
      a["е"] = "e"
      a["н"] = "n"
      a["г"] = "g"
      a["ш"] = "sh"
      a["щ"] = "sch"
      a["з"] = "z"
      a["х"] = "h"
      a["ъ"] = "'"
      a["Ф"] = "F"
      a["Ы"] = "I"
      a["В"] = "V"
      a["А"] = "a"
      a["П"] = "P"
      a["Р"] = "R"
      a["О"] = "O"
      a["Л"] = "L"
      a["Д"] = "D"
      a["Ж"] = "ZH"
      a["Э"] = "E"
      a["ф"] = "f"
      a["ы"] = "i"
      a["в"] = "v"
      a["а"] = "a"
      a["п"] = "p"
      a["р"] = "r"
      a["о"] = "o"
      a["л"] = "l"
      a["д"] = "d"
      a["ж"] = "zh"
      a["э"] = "e"
      a["Я"] = "Ya"
      a["Ч"] = "CH"
      a["С"] = "S"
      a["М"] = "M"
      a["И"] = "I"
      a["Т"] = "T"
      a["Ь"] = "'"
      a["Б"] = "B"
      a["Ю"] = "YU"
      a["я"] = "ya"
      a["ч"] = "ch"
      a["с"] = "s"
      a["м"] = "m"
      a["и"] = "i"
      a["т"] = "t"
      a["ь"] = "'"
      a["б"] = "b"
      a["ю"] = "yu"
      i = 0
      while i < word.length
        answer += (if a[word[i]] is `undefined` then word[i] else a[word[i]])
        ++i
      answer = answer.toLowerCase().replace(/[^a-z0-9_\-\s]/g, '').replace(/\s+/g, '-')
      answer

  .filter 'trusted', ($sce) ->
    (text) ->
      $sce.trustAsHtml(text)

  .filter 'orderObjectBy', ->
    (items, field, reverse) ->
      filtered = []
      result = {}
      angular.forEach items, (item,key) ->
        item.key = key
        filtered.push item
        return
      filtered.sort (a, b) -> if a[field] > b[field] then 1 else -1
      if reverse then filtered.reverse()
      filtered

  .filter 'checkTree', ->
    (input, where) ->
      angular.forEach input, (item) -> item.checked = _.includes(where, item.id)
      input
