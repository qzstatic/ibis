angular.module('app')
  .directive 'rmPage', ->
    restrict: 'A'
    controller: ($rootScope) ->
      # $rootScope.toggle_stats = ->
      #   params = if $('canvas').length > 0
      #     false
      #   else
      #     { position: 'bottomright', logDigest: true, logWatches: true, autoload: true }
      #   
      #   showAngularStats(params)

      $rootScope.development = -> 'development' == NODE_ENV
      $rootScope.production  = -> NODE_ENV in ['production', 'staging']

      $rootScope.ping = -> console.warn 'ping'      
