angular.module('models').factory 'Editor', ($http, $state, $interval, ProxyStorage, Settings) ->
  
  # Класс {Editor} управляет авторизацией пользователя. Авторизационные данные сохраняются в {ProxyStorage}
  class Editor
    
    timer = null
    auth  = {}
    progress = false

    # Конструктор класса загружаем токены из хранилища {ProxyStorage}
    constructor: -> 
      @update_tokens(ProxyStorage.get('auth') || {})

      # Синхронизация обновления токенов
      $interval => 
        @update_tokens(ProxyStorage.get('auth'))
      , 3000
    
    # Возвращает валидность токена, экспайрит его чуть заранее
    #
    # @return [Boolean] статус токена 
    is_expired: ->
      created_at = @created_at()
      expires_in = @expires_in() 
      now        = new Date().getTime()
      
      now - created_at >= expires_in * 1000
    
    # @return [String] токен доступа
    access_token:  -> auth['access_token']

    # @return [String] обновляющий токен 
    refresh_token: -> auth['refresh_token']
    
    # Возвращает время последней авторизации в миллисекундах
    #
    # @return [Integer] время последней авторизации
    created_at: -> if auth['created_at'] then +auth['created_at'] else null
    
    id: -> if auth['editor_id'] then auth['editor_id'] else null
    
    # Возвращает время жизни токена в секундах
    #
    # @return [Integer] время жизни токена
    expires_in: -> if auth['expires_in'] then +auth['expires_in'] else null

    # @return [Boolean] возвращает статус пользователя — вошел или не вошел пользователь в систему 
    is_logged: -> @access_token()?
    
    # @return [Boolean] отвечает на вопрос, есть ли у пользователя обновляющий токен 
    has_refresh_token: -> @refresh_token()?
 
    # Принудительно выбрасывает пользователя из системы
    # 
    # @return [Deferred] deferred объект $http
    logout: -> @update_tokens()

    # Авторизует пользователя по паре email и password
    #
    # @param [Object] editor редактор
    # @option editor [String] login логин пользователя
    # @option editor [String] password пароль пользователя
    #
    # @return [Deferred] deferred объект $http
    authenticate: (editor) -> $http.post("#{Settings.snipe_api_url}/access_token", editor: editor).success (tokens) => @update_tokens(tokens)

    # Возвращает true, если осуществляется обновление токена и false если сейчас нет обновления
    in_progress: -> progress

    # Обновляет токены
    #
    # @return [Deferred] deferred объект $http
    refresh_tokens: ->
      progress = true
      # @add_auth_header()
      
      $http(method: 'PATCH', url: "#{Settings.snipe_api_url}/access_token", data: { refresh_token: @refresh_token() })
        .success (token) =>
          @update_tokens(token)
          progress = false
        .error => 
          @update_tokens()
          $state.go('not_logged.login')
          progress = false

    # Устанавливает специальный заголовок для передачи сессионного токена
    #
    # @param [String] token токен, который будет использоваться для выполнения дальнейших HTTP-запросов
    add_auth_header: (token = null) ->
      if token?
        $http.defaults.headers.common['X-Access-Token'] = token
      else
        delete($http.defaults.headers.common['X-Access-Token'])
    
    # Обновляет хранилище
    #
    # @param [Object] auth объект аутентификации, приходящий с сервера
    update_tokens: (new_auth = {}) ->
      angular.extend(new_auth, created_at: new Date().getTime()) unless @created_at()?

      auth = new_auth
      ProxyStorage.put('auth', new_auth)
      
      @add_auth_header @access_token()

  new Editor
