angular.module('app').run ($templateCache) ->
    $templateCache.put 'instantArticleTemplate', '''
      <customHTML>
        <customHEAD>
          <title>{{ document.title }}</title>
          <meta charset="utf-8" />
          <meta content="v1.0" property="op:markup_version" />
          <link href="http://www.{{ documentUrl }}" rel="canonical" />
          <meta content="default" property="fb:article_style" />
        </customHEAD>
        <customBody>
          <article>
            <header>
              <figure>
                <img ng-src="{{ Settings.cdn }}{{ title_image.versions.original.url }}" />
                <figcaption>
                {{ document.image_alt }} {{ document.image_author ? '/ ' + document.image_author : '' }} {{ document.image_source ? '/ ' + document.image_source : '' }}
                </figcaption>
              </figure>
              <h1>{{ document.title }}</h1>
              <h2>{{ document.subtitle }}</h2>
              <h3 class="op-kicker">{{ kicker }}</h3>
              <time class="op-published" datetime="{{ document.published_at }}">{{ document.published_at }}</time>
              <time class="op-modified" ng-if="document.datetime_and_flag" datetime="{{ document.datetime_and_flag }}">{{ document.datetime_and_flag }}</time>
              <address ng-repeat="author in authors">
                <a>{{ author.bound_document.title }}</a>
              </address>
            </header>
            <div ng-repeat="box in boxes" ng-if="box.type != 'container_attachments'">

              <p ng-if="box.type == 'paragraph' && box.kind == 'plain'" ng-bind-html="box.body"></p>

              <h2 ng-if="box.type == 'paragraph' && box.kind == 'h1'">{{ box.body }}</h2>

              <blockquote ng-if="box.type == 'paragraph' && box.kind == 'Q'">{{ box.body }}</blockquote>

              <h2 ng-if="box.type == 'text'">{{ box.title }}</h2>
              <p ng-if="box.type == 'text'" ng-bind-html="box.body"></p>

              <figure ng-if="box.type == 'inset_text'">
                <img ng-src="{{ Settings.cdn }}{{ box.attachment.versions.original.url }}" />
                <figcaption class="op-vertical-below">
                  <h1>{{ box.title }}</h1>
                  <cite class="op-vertical-below">{{ box.body }}</cite>
                </figcaption>
              </figure>

              <figure ng-if="box.type == 'inset_image'">
                <img ng-src="{{ Settings.cdn }}{{ box.attachment.versions.original.url }}" />
                <figcaption class="op-vertical-below">
                  <h1>{{ box.description }}</h1>
                  <cite class="op-vertical-below">{{ box.author }} {{ box.credits ? '/ ' + box.credits : '' }}</cite>
                </figcaption>
              </figure>

              <figure class="op-slideshow" ng-if="box.type == 'gallery'">
                <figure ng-repeat="box in box.children">
                  <img ng-src="{{ Settings.cdn }}{{ box.attachment.versions.original.url }}" />
                  <figcaption>{{ box.description }}</figcaption>
                </figure>
              </figure>

              <aside ng-if="box.type == 'quote'">
                {{ box.body }}
                <cite>{{ box.name }}</cite>
              </aside>

              <figure ng-if="box.type == 'inset_media'" ng-bind-html="box.body | trusted"></figure>

              <h2 ng-if="box.type == 'table' || box.type == 'chronology' || box.type == 'dulton_media'">
                <a href="http://www.{{ documentUrl }}/#{{ box.type }}_{{ box.id }}">
                  Посмотреть {{ box.type == 'table' ? 'таблицу' : box.type == 'chronology' ? 'хронологию' : 'видео с Dulton Media' }}
                </a>
              </h2>

              <h2 ng-if="box.type == 'inset_link' || box.type == 'inset_document'">
                <a href="http://www.{{ box.url }}">{{ box.title }}</a>
              </h2>

            </div>
            <footer ng-if="document.source_title">
              <small>{{ document.source_title }}</small>
            </footer>
          </article>
        </customBody>
      </customHTML>'''
