angular.module('agami', ['models', 'settings']).factory 'Agami', (Settings, Editor, $q, Snipe) ->

  class Agami

    upload = (file, replace = false, variants = {}) ->
      defer = $q.defer()

      url    = Settings.agami
      reader = new FileReader()

      # Это перезаливка определенного изображения
      url += replace.path if replace && replace.path? && replace.id?

      reader.onload = (e) ->
        xhr = new XMLHttpRequest()

        xhr.onload = (e) ->
          json = JSON.parse(e.currentTarget.responseText)
          defer.resolve(json)

        xhr.open('POST', "#{url}?converts=#{angular.toJson(variants)}", true)

        xhr.setRequestHeader('X-Access-Token', Editor.access_token())
        xhr.setRequestHeader('X-Original-Filename', encodeURIComponent(file.name))
        xhr.setRequestHeader('Content-Type', 'application/octet-stream')

        view = new Uint8Array(e.target.result)

        try
          xhr.send(view)
        catch error
          xhr.send(view.buffer)

      reader.readAsArrayBuffer(file)

      defer.promise

    # 1. (file)
    # 2. (file, { id: path: '/image/2014/69/t7wg3/' } })
    # 3. (file, false, { small: '120x90^' } })
    upload: (file, replace = false, variants = {}) ->
      upload.apply(@, arguments).then (json) ->
        if replace && replace.path? && replace.id?
          attachment = Snipe.one('attachments', replace.id)
          attachment.patch(json).then -> attachment.get()
        else
          Snipe.all('attachments').post(attributes: json)

  new Agami
