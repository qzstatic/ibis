angular.module('utilities').service 'ProxyStorage', ($cookies) ->

  class LocalStorage

    exists: (key) -> localStorage[key]?
    
    get: (key) -> 
      if @exists(key)
        angular.fromJson(localStorage[key])
      else
        null

    put: (key, object)-> localStorage[key] = angular.toJson(object)
    remove: (key) -> localStorage.removeItem(key)

  class LocalStorageMock
    
    storage = {}

    exists: (key) -> storage[key]?
    
    get: (key) -> 
      if @exists(key)
        angular.fromJson(storage[key])
      else
        null

    put: (key, object)-> storage[key] = angular.toJson(object)
    remove: (key) -> delete(storage[key])
    empty: -> storage = {}
        
  class CookieStorage
    constructor: ($cookies) -> @$cookies = $cookies
    get: (key) -> @$cookies.get(key)  
    put: (key, object) -> @$cookies.put(key, object)  

  if 'test' == NODE_ENV
    new class ProxyStorage extends LocalStorageMock
  else if localStorage?
    new class ProxyStorage extends LocalStorage
  else
    new class ProxyStorage extends CookieStorage
