if NODE_ENV == 'production'
  settingsConfig = require('json!yaml!config/settings/production.yml')
else if NODE_ENV == 'staging'
  settingsConfig = require('json!yaml!config/settings/staging.yml')
else if NODE_ENV == 'development'
  settingsConfig = require('json!yaml!config/settings.yml')  

angular.module('settings', [])

  .constant 'Settings',

    attachments_host: settingsConfig.hosts.agami
    cdn:              settingsConfig.hosts.cdn
    agami:            settingsConfig.hosts.agami + '/files'
    snipe_api_url:    settingsConfig.hosts.snipe + '/v1'
    aquarium_api_url: settingsConfig.hosts.aquarium + '/ibis'
    regulus_api_url:  settingsConfig.hosts.regulus + '/'
    shark_url:        settingsConfig.hosts.shark + '/'
    root_url:         '/sandboxes/list'
    barbus:           settingsConfig.hosts.barbus
    chatter:          settingsConfig.hosts.chatter
    cuckoo:           settingsConfig.hosts.cuckoo

  .constant 'DocumentSettings', {
    title: {
      name:      'title'
      label:     'Заголовок'
      type:      'string',
      hint:      'Для новостей: до 90 знаков, для других материалов: до 75 знаков.',
      max_chars: 75
    }
  }

  .constant 'QuotesSettings',
    quotesRegexp: /(«|“|^"|([^=])")([^\>\s][^"]+\<\S.+?\S\>\S+|[^\>\s].+?)([^=])("([^\>])(?!target|entry|href)|”|»|"$)/g
    spruceRegexp: /«(\S.+?\S)»/g

  .constant 'EditorSettings', {
    step: 1000
  }

  .constant 'UserSettings', {
    states_icons: {
      registered: 'envelope',
      confirmed:  'ok',
      blocked:    'remove',
      banned:     'remove',
      deleted:    'remove'
    },
    states: [
      { label: 'confirmed', title: 'подтвержден' }
      { label: 'registered', title: 'зарегистрирован' }
      { label: 'blocked', title: 'заблокирован' }
      { label: 'banned', title: 'забанен' }
      { label: 'deleted', title: 'удалён' }
    ]
  }

  .constant 'OrdersSettings', {
    states: {
      paid:     'оплачен'
      notpaid:  'не оплачен'
      canceled: 'отменен'
    },
    periods: {
      for_year: '12',
      for_month: '1'
      for_6months: '6'
    },
    methods: {
      by_card: 'карта'
      by_apple: 'Apple'
      by_unknown: 'другое'
      by_yandex: 'Яндекс'
      by_google: 'Гугл'
    },
    products: {
      online: 'online',
      profi: 'profi',
      paper: 'paper'
    }
  }

  .constant 'ListSettings',
    colors: {
      'List::Timeline': 'success'
      'List::Mixed':    'info'
      'List::Manual':   'primary'
    }
