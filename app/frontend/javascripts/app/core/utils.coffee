angular.module('utils', [])
  .factory 'Utils', (QuotesSettings) ->
    {
      getPartName: (part) ->
        if part == 'article' then return 'Статья'
        if part == 'news' then return 'Новость'
        if part == 'video' then return 'Видео'
        if part == 'gallery' then return 'Галерея'
        if part == 'quote' then return 'Цитата'
        if part == 'character' then return 'Интервью'
        if part == 'column' then return 'Колонка'
        if part == 'blog' then return 'Блог'
        if part == 'map' then return 'Карта'
        if part == 'online' then return 'Онлайн'
        return part

      replaceQuotes: (html) ->
        if !html then return html
        splitted = html.split('')

        isSpruce = false
        isQuoteInSpruce = false
        isTag = false

        quotes = ['"', '“', '”']

        _.map splitted, (char, index) ->

          if char == '<'
            isTag = true
            return

          if char == '>'
            isTag = false
            return

          if isTag then return

          if char == '«'
            isSpruce = true
            return

          if char == '»'
            isSpruce = false
            return

          spaceOnTheLeft = /(\s|\n|\t|\>|\()/g.test(splitted[index-1])

          if !isSpruce
            if char in quotes
              if spaceOnTheLeft || index == 0
                isSpruce = true
                splitted[index] = "«"
                return
              if !spaceOnTheLeft
                isSpruce = false
                splitted[index] = "»"
                return

          if isSpruce
            if char in quotes
              if spaceOnTheLeft
                isQuoteInSpruce = true
                return
              if !spaceOnTheLeft
                if isQuoteInSpruce
                  isQuoteInSpruce = false
                  return
                isSpruce = false
                splitted[index] = "»"
                return

        html = splitted.join('')
        html

        # spruces = html.match(QuotesSettings.spruceRegexp)
        # html = html.replace(QuotesSettings.spruceRegexp, "{cut}")
        # html = html.replace(QuotesSettings.quotesRegexp, "$2«$3$4»$6")
        # _.map spruces, (spruce) -> html = html.replace("{cut}", spruce)
        # html
    }
