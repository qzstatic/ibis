angular.module('vendors', [
  'http-auth-interceptor',
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'angular-loading-bar',
  'ajoslin.promise-tracker',
  'angularMoment',
  'colorpicker.module',
  'as.sortable',
  'checklist-model',
  'ngStorage',
  'jsonFormatter',
  'ngWebSocket',
  'ngFileUpload',
  'permission',
  'permission.ui',
  'nvd3'
])

angular.module('modules', [
  'auth',
  'documents',
  'editors',
  'groups',
  'sandboxes',
  'pages',
  'directories',
  'services',
  'commerce',
  'categories',
  'search',
  'import',
  'wiki',
  'inline_reports',
  'redlines',
  'mailings',
  'prices',
  'banners'
])

angular.module('utilities', [
  'ngCookies',
  'notify',
  'agami',
  'utils'
])

angular.module('models', [
  'utilities'
])

angular.module('app', [
  'vendors',
  'settings',
  'utilities',
  'models',
  'modules',
  'ui'
])

angular.module('app')

  .config (RestangularProvider, $httpProvider, $compileProvider) ->

    $httpProvider.defaults.withCredentials = true
    $httpProvider.interceptors.push 'errorHttpInterceptor'

    # Оборачивает put/path в attributes
    RestangularProvider.addRequestInterceptor (elem, method, what) ->
      if method in ['put', 'patch'] && !elem.meta? then { attributes: elem } else elem

  .run (Restangular, Editor, $http, amMoment, Notify, $rootScope, authService, $interval, $window, Atasus, PermRoleStore, GroupsService, $q) ->
    amMoment.changeLocale('ru')

    timeForUpdateToken = (Editor.expires_in() * 1000) - 10000
    lastMilliseconds = moment().valueOf()

    refreshTokens = ->
      unless Editor.in_progress()
        Editor.refresh_tokens().then ->
          lastMilliseconds = moment().valueOf()
          authService.loginConfirmed 'success', (config) ->
            # Обновляет буферезированные запросы новым токеном
            config.headers['X-Access-Token'] = Editor.access_token()
            config

    # Обработка 401-х запросов и обновление токена
    $rootScope.$on 'event:auth-loginRequired', (event, data) -> refreshTokens()

    interval = $interval ->
      if !Editor.has_refresh_token()
        $interval.cancel interval
        return
      else
        refreshTokens()
    , timeForUpdateToken

    $(document).on "visibilitychange", ->
      if document.visibilityState == 'visible'
        nowMilliseconds = moment().valueOf()
        if (nowMilliseconds - lastMilliseconds) >= timeForUpdateToken
          refreshTokens()

    Restangular.setErrorInterceptor (response, deferred, finisher) ->
      $rootScope.$emit 'document.disablePublishButton', false
      if angular.isObject(response.data)
        if 412 == response.status
          Notify.add msg: "Произошел конфликт!", type: 'error'
        else
          Notify.add msg: "Ошибка выполнения операции: #{angular.toJson(response.data)}", type: 'error'
      else if 500 == response.status
        Notify.add msg: 'Критическая ошибка сервера.', type: 'error'
      else if 403 == response.status
        Notify.add msg: 'Недостаточно прав доступа.', type: 'error'

      if $window.atatus && window.ENV != 'development'
        message = 'AJAX Error: ' + response.config.method + ' ' + response.status + ' ' + response.config.url
        atatus.notify new Error(message),
          config: response.config
          status: response.status

    Restangular.addRequestInterceptor (elem, method, what, url) ->
      if method in ['patch', 'post'] && what == 'boxes'
        $rootScope.$emit 'document.disablePublishButton', true
      if Editor.is_expired() then refreshTokens()
      elem

    Restangular.addResponseInterceptor (data, operation, what, url) ->
      if operation in ['patch', 'post'] && what == 'boxes'
        $rootScope.$emit 'document.disablePublishButton', false
      data


    isEditorInGroup = (group) -> if group then _.includes group.editor_ids, Editor.id()

    isAdmin = ->
      deferred = $q.defer()
      GroupsService.list().then (groups) ->
        group = _.find groups, {'slug': 'administrators'}
        if isEditorInGroup group
          deferred.resolve()
        else
          deferred.reject()
      deferred.promise

    isEditor = ->
      deferred = $q.defer()
      GroupsService.list().then (groups) ->
        group = _.find groups, {'slug': 'web_editors'}
        if isEditorInGroup group
          deferred.resolve()
        else
          deferred.reject()
      deferred.promise

    isCommers = ->
      deferred = $q.defer()
      GroupsService.list().then (groups) ->
        group = _.find groups, {'slug': 'advertisers'}
        if isEditorInGroup group
          deferred.resolve()
        else
          deferred.reject()
      deferred.promise

    isSubscription = ->
      deferred = $q.defer()
      GroupsService.list().then (groups) ->
        group = _.find groups, {'slug': 'subscription'}
        if isEditorInGroup group
          deferred.resolve()
        else
          deferred.reject()
      deferred.promise

    PermRoleStore.defineManyRoles
      'ADMIN': () -> isAdmin()
      'EDITOR': () -> isEditor()
      'COMMERS': () -> isCommers()
      'SUBSCRIPTION': () -> isSubscription()

    # Блокируем UI пользователя если сети нет
    $disabledLayout = $('.popup-disabled')
    Offline.on 'up', -> $disabledLayout.fadeOut(300)
    Offline.on 'down', -> $disabledLayout.fadeIn(300)

    # atatus.onBeforeErrorSend (payload) ->
    #   payload.userActions = Atasus.getUserActions()
    #   return true;

    # Запрещаем переход на предыдущую страницу при нажатии Backspace
    $(document).on 'keydown', (event) ->
      doPrevent = false
      if (event.keyCode == 8)
        d = event.srcElement || event.target
        if d.tagName.toUpperCase() == 'INPUT' || d.tagName.toUpperCase() == 'TEXTAREA' || d.tagName.toUpperCase() == 'DIV'
          doPrevent = d.readOnly || d.disabled
        else
          doPrevent = true
      if doPrevent
        event.preventDefault()

# Снайп
angular.module('app').factory 'Snipe', (Restangular, Settings) ->
  Restangular.withConfig (RestangularConfigurer) ->
    RestangularConfigurer.setBaseUrl Settings.snipe_api_url

# Аквариум
angular.module('app').factory 'Aquarium', (Restangular, Settings) ->
  Restangular.withConfig (RestangularConfigurer) ->
    RestangularConfigurer.setBaseUrl Settings.aquarium_api_url

# Regulus
angular.module('app').factory 'Regulus', (Restangular, Settings) ->
  Restangular.withConfig (RestangularConfigurer) ->
    RestangularConfigurer.setBaseUrl Settings.regulus_api_url

# Barbus
angular.module('app').factory 'Barbus', (Restangular, Settings) ->
  Restangular.withConfig (RestangularConfigurer) ->
    RestangularConfigurer.setBaseUrl Settings.barbus

# Atasus Integration
angular.module('app').factory '$exceptionHandler', ($window) ->
  (exception, cause) ->
    console.log exception
    if $window.atatus && NODE_ENV != 'development'
      $window.atatus.notify(exception)

# Atasus ajax error handler
angular.module('app').factory 'errorHttpInterceptor', ($q, $window) ->
  return {
    responseError: (rejection) ->
      if $window.atatus && NODE_ENV != 'development'
        message = 'AJAX Error: ' + rejection.config.method + ' ' + rejection.status + ' ' + rejection.config.url
        atatus.notify new Error(message),
          config: rejection.config
          status: rejection.status
      return $q.reject rejection
  }
