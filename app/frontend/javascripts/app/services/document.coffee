angular.module('services')
  .factory 'Document', (Snipe, doc_settings) ->

    class Document
      @loaded_document = null

      load: (id) ->
        Snipe.one('documents', id).get().then (document) ->
          doc_settings.set(document)
          @loaded_document = document

      create: (document) -> Snipe.all('documents').post(attributes:document)

    new Document
