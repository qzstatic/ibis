angular.module('services')
  .factory 'GroupsService', (Snipe, $q) ->

    class GroupsService
      list = null
      deferred = $q.defer()
      
      groups: []
      
      list: ->
        if list? 
          if list.length
            deferred.resolve(list)
        else
          list = []
          Snipe.all('groups').getList().then (json) => 
            list = json
            @groups = json
            deferred.resolve(list)
        deferred.promise

    new GroupsService
