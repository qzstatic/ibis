angular.module('services')
  .factory 'LinksService', () ->

    class LinksService

      unpublishedLinks = []
      emptyLinks = []

      addUnpublished: (id) ->
        if !_.includes unpublishedLinks, id then unpublishedLinks.push(id)

      removeUnpublished: (id) ->
        _.remove unpublishedLinks, (n) -> n == id

      totalUnpublished: -> unpublishedLinks.length

      clearUnpublished: -> unpublishedLinks.length = 0

      addEmpty: (link) -> if !_.includes emptyLinks, link then emptyLinks.push link

      removeEmpty: (link) -> _.remove emptyLinks, (n) -> n == link

      getEmpty: -> emptyLinks

      clearEmpty: -> emptyLinks.length = 0

      totalEmpty: -> emptyLinks.length

    new LinksService
