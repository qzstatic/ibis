angular.module('services')
  .factory 'CompaniesService', () ->

    class CompaniesService

      companyIds: []

      update: (ids) ->
        @companyIds = ids
        @companyUpdated @companyIds if @companyUpdated?

    new CompaniesService
