angular.module('services')
  .factory 'Atasus', () ->

    class AtasusService
      
      userActions = []
      
      add: (event, box, optional = false) ->
        action = {}
        action.a = event
        if box.type? && box.id? && box.position?
          action.e = moment().format('HH:mm:ss') + ' ' + box.type + ' / id: ' + box.id + ' / position: ' + box.position
        else
          action.e = moment().format('HH:mm:ss') + box
        userActions.push action

      getUserActions: -> userActions

    new AtasusService
