angular.module('services')
  .factory 'PaywallService', () ->

    class PaywallService
      enabled = false
      position = 2
      excludeBoxes = ['text', 'chronology', 'inset_text']

      isBoxExcluded: (box) ->
        if box.type == 'inset_link' && box.align == 'left'
          return true
        box.type in excludeBoxes


    new PaywallService
