angular.module('services')
  .factory 'SandboxesService', (Snipe, $q) ->

    class SandboxesService
      list = null

      by_id: (id) -> _.find(list, id: id)

      reload: ->
        list = []
        Snipe.all('sandboxes').getList().then (json) -> angular.extend(list, json)

      list: -> 
        if list?
          deferred = $q.defer()
          deferred.resolve(list)

          deferred.promise
        else
          @reload()

    new SandboxesService
