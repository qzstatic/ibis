angular.module('services')
  .factory 'CategoriesService', (Snipe, $q) ->
    class CategoriesService
      list = null
      tree = null
      list_with_children = null
      by_ids = {}
      selectedCategories: []
      selected = null
      list: ->
        if list?
          deferred = $q.defer()
          deferred.resolve(list)
          deferred.promise
        else
          list = []
          Snipe.all('categories').getList().then (json) ->
            list = json
            by_ids[category.id] = category for category in list

      list_with_children: ->
        if list_with_children?
          deferred = $q.defer()
          deferred.resolve(list_with_children)
          deferred.promise
        else
          list_with_children = []
          Snipe.one('categories/with_children').get().then (json) ->
            list_with_children = json

      tree: ->
        if tree?
          deferred = $q.defer()
          deferred.resolve(tree)
          deferred.promise
        else
          tree = []
          Snipe.one('categories/tree').get().then (json) -> tree = json

      slugs_by_ids: (ids, delimiter = ', ') ->
        slugs = []
        for id in ids
          slugs.push(by_ids[id].slug) if by_ids[id]?
        slugs.join(delimiter)

      titles_by_ids: (ids, delimiter = ', ') ->
        titles = []
        for id in ids
          titles.push(by_ids[id].title) if by_ids[id]?
        titles.join(delimiter)

      get_category_by_id: (id) -> by_ids[id]

    new CategoriesService
