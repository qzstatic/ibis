angular.module('services')
  .factory 'PartsService', () ->

    class PartsService

      unsavedParts = []

      unpublishedParts = []

      addUnsaved: (id) ->
        if !_.includes unsavedParts, id then unsavedParts.push(id)

      removeUnsaved: (id) ->
        _.remove unsavedParts, (n) -> n == id

      checkUnsaved: (id) -> unsavedParts.indexOf(id) == -1

      totalUnsaved: -> unsavedParts.length

      addUnpublished: (id) ->
        if !_.includes unpublishedParts, id then unpublishedParts.push(id)

      removeUnpublished: (id) ->
        _.remove unpublishedParts, (n) -> n == id

      totalUnpublished: -> unpublishedParts.length

      clearUnsaved: -> unsavedParts.length = 0

      clearUnpublished: -> unpublishedParts.length = 0

    new PartsService
