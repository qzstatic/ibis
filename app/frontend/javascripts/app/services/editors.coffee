angular.module('services')
  .factory 'EditorsService', (Snipe, $q) ->

    class EditorsService
      list       = []
      list_by_id = {}
      deferred   = $q.defer()
      
      constructor: -> 
        Snipe.all('editors').getList().then (editors) ->
          list = editors
          for editor in editors
            editor.full_name = [editor.first_name, editor.last_name].join(' ')
            list_by_id[editor['id']] = editor

          deferred.resolve(list)

      by_id: (id) -> list_by_id[id]
      full_name_by_id: (id) -> list_by_id[id].full_name if list_by_id[id]?
      list: -> deferred.promise

    new EditorsService

