angular.module('services')
  .factory 'Search', (Snipe, Settings, $http) ->

    class SearchService

      documents: (pattern, categories = []) ->
        # Очищаем паттерн
        pattern = pattern.replace(/^(http|https):\/\//, '')
        if 'development' == NODE_ENV
          # Обычный поиск
          Snipe.all('documents/search').getList(query: pattern, category_slugs: categories.join(','))
        else
          # Поиск через Elastic
          $http.get("#{Settings.regulus_api_url}documents?q=#{pattern}&limit=50&categories=#{categories.join(',')}").then (json) ->
            _.map json.data.found, (item) ->
              item.source.highlight = item.highlight
              item.source

      documents_with_params: (query) ->
        query = angular.copy(query)
        query.q = query.q.replace(/^(http|https):\/\//, '') if query.q?
        query.from = (query.page - 1) * 20

        params = $.param(query)

        $http.get("#{Settings.regulus_api_url}documents?#{params}").then (json) ->
          found = _.map json.data.found, (item) ->
            item.source.highlight = item.highlight
            item.source

          { found: found, stat: json.data.stat }

      users: (query) ->
        query.from = (query.page - 1) * 100
        params     = $.param(query)
        $http.get("#{Settings.regulus_api_url}users?#{params}").then (users) -> users.data

      mobile_users: (query) ->
        query.from = (query.page - 1) * 100
        params     = $.param(query)
        $http.get("#{Settings.regulus_api_url}mobile_users?#{params}").then (users) -> users.data

      contacts: (query) ->
        query.from = (query.page - 1) * 100
        params     = $.param(query)
        $http.get("#{Settings.regulus_api_url}contacts?#{params}").then (contacts) -> contacts.data

      orders: (query) ->
        query.from = (query.page - 1) * 50
        params     = $.param(query)
        $http.get("#{Settings.regulus_api_url}orders?#{params}").then (orders) -> orders.data

      payments: (query) ->
        query.from = (query.page - 1) * 50
        params     = $.param(query)
        $http.get("#{Settings.regulus_api_url}payments?#{params}").then (orders) -> orders.data

    new SearchService
