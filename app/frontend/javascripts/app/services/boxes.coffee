angular.module('services')
  .factory 'BoxesService', () ->

    class BoxesService

      unsavedBoxes = []
      unpublishedBoxes = []
      deletedBoxes = []

      addUnsaved: (id) ->
        if !_.includes unsavedBoxes, id then unsavedBoxes.push(id)

      removeUnsaved: (id) ->
        _.remove unsavedBoxes, (n) -> n == id

      totalUnsavedBoxes: -> unsavedBoxes.length

      checkUnsaved: (id) -> unsavedBoxes.indexOf(id) == -1


      addUnpublished: (id) ->
        if !_.includes unpublishedBoxes, id then unpublishedBoxes.push(id)

      removeUnpublished: (id) ->
        _.remove unpublishedBoxes, (n) -> n == id

      totalUnpublishedBoxes: -> unpublishedBoxes.length


      addDeleted: (id) ->
        if !_.includes deletedBoxes, id then deletedBoxes.push(id)

      removeDeleted: (id) ->
        _.remove deletedBoxes, (n) -> n == id

      totalDeletedBoxes: -> deletedBoxes.length


      clearUnsaved: -> unsavedBoxes.length = 0

      clearUnpublished: -> unpublishedBoxes.length = 0

      clearDeleted: -> deletedBoxes.length = 0

    new BoxesService
