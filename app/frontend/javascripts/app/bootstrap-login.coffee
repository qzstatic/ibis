require('./login')

require('./services/groups')

req = require.context("./core", true, /.*/)
req.keys().forEach( (key) -> req(key) )

req = require.context("./modules/auth", true, /.*/)
req.keys().forEach( (key) -> req(key) )
