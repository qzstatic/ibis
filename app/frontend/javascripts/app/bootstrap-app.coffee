require('./app')

req = require.context("./services", true, /.coffee/)
req.keys().forEach( (key) -> req(key) )

req = require.context("./core", true, /.coffee/)
req.keys().forEach( (key) -> req(key) )

req = require.context("./modules", true, /.coffee/)
req.keys().forEach( (key) -> req(key) )
